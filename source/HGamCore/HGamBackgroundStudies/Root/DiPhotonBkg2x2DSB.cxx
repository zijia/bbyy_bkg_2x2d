#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <HGamBackgroundStudies/DiPhotonBkg2x2DSB.h>
#include <HGamAnalysisFramework/HgammaIncludes.h>
#include "TTimeStamp.h"
#include "TRandom3.h"
#include "TCanvas.h"


#define LUMI 140.0 


ClassImp(DiPhotonBkg2x2DSB)

DiPhotonBkg2x2DSB :: DiPhotonBkg2x2DSB (Str name)
{}

StrV vectorize(Str str, Str sep=" ")
{
	StrV result;
	TObjArray *strings = str.Tokenize(sep.Data());
	if (strings->GetEntries()==0) { delete strings; return result; }
	TIter istr(strings);
	while (TObjString* os=(TObjString*)istr()) {
		if (os->GetString()[0]=='#') break;
		result.push_back(os->GetString());
	}
	delete strings;
	return result;
}

VecD vectorizeNum(Str str, Str sep=" ")
{
	VecD result; StrV vecS = vectorize(str,sep);
	for (uint i=0;i<vecS.size();++i)
		result.push_back(atof(vecS[i]));
	return result;
}

VecD MakeUniformVecD(int N, double min, double max)
{
	VecD vec; double dx=(max-min)/N;
	for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
	return vec;
}


StrV setBinEdgeLabels(VecD bins, bool useDecimal = false ){ 
	Str binlables = "";
	for(unsigned int i=0; i<bins.size()-1; i++)
		binlables += useDecimal?Form("%.1f-%.1f ",bins[i],bins[i+1]):Form("%.0f-%.0f ",bins[i],bins[i+1]);
	binlables.ReplaceAll("99999","#infty");
	StrV labels = vectorize(binlables);
	return labels;
}

void DiPhotonBkg2x2DSB :: addCfgFile(Str conf) {
	TEnv cfg;
	int file_ok = cfg.ReadFile(conf.Data(),EEnvLevel(0));
	if (file_ok!=0) { Fatal("","Cannot open %s",conf.Data()); abort(); }
	TIter cfg_itr((const TCollection *)cfg.GetTable());
	while (TEnvRec *entry = (TEnvRec*) cfg_itr()){
		if (!confHasVal(entry->GetName())){
			setConfigVal(entry->GetName(),entry->GetValue());
		}else{
			if(!confHasEqualVal(entry->GetName(),entry->GetValue()))
				cout<<">>>>>>>>>>>>>>>>>>>>>>>>>> Warning Inconsistant values for config entry: "
					<< entry->GetName() << " :: " << entry->GetValue() << " vs. "
					<< m_config->GetValue(entry->GetName(),"") <<endl;
		}
	}
}

EL::StatusCode DiPhotonBkg2x2DSB :: initializeInputTree (SBInputs &SBin)
{


	SBin.cutFlow      = new TH1D(Form("cutFlow_%s_%s",SBin.name.Data(),SBin.type.Data()),"",16,0,16);

	//cutFlow_ori
	SetupFloat(*SBin.inTree, "BCal_m_yyjj_tilde", SBin.BCal_m_yyjj_tilde);
	SetupFloat(*SBin.inTree, "BCal_m_jj", SBin.BCal_m_jj);
	SetupFloat(*SBin.inTree, "m_yy", SBin.m_yy);
	SetupFloat(*SBin.inTree, "y1_pt", SBin.y1_pt);
	//SetupFloat(*SBin.inTree, "y1_eta", SBin.y1_eta);
	//SetupFloat(*SBin.inTree, "y1_phi", SBin.y1_phi);
	SetupFloat(*SBin.inTree, "y2_pt", SBin.y2_pt);
	//SetupFloat(*SBin.inTree, "y2_eta", SBin.y2_eta);
	//SetupFloat(*SBin.inTree, "y2_phi", SBin.y2_phi);
	SetupFloat(*SBin.inTree, "weight", SBin.weight);
	SetupFloat(*SBin.inTree, "weightFJvt", SBin.weightFJvt);
	SetupFloat(*SBin.inTree, "Lumi", SBin.Lumi);
	//SetupFloat(*SBin.inTree, "weight_lumiXsec", SBin.weight_lumiXsec);
	SetupFloat(*SBin.inTree, "yybb_weight", SBin.yybb_weight);
	SetupFloat(*SBin.inTree, "crossSectionBRfilterEff", SBin.crossSectionBRfilterEff);
	//SetupInt(*SBin.inTree, "y1_convType", SBin.y1_convType);
	//SetupInt(*SBin.inTree, "y2_convType", SBin.y2_convType);  
	SetupInt(*SBin.inTree, "y1_isTight", SBin.y1_isTight);
	SetupInt(*SBin.inTree, "y2_isTight", SBin.y2_isTight);
	SetupInt(*SBin.inTree, "y1_isIso", SBin.y1_IsIso);
	SetupInt(*SBin.inTree, "y2_isIso", SBin.y2_IsIso);
	SetupUInt(*SBin.inTree, "y1_isEMTight", SBin.y1_isEMTight);
	SetupUInt(*SBin.inTree, "y2_isEMTight", SBin.y2_isEMTight);
	SetupBool(*SBin.inTree, "isPassedMassCut",SBin.isPassedMassCut);
	SetupBool(*SBin.inTree, "isPassedRelPtCuts",SBin.isPassedRelPtCuts);
	//SetupBool(*SBin.inTree, "isPassedTriggerMatch",SBin.isPassedTriggerMatch);
	//SetupBool(*SBin.inTree, "passCrackVetoCleaning",SBin.passCrackVetoCleaning);
	//SetupBool(*SBin.inTree, "isPassedJetEventClean",SBin.isPassedJetEventClean);
	SetupInt(*SBin.inTree, "yybb_btag77_85_BCal_cutFlow",SBin.yybb_btag77_85_BCal_cutFlow);
	SetupInt(*SBin.inTree, "yybb_btag77_BCal_cutFlow",SBin.yybb_btag77_BCal_cutFlow);
	SetupInt(*SBin.inTree, "N_j_b77", SBin.N_j_b77); 
	SetupFloat(*SBin.inTree, "y1_topoetcone20", SBin.y1_topoetcone20); 
	SetupFloat(*SBin.inTree, "y1_ptcone20", SBin.y1_ptcone20); 
	SetupFloat(*SBin.inTree, "y2_topoetcone20", SBin.y2_topoetcone20); 
	SetupFloat(*SBin.inTree, "y2_ptcone20", SBin.y2_ptcone20); 
	SetupInt(*SBin.inTree, "yybb_nonRes_XGBoost_btag77_BCal_Cat",SBin.yybb_nonRes_XGBoost_btag77_BCal_Cat);
	SetupInt(*SBin.inTree, "yybb_nonRes_XGBoost_btag77_85_BCal_Cat",SBin.yybb_nonRes_XGBoost_btag77_85_BCal_Cat);
	SetupInt(*SBin.inTree, "runNumber", SBin.runNumber);
	SetupFloat(*SBin.inTree, "BDT_BCal_yy_Score", SBin.BDT_BCal_yy_Score);		
	SetupFloat(*SBin.inTree, "BDT_BCal_ttH_Score", SBin.BDT_BCal_ttH_Score);		

	if(SBin.isMC){
		SetupInt(*SBin.inTree, "y1_isTight_nofudge", SBin.y1_isTight_noFF);
		SetupInt(*SBin.inTree, "y2_isTight_nofudge", SBin.y2_isTight_noFF);
		SetupUInt(*SBin.inTree, "y1_isEMTight_nofudge", SBin.y1_isEMTight_noFF);
		SetupUInt(*SBin.inTree, "y2_isEMTight_nofudge", SBin.y2_isEMTight_noFF);
	}
	//else {SetupFloat(*SBin.inTree, "frac_weight", SBin.frac_weight);}
	for(auto var:inputs_F) SetupFloat(*SBin.inTree,var,SBin.fMap[var]);
	for(auto var:inputs_I) SetupInt(*SBin.inTree,var,SBin.iMap[var]);

	return EL::StatusCode::SUCCESS;
}


EL::StatusCode DiPhotonBkg2x2DSB :: initDataSideBand ( SBInputs &inSB, Str type)
{

	// solution mode :: FIT :: SIMPLEST, SIMPLIFIED, ISOLATION
	inSB.sol_mode = SB2x2DSolver::FIT;
	inSB.pointers.resize(SB2x2D::NbPointers,0);
	inSB.pointers[SB2x2D::Quality1Signal1] = &inSB.y1_isTight;
	inSB.pointers[SB2x2D::Quality1Signal2] = &inSB.y2_isTight;
	inSB.pointers[SB2x2D::Quality2Signal1] = &inSB.y1_IsIso;
	inSB.pointers[SB2x2D::Quality2Signal2] = &inSB.y2_IsIso;
	for(int i=0; i<(int)histo_names.size(); i++){
		Str name = histo_names[i];
		int nbinsx = var_bins[name].size();
		double *xbins = new double[nbinsx];
		for(int ib=0;ib<nbinsx;ib++) xbins[ib]=var_bins[name][ib];
		Str hname = Form("DATA_%s_%s",name.Data(),type.Data());
		SB2x2D * sb = new SB2x2D(hname,"",inSB.pointers,nbinsx-1,xbins);
		inSB.SB_data.push_back(sb);
	}
	return EL::StatusCode::SUCCESS;
}



EL::StatusCode DiPhotonBkg2x2DSB :: initMCSideBand ( SBInputs &inSB, Str type)
{

	inSB.pair_IsSignal = 1;
	inSB.pointers.resize(SB2x2DTruth::NbPointers,0);
	inSB.pointers[SB2x2DTruth::IsTrueSignal]    = &inSB.pair_IsSignal;
	inSB.pointers[SB2x2DTruth::Quality2Signal1] = &inSB.y1_IsIso;
	inSB.pointers[SB2x2DTruth::Quality2Signal2] = &inSB.y2_IsIso;
	if(type!="NoFudgeFactor"){
		inSB.pointers[SB2x2DTruth::Quality1Signal1] = &inSB.y1_isTight;
		inSB.pointers[SB2x2DTruth::Quality1Signal2] = &inSB.y2_isTight;
	}else{
		inSB.pointers[SB2x2DTruth::Quality1Signal1] = &inSB.y1_isTight_noFF;
		inSB.pointers[SB2x2DTruth::Quality1Signal2] = &inSB.y2_isTight_noFF;
	}
	for(int i=0; i<(int)histo_names.size(); i++){
		Str name = histo_names[i];
		int nbinsx = var_bins[name].size();
		double *xbins = new double[nbinsx];
		for(int ib=0;ib<nbinsx;ib++) xbins[ib]=var_bins[name][ib];    
		Str hname = Form("MC_%s_%s_%s_%s",inSB.catMC.Data(),inSB.name.Data(),name.Data(),type.Data());
		SB2x2DTruth * sb = new SB2x2DTruth(hname,"",inSB.pointers,nbinsx-1,xbins);
		inSB.SB_truth.push_back(sb);
	}
	return EL::StatusCode::SUCCESS;
}


// EL::StatusCode DiPhotonBkg2x2DSB :: initMCSideBandFromFile ( SBInputs &inSB, TFile*f, Str type)
// {
//   f = TFile::Open(m_config->GetValue("InfileMC",""));
//   for(int i=0; i<(int)histo_names.size(); i++){
//     Str name = histo_names[i];
//     Str hname = Form("MC/MC_%s_%s_%s_%s",inSB.catMC.Data(),inSB.name.Data(),name.Data(),type.Data());
//     SB2x2DTruth *sb = (SB2x2DTruth*)f->Get(hname);
//     if(sb) 
//       inSB.SB_truth.push_back(sb);      
//     else fatal(Form("Cannot locate sideband object: %s",hname.Data()));
//   }
//   return EL::StatusCode::SUCCESS;
// }



VecD GetIncrementalBinning(Str bin_str){

	double step = -1.0;
	Str step_str = bin_str(bin_str.First(":")+1,bin_str.Length());
	step = Str(bin_str(bin_str.First(":")+1,bin_str.Length())).Atof();
	bin_str.Remove(bin_str.Length()-1);
	bin_str.ReplaceAll(":","").ReplaceAll("-"," ").ReplaceAll("N","-");
	VecD bins = vectorizeNum(bin_str);
	if(step>0){
		double min = bins[0];
		double max = bins[bins.size()-1];
		int N = (max-min)/step;
		bins = MakeUniformVecD(N,min,max);
	}

	return bins;
}


void addHistosToList(StrV &hnv, StrV &vnv, StrV hnvin, StrV vnvin){
	for(unsigned long i=0; i<hnvin.size(); i++)
	{hnv.push_back(hnvin[i]); vnv.push_back(vnvin[i]);}
}

EL::StatusCode DiPhotonBkg2x2DSB :: initialize ( TString cfgName)
{
	myy_low=105; myy_high=160;
	doRenormalize = true;
	m_config = new TEnv();
	string TestArea = string(getenv("TestArea")); 
	Str confBaseDir = TestArea+"/HGamCore/HGamBackgroundStudies/data/";
	addCfgFile( confBaseDir+cfgName+".cfg" );
	//addCfgFile( confBaseDir+"input.cfg" );
	Str bin_cfg = m_config->GetValue("ExternalBinningConfig","");
	if(bin_cfg!="") addCfgFile( confBaseDir+bin_cfg );
	Str xsec_cfg = m_config->GetValue("xsecHistoConfig","");
	if(xsec_cfg!="") addCfgFile( confBaseDir+xsec_cfg );
	Str xsec_vs_mgg_cfg = m_config->GetValue("xsecVmggHistoConfig","");
	if(xsec_vs_mgg_cfg!="") addCfgFile( confBaseDir+xsec_vs_mgg_cfg );
	Str yybb_cfg = m_config->GetValue("yybbHistoConfig","");
	if(yybb_cfg!="") addCfgFile( confBaseDir+yybb_cfg );
	Str coupling_cfg = m_config->GetValue("couplingHistoConfig","");
	if(coupling_cfg!="") addCfgFile( confBaseDir+coupling_cfg );
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	StrV histo_names_temp, varName_temp;

	StrV histo_names_basic  = vectorize(m_config->GetValue("HistoName",""));
	StrV varName_basic  = vectorize(m_config->GetValue("HistoVar",""));
	addHistosToList(histo_names_temp,varName_temp,histo_names_basic,varName_basic);

	StrV xsec_histo_names  = vectorize(m_config->GetValue("xsecHistoName",""));
	StrV xsec_varName  = vectorize(m_config->GetValue("xsecHistoVar",""));
	addHistosToList(histo_names_temp,varName_temp,xsec_histo_names,xsec_varName);

	StrV xsec_vs_mgg_histo_names  = vectorize(m_config->GetValue("xsecVmggHistoName",""));
	StrV xsec_vs_mgg_varName  = vectorize(m_config->GetValue("xsecVmggHistoVar",""));
	addHistosToList(histo_names_temp,varName_temp,xsec_vs_mgg_histo_names,xsec_vs_mgg_varName);

	StrV yybb_histo_names  = vectorize(m_config->GetValue("yybbHistoName",""));
	StrV yybb_varName  = vectorize(m_config->GetValue("yybbHistoVar",""));
	addHistosToList(histo_names_temp,varName_temp,yybb_histo_names,yybb_varName);
	StrV coupling_histo_names  = vectorize(m_config->GetValue("couplingHistoName",""));
	StrV coupling_varName  = vectorize(m_config->GetValue("couplingHistoVar",""));
	addHistosToList(histo_names_temp,varName_temp,coupling_histo_names,coupling_varName);
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	for(unsigned long i=0; i<histo_names_temp.size(); i++){

		Str name = histo_names_temp[i];
		histo_2dSel[name] = m_config->GetValue(Form("%s.2DSel",name.Data()),"");
		if ( name.Contains("catCoupBDT") ) histo_2dSel[name].ReplaceAll("catCoup","catCoupBDT");	
		///////////////////////////////////////////////////////////////////////////////////

		cout<<"-------------------->>>>>> "<<name<<endl;
		cout<<"histo_2dSel[name] == "<<histo_2dSel[name]<<endl;
		if(histo_2dSel[name].Contains("catCoup")){
			Str tmp_index = histo_2dSel[name];
			tmp_index.ReplaceAll("catCoup=","");
			tmp_index.ReplaceAll("catCoupBDT=","");
			StrV merge;
			if ( tmp_index.Contains("-") ){
				tmp_index.ReplaceAll("-"," ");
				merge = vectorize(tmp_index);
			}
			cout<<" MERGE SIZE == "<<merge.size()<<endl;
			if ( merge.size() == 0 )
				histo_2dSel_catIndex[name].push_back( atoi(tmp_index) );
			else {
				for ( Str bin:merge ) 
					histo_2dSel_catIndex[name].push_back( atoi(bin) );
			}
		}


		///////////////////////////////////////////////////////////////////////////////////

		StrV bin_strV = vectorize(m_config->GetValue(Form("%s.bins",name.Data()),""));
		if(bin_strV.size()==0) fatal(Form("Cannot find a valid binning for %s",name.Data()));

		///////////////////////////////////////////////////////////////////////////////////

		if(name.Contains("pT_yy_vs_mgg")){      
			Str tempBins = bin_strV[0];
			tempBins.ReplaceAll(".bins","");
			Str myyBins = m_config->GetValue(Form("%s.bins",tempBins.Data()),"");
			VecD pTyyBins = vectorizeNum(m_config->GetValue("pT_yy.bins",""));
			for(unsigned long ipt=0; ipt<pTyyBins.size()-1; ipt++){
				Str new_hname = Form("%s_%.0f-%.0f",name.Data(),pTyyBins[ipt],pTyyBins[ipt+1]);
				histo_names.push_back(new_hname);
				var_bins[new_hname] = GetIncrementalBinning(myyBins);
				histo_vars[new_hname] = varName_temp[i];
				var_Scale[new_hname] = m_config->GetValue("m_yy.scale",1.0);
				histo_2dSel_catIndex[new_hname].push_back( ipt );
				histo_2dSel[new_hname] = m_config->GetValue(Form("%s.2DSel",name.Data()),"");
			}
			continue;
		}
		histo_names.push_back(name);
		histo_vars[name] = varName_temp[i];
		var_Scale[name] = m_config->GetValue(Form("%s.scale",name.Data()),1.0);

		///////////////////////////////////////////////////////////////////////////////////

		if(bin_strV[0].Contains(".bins")){
			Str tempBins = bin_strV[0]; 
			tempBins.ReplaceAll(".bins","");      
			bin_strV = vectorize(m_config->GetValue(Form("%s.bins",tempBins.Data()),""));
		}
		///////////////////////////////////////////////////////////////////////////////////

		if(bin_strV.size()>1){
			var_bins[name] = vectorizeNum(m_config->GetValue(Form("%s.bins",name.Data()),""));
			if(var_bins[name].back()==99999) var_bins[name].pop_back();

		}
		else{
			Str hname = bin_strV[0];
			var_bins[name] = GetIncrementalBinning(hname);
		}

		if(var_bins[name].size()==0)
			fatal(Form("Cannot find binning for variable %s",name.Data()));
	}

	for(auto name:histo_names){
		cout<<" Sideband will be created for variable --->> "<<name<<endl;
		//for(auto bin:var_bins[name]) cout<<" bin edge = "<<bin<<endl;  
	}
	inputs_F = vectorize(m_config->GetValue("InputFVariables",""));
	inputs_I = vectorize(m_config->GetValue("InputIVariables",""));

	/*
	 * initialise input structs
	 */

	//abort();

	///////////////////
	// data
	///////////////////


	std::cout<<"Inizializzo dati"<<std::endl;

	Str treeName = m_config->GetValue("TreeName","");
	SBin_data.inTree = new TChain(treeName);
	for(auto file:vectorize(m_config->GetValue("INF_data",""))){
		std::cout<<m_config->GetValue("INF_data","")<<std::endl;
		SBin_data.inTree->Add(file);
	}
	//========= zihang =================
	Str dataName = m_config->GetValue("INF_data","");
	//SBin_data.name = "data1516";
	//SBin_data.name = "data18";
	SBin_data.name = "data15161718";
	SBin_data.type = "LP3";
	SBin_data.isMC = false;
	initializeInputTree(SBin_data);

	///////////////////
	// data systematics
	///////////////////

	// control regions
	// ZIHANG 
	SBin_data_lp5.inTree = new TChain(treeName);
	for(auto file:vectorize(m_config->GetValue("INF_data","")))
		SBin_data_lp5.inTree->Add(file);
	//SBin_data_lp5.name = "data1516";
	//SBin_data_lp5.name = "data18";
	SBin_data_lp5.name = "data15161718";
	SBin_data_lp5.type = "LP5";
	SBin_data_lp5.isMC = false;
	initializeInputTree(SBin_data_lp5);

	SBin_data_lp2.inTree = new TChain(treeName);
	for(auto file:vectorize(m_config->GetValue("INF_data","")))
		SBin_data_lp2.inTree->Add(file);
	//SBin_data_lp2.name = "data1516";
	//SBin_data_lp2.name = "data18";
	SBin_data_lp2.name = "data15161718";
	SBin_data_lp2.type = "LP2";
	SBin_data_lp2.isMC = false;
	initializeInputTree(SBin_data_lp2);
	//
	//SBin_data_gen.inTree = new TChain(treeName);
	//for(auto file:vectorize(m_config->GetValue("INF_data","")))
	//	SBin_data_gen.inTree->Add(file);
	//SBin_data_gen.name = "data15161718";
	//SBin_data_gen.type = "Gen";
	//SBin_data_gen.isMC = false;
	//initializeInputTree(SBin_data_gen);
	//
	//SBin_data_noFF.inTree = new TChain(treeName);
	//for(auto file:vectorize(m_config->GetValue("INF_data","")))
	//	SBin_data_noFF.inTree->Add(file);
	//SBin_data_noFF.name = "data15161718";
	//SBin_data_noFF.type = "noFF";
	//SBin_data_noFF.isMC = false;
	//initializeInputTree(SBin_data_noFF);

	///////////////////
	// initialize data sideband objects
	///////////////////
	initDataSideBand( SBin_data, "LoosePrime3" );
	initDataSideBand( SBin_data_lp5, "LoosePrime5" ); // Zihang systematics
	initDataSideBand( SBin_data_lp2, "LoosePrime2" );
	//initDataSideBand( SBin_data_gen, "Generator" );
	//initDataSideBand( SBin_data_noFF, "NoFudgeFactor" );


	///////////////////
	// nom MC
	///////////////////

	std::cout<<"Inizializzo MC"<<std::endl;
	SBin_mcNom.inTree = new TChain(treeName);
	for(auto file:vectorize(m_config->GetValue("INF_nomMC","")))
	{
		std::cout<<m_config->GetValue("INF_nomMC","")<<std::endl;
		TFile *f = TFile::Open(file);
		SBin_mcNom.infiles[file] = f;

		TH1D *cutflow = NULL;
		// Get noDalitz_weighted cutflow
		TIter nextiter(f->GetListOfKeys());
		TKey *key = NULL;
		while ((key = (TKey*)nextiter())) {
			TClass *cl = gROOT->GetClass(key->GetClassName());
			if (!cl->InheritsFrom("TH1")) continue;
			TH1 *h = (TH1*)key->ReadObj();
			if (h != NULL) {
				TString name(h->GetName());
				sum_of_weight=h->GetBinContent(1)/h->GetBinContent(2)*h->GetBinContent(3); 
				//std::cout<<"ZIHANG sum_of_weight = "<<sum_of_weight<<std::endl;
				if (name.Contains("noDalitz_weighted")) {
					if (cutflow != NULL) { // Found more than one!
						fatal(TString("Found more than one cutflow hist for file = " + file));
					}
					else
						cutflow = (TH1D*)h;
				}
			}
		}
		if ( cutflow  == NULL ) fatal(TString("Cannot find cutflow hist for file = " + file));
		else SBin_mcNom.cutFlow_orig.push_back(cutflow);
		SBin_mcNom.inTree->Add(file);
	}  
	SBin_mcNom.name = "Sherpa";
	SBin_mcNom.type = "LP3";
	SBin_mcNom.isMC = true;
	SBin_mcNom.catMC = "GG";
	initializeInputTree(SBin_mcNom);

	///////////////////
	// MC systematics
	///////////////////
	// control regions 

	SBin_mc_lp5.inTree = new TChain(treeName);
	for(auto file:vectorize(m_config->GetValue("INF_nomMC",""))){
		TFile *f = TFile::Open(file);
		SBin_mc_lp5.infiles[file] = f;

		TH1D *cutflow = NULL;
		// Get noDalitz_weighted cutflow
		TIter nextiter(f->GetListOfKeys());
		TKey *key = NULL;
		while ((key = (TKey*)nextiter())) {
			TClass *cl = gROOT->GetClass(key->GetClassName());
			if (!cl->InheritsFrom("TH1")) continue;
			TH1 *h = (TH1*)key->ReadObj();
			if (h != NULL) {
				TString name(h->GetName());

				if (name.Contains("noDalitz_weighted")) {
					if (cutflow != NULL) { // Found more than one!
						fatal(TString("Found more than one cutflow hist for file = " + file));
					}
					else
						cutflow = (TH1D*)h;
				}
			}
		}
		if ( cutflow  == NULL ) fatal(TString("Cannot find cutflow hist for file = " + file));
		else SBin_mc_lp5.cutFlow_orig.push_back(cutflow);

		SBin_mc_lp5.inTree->Add(file);
	}
	SBin_mc_lp5.name = "Sherpa";
	SBin_mc_lp5.type = "LP5";
	SBin_mc_lp5.isMC = true;
	SBin_mc_lp5.catMC = "GG";
	initializeInputTree(SBin_mc_lp5);


	////SBin_mc_lp3.inTree = new TChain(treeName);
	////for(auto file:vectorize(m_config->GetValue("INF_nomMC",""))){
	////	TFile *f = TFile::Open(file);
	////	SBin_mc_lp3.infiles[file] = f;
	////
	////	TH1D *cutflow = NULL;
	////	// Get noDalitz_weighted cutflow
	////	TIter nextiter(f->GetListOfKeys());
	////	TKey *key = NULL;
	////	while ((key = (TKey*)nextiter())) {
	////		TClass *cl = gROOT->GetClass(key->GetClassName());
	////		if (!cl->InheritsFrom("TH1")) continue;
	////		TH1 *h = (TH1*)key->ReadObj();
	////		if (h != NULL) {
	////			TString name(h->GetName());
	////
	////			if (name.Contains("noDalitz_weighted")) {
	////				if (cutflow != NULL) { // Found more than one!
	////					fatal(TString("Found more than one cutflow hist for file = " + file));
	////				}
	////				else
	////					cutflow = (TH1D*)h;
	////			}
	////		}
	////	}
	////	if ( cutflow  == NULL ) fatal(TString("Cannot find cutflow hist for file = " + file));
	////	else SBin_mc_lp3.cutFlow_orig.push_back(cutflow);
	////	SBin_mc_lp3.inTree->Add(file);
	////}
	////SBin_mc_lp3.name = "Sherpa";
	////SBin_mc_lp3.type = "LP3";
	////SBin_mc_lp3.isMC = true;
	////SBin_mc_lp3.catMC = "GG";
	////initializeInputTree(SBin_mc_lp3);

	SBin_mc_lp2.inTree = new TChain(treeName);
	for(auto file:vectorize(m_config->GetValue("INF_nomMC",""))){
		TFile *f = TFile::Open(file);
		SBin_mc_lp2.infiles[file] = f;
		TH1D *cutflow = NULL;
		// Get noDalitz_weighted cutflow
		TIter nextiter(f->GetListOfKeys());
		TKey *key = NULL;
		while ((key = (TKey*)nextiter())) {
			TClass *cl = gROOT->GetClass(key->GetClassName());
			if (!cl->InheritsFrom("TH1")) continue;
			TH1 *h = (TH1*)key->ReadObj();
			if (h != NULL) {
				TString name(h->GetName());

				if (name.Contains("noDalitz_weighted")) {
					if (cutflow != NULL) { // Found more than one!
						fatal(TString("Found more than one cutflow hist for file = " + file));
					}
					else
						cutflow = (TH1D*)h;
				}
			}
		}
		if ( cutflow  == NULL ) fatal(TString("Cannot find cutflow hist for file = " + file));
		else SBin_mc_lp2.cutFlow_orig.push_back(cutflow);
		SBin_mc_lp2.inTree->Add(file);
	}
	SBin_mc_lp2.name = "Sherpa";
	SBin_mc_lp2.type = "LP2";
	SBin_mc_lp2.isMC = true;
	SBin_mc_lp2.catMC = "GG";
	initializeInputTree(SBin_mc_lp2);

	// no fudge factor MC
	////SBin_mcFullSim.inTree = new TChain(treeName);
	////for(auto file:vectorize(m_config->GetValue("INF_nomMC.FS",""))){
	////	TFile *f = TFile::Open(file);
	////	SBin_mcFullSim.infiles[file] = f;
	////	TH1D *cutflow = NULL;
	////	// Get noDalitz_weighted cutflow
	////	TIter nextiter(f->GetListOfKeys());
	////	TKey *key = NULL;
	////	while ((key = (TKey*)nextiter())) {
	////		TClass *cl = gROOT->GetClass(key->GetClassName());
	////		if (!cl->InheritsFrom("TH1")) continue;
	////		TH1 *h = (TH1*)key->ReadObj();
	////		if (h != NULL) {
	////			TString name(h->GetName());
	////
	////			if (name.Contains("noDalitz_weighted")) {
	////				if (cutflow != NULL) { // Found more than one!
	////					fatal(TString("Found more than one cutflow hist for file = " + file));
	////				}
	////				else
	////					cutflow = (TH1D*)h;
	////			}
	////		}
	////	}
	////	if ( cutflow  == NULL ) fatal(TString("Cannot find cutflow hist for file = " + file));
	////
	////	SBin_mcFullSim.cutFlow_orig.push_back(cutflow);
	////	SBin_mcFullSim.inTree->Add(file);
	////}
	////SBin_mcFullSim.name = "Sherpa";
	////SBin_mcFullSim.type = "FullSim";
	////SBin_mcFullSim.isMC = true;
	////SBin_mcFullSim.catMC = "GG";
	////initializeInputTree(SBin_mcFullSim);
	////// syst MC
	////SBin_mcSyst.inTree = new TChain(treeName);
	////for(auto file:vectorize(m_config->GetValue("INF_systMC",""))){
	////	TFile *f = TFile::Open(file);
	////	SBin_mcSyst.infiles[file] = f;
	////
	////	TH1D *cutflow = NULL;
	////	// Get noDalitz_weighted cutflow
	////	TIter nextiter(f->GetListOfKeys());
	////	TKey *key = NULL;
	////	while ((key = (TKey*)nextiter())) {
	////		TClass *cl = gROOT->GetClass(key->GetClassName());
	////		if (!cl->InheritsFrom("TH1")) continue;
	////		TH1 *h = (TH1*)key->ReadObj();
	////		if (h != NULL) {
	////			TString name(h->GetName());
	////
	////			if (name.Contains("noDalitz_weighted")) {
	////				if (cutflow != NULL) { // Found more than one!
	////					fatal(TString("Found more than one cutflow hist for file = " + file));
	////
	////				}
	////				else
	////					cutflow = (TH1D*)h;
	////			}
	////		}
	////	}
	////	if ( cutflow  == NULL ) fatal(TString("Cannot find cutflow hist for file = " + file));
	////	else SBin_mcSyst.cutFlow_orig.push_back(cutflow);
	////
	////
	////	SBin_mcSyst.inTree->Add(file);
	////}
	////SBin_mcSyst.name = "Pythia";
	////SBin_mcSyst.type = "LP3";
	////SBin_mcSyst.isMC = true;
	////SBin_mcSyst.catMC = "GG";
	////initializeInputTree(SBin_mcSyst);

	///////////////////
	// initialize mc sideband objects
	///////////////////

	existingMCInputs = m_config->GetValue("UseExistingMCResults",0);
	if(!existingMCInputs){

		initMCSideBand( SBin_mcNom, "LoosePrime3" );
		initMCSideBand( SBin_mc_lp5, "LoosePrime5" );   // Zihang systematics
		initMCSideBand( SBin_mc_lp2, "LoosePrime2" );
		//initMCSideBand( SBin_mcSyst, "Generator" );
		//initMCSideBand( SBin_mcFullSim, "NoFudgeFactor" );

	}else{ // not working... yet!
		/*
		//f = TFile::Open(m_config->GetValue("InfileMC",""));  
		initMCSideBandFromFile( SBin_mcNom, SBin_mcNom.f, "LoosePrime3" );
		initMCSideBandFromFile( SBin_mc_lp5, SBin_mc_lp5.f, "LoosePrime5" );
		initMCSideBandFromFile( SBin_mc_lp2, SBin_mc_lp2.f, "LoosePrime2" );
		initMCSideBandFromFile( SBin_mcSyst, SBin_mcSyst.f, "Generator" );
		initMCSideBandFromFile( SBin_mcFullSim, SBin_mcFullSim.f,"NoFudgeFactor" );
		*/
	}

	///////////////////
	// initialize output file
	///////////////////

	m_fout = new TFile(m_config->GetValue("OutputFile","unknown_file_name.root"),"recreate");
	//m_fout = new TFile(getenv("OutputFile"),"recreate");
	m_fout->mkdir("MC");
	m_fout->mkdir("Data");

	return EL::StatusCode::SUCCESS;

}

EL::StatusCode DiPhotonBkg2x2DSB :: SolveData(SBInputs &inSBdata, SBInputs &inSBtruth)
{

	cout<<"=========================================================="<<endl;
	cout<<"                    SOLVING DATA                          "<<endl;
	cout<<"=========================================================="<<endl;

	// Setting leakage and correlation factors
	for(int i=0; i<(int)histo_names.size(); i++){
		cout<<"------------------------------------------->> solving histogram:"
			<<histo_names[i]<<endl;
		cout<<"-------------------------------------------"<<endl;
		SB2x2D *sb = inSBdata.SB_data[i];
		SB2x2DTruth *sb_truth = inSBtruth.SB_truth[i];
		if(sb_truth) { // From SB Truth object
			// And bin per bin
			cout<<"-------------------------------------------"<<endl;
			cout<<"Setting efficiency parameters from SB2x2D Truth: "
				<<sb_truth->GetName()<<endl;
			cout<<"-------------------------------------------"<<endl;
			//cout<<"======= ZIHANG getting truth value ======="<<endl;
			for(int ip=0;ip<NPARAMS;ip++) {
				double x = sb_truth->GetTruthValue(ip);
				//cout<<sb_truth->GetTruthValue(ip)<<endl;
				double e = sb_truth->GetTruthError2(ip);
				if(e>0) {
					sb->SetParameterAndError2(ip,x,e);
				}
			}
		}else cout << "No MC side band --> input parameters could not be set" << endl;
		cout<<"ABOUT to SOLVE Data " <<endl; 

		// When changing method inputs you have to:
		// - switch to XIJJ=1 if you want the SIMPLEST of the SIMPLEST 2x2DSB (like the one used in early 2011)
		// - other parameters don't have to be commented they will not be read if not used
		sb->SetInputMode(inSBdata.sol_mode);
		sb->Solve();
		cout<<"-------------------------------------------"<<endl;
		cout<<"Solving data sideband : "<<sb->GetName()<<endl;
		sb->Print("pred results TITI");
		cout<<"-------------------------------------------"<<endl;
	}
	return EL::StatusCode::SUCCESS;
}

//CollectionTree->Draw("HGamEventInfoAuxDyn.m_yy",
//"HGamEventInfoAuxDyn.isPassedRelPtCuts &&
//HGamEventInfoAuxDyn.isPassedIsolation  &&
//HGamEventInfoAuxDyn.isPassedMassCut    &&
//HGamEventInfoAuxDyn.isPassedPID        &&
//HGamEventInfoAuxDyn.isPassedTriggerMatch"


	template <typename SB >
EL::StatusCode DiPhotonBkg2x2DSB :: fillSideBand (SBInputs &inSB, SB &SideBands, Str fillType)
{

	const unsigned int PhotonLoosePrime5 = egammaPID::PhotonLoose |
		0x1 << egammaPID::ClusterStripsEratio_Photon | // not the normal "Eratio" (this is f1 = E(layer1)/E(total), needed to have a meaningful measurement of the shower shape in the strips
		0x1 << egammaPID::ClusterStripsDeltaEmax2_Photon | // never failed in isEMTight anymore, but added since it's included in tight |
		0x1 << egammaPID::ClusterStripsDEmaxs1_Photon; // Eratio

	const unsigned int PhotonLoosePrime4 = PhotonLoosePrime5;

	const unsigned int PhotonLoosePrime3 = PhotonLoosePrime4 | 
		0x1 << egammaPID::ClusterStripsWtot_Photon;

	const unsigned int PhotonLoosePrime2 = PhotonLoosePrime3 | // also require "DeltaE" ("w3" and "Fside" are only remaining requirements for tight)
		0x1 << egammaPID::ClusterStripsDeltaE_Photon;

	uint isEMloose = PhotonLoosePrime3;
	if(fillType=="LoosePrime2") isEMloose = PhotonLoosePrime2;
	if(fillType=="LoosePrime5") isEMloose = PhotonLoosePrime5;

	//cout<<"egammaPID::PhotonLoose "<<egammaPID::PhotonLoose<<endl;
	//cout<<"egammaPID::ClusterStripsEratio_Photon "<<egammaPID::ClusterStripsEratio_Photon<<endl;
	//cout<<"egammaPID::ClusterStripsDeltaEmax2_Photon "<<egammaPID::ClusterStripsDeltaEmax2_Photon<<endl;
	//cout<<"egammaPID::ClusterStripsDEmaxs1_Photon "<<egammaPID::ClusterStripsDEmaxs1_Photon<<endl;
	//cout<<"egammaPID::ClusterStripsWtot_Photon "<<egammaPID::ClusterStripsWtot_Photon<<endl;
	//cout<<"egammaPID::ClusterStripsDeltaE_Photon "<<egammaPID::ClusterStripsDeltaE_Photon<<endl;
	//cout<<"Zihang LP5= "<<PhotonLoosePrime5<<" LP3= "<<PhotonLoosePrime3<<endl;


	int n = inSB.inTree->GetEntries(); 
	//int n=100;
	cout<<"\\/\\/\\/\\/\\/ Filling SB :: "<<inSB.name<<" -- fill type :: "<<fillType<<endl;
	cout<<"\\/\\/\\/\\/\\/ Running over tree with "<<n<<" entries\n"<<endl;

	TTimeStamp mytime; mytime.Set();
	double previous = mytime.GetSec()+mytime.GetNanoSec()/(1.0e9);
	int treenum_temp = 0;


	//for(int i=0;i<50000;i++) {
	for(int i=0;i<n;i++) {

		//std::cout<<"*************************************************NEVENT = "<<i<<std::endl;

		//================== progress bar =================
		/////if(i && ((i%100000)==0 || i==n-1)){
		/////	mytime.Set();
		/////	double current = mytime.GetSec()+mytime.GetNanoSec()/(1.0e9);
		/////	std::cout << "Number of events processed/total: " << i <<
		/////		" / " << n << " Process time = " << current-previous <<
		/////		" seconds" << std::endl;
		/////	previous = current;
		/////}

		if ( treenum_temp < inSB.inTree->GetTreeNumber() ) {
			Str filename = inSB.inTree->GetFile()->GetName();
			cerr<<" --------------------> file name :: "<<filename<<endl;
			cerr<<" ---> tree num :: "<<treenum_temp<<endl;
			treenum_temp = inSB.inTree->GetTreeNumber();
			cerr<<" ---> tree num now :: "<<treenum_temp<<endl;
		}

		inSB.inTree->GetEntry(i);

		uint gam1_isTight = inSB.y1_isEMTight;
		uint gam2_isTight = inSB.y2_isEMTight;
		if(inSB.isMC && fillType=="NoFudgeFactor"){  
			gam1_isTight = inSB.y1_isEMTight_noFF;
			gam2_isTight = inSB.y2_isEMTight_noFF;
		}


		// The tight flag is already in the ntuple -->  Check LP only
		inSB.cutFlow->Fill(0.5);
		bool loose1 = !(gam1_isTight & isEMloose); if(!loose1) continue;
		bool loose2 = !(gam2_isTight & isEMloose); if(!loose2) continue;


		// for MC assume signal... for now
		inSB.pair_IsSignal = 1;

		// Set isolation for sideband
		inSB.y1_IsIso =  (inSB.y1_topoetcone20<inSB.y1_pt*0.065 &&
				inSB.y1_ptcone20<inSB.y1_pt*0.05 )?1:0; 
		inSB.y2_IsIso =  (inSB.y2_topoetcone20<inSB.y2_pt*0.065 &&
				inSB.y2_ptcone20<inSB.y2_pt*0.05 )?1:0; 

		//====================== yybb selections ==========================
		inSB.cutFlow->Fill(1.5);
		//if (!inSB.isPassedTriggerMatch) continue;
		inSB.cutFlow->Fill(2.5);
		//if (!inSB.isPassedJetEventClean) continue;
		inSB.cutFlow->Fill(3.5);
		if (!inSB.isPassedRelPtCuts) continue;
		inSB.cutFlow->Fill(4.5);
		if (!inSB.isPassedMassCut) continue;
		inSB.cutFlow->Fill(5.5);


		if (inSB.yybb_btag77_BCal_cutFlow!=6) continue; /// !!! Also change the weight !!!
		inSB.cutFlow->Fill(6.5);
		if (inSB.N_j_b77 <2) continue;
		inSB.cutFlow->Fill(7.5);

		//=================  Non-resonant selections ==================
		//if (inSB.iMap["yybb_nonRes_XGBoost_btag77_BCal_Cat"]/1000%10==9) continue;

		//if (inSB.iMap["yybb_nonRes_XGBoost_btag77_BCal_Cat"]/1000%10==1) inSB.cutFlow->Fill(8.5);
		//if (inSB.iMap["yybb_nonRes_XGBoost_btag77_BCal_Cat"]/1000%10==2) inSB.cutFlow->Fill(9.5);
		//if (inSB.iMap["yybb_nonRes_XGBoost_btag77_BCal_Cat"]/1000%10==3) inSB.cutFlow->Fill(10.5);
		//if (inSB.iMap["yybb_nonRes_XGBoost_btag77_BCal_Cat"]/1000%10==4) inSB.cutFlow->Fill(11.5);

		//=================  Resonant selections ==================
		double c1 = 0.65;
		double c2 = 1-c1;
		double Res_BDT_final = 1/(TMath::Sqrt(TMath::Power(c1,2)+TMath::Power(c2,2))) * TMath::Sqrt(TMath::Power(c1*(inSB.BDT_BCal_yy_Score+1)/2,2)+TMath::Power(c2*(inSB.BDT_BCal_ttH_Score+1)/2,2));

		Str MassPoint = m_config->GetValue("MassPoint","350GeV");
		double Mean=346.891;
		double RMS = 9.42102;
		double BDT_cut=0.85;
		if (MassPoint.Contains("260")) {
			Mean=260.012;
			RMS =3.13066;
			BDT_cut=0.75;
		}
		else if (MassPoint.Contains("251")) {
			Mean=249.075;
			RMS =3.47843;
			BDT_cut=0.7;
		}
		else if (MassPoint.Contains("270")) {
			Mean=269.762;
			RMS =4.14833;
			BDT_cut=0.8;
		}
		else if (MassPoint.Contains("280")) {
			Mean=279.201;    
			RMS =5.04804;
			BDT_cut=0.85;
		}	
		else if (MassPoint.Contains("290")) {
			Mean=288.958;    
			RMS =6.20036;
			BDT_cut=0.85;
		}	
		else if (MassPoint.Contains("300")) {
			Mean=298.577;    
			RMS =6.70871;
			BDT_cut=0.85;
		}	
		else if (MassPoint.Contains("3125")) {
			Mean=310.85;    
			RMS =7.77032;
			BDT_cut=0.85;
		}	
		else if (MassPoint.Contains("325")) {
			Mean=323.806;    
			RMS =7.449;
			BDT_cut=0.85;
		}	
		else if (MassPoint.Contains("3375")) {
			Mean=336.07;    
			RMS =8.02539;
			BDT_cut=0.85;
		}	
		else if (MassPoint.Contains("350")) {
			Mean=348.32;    
			RMS =8.66209;
			BDT_cut=0.85;
		}	
		else if (MassPoint.Contains("375")) {
			Mean=372.906;    
			RMS =9.75946;
			BDT_cut=0.9;
		}	
		else if (MassPoint.Contains("400")) {
			Mean=397.394;    
			RMS =10.719;
			BDT_cut=0.8;
		}	
		else if (MassPoint.Contains("425")) {
			Mean=422.18;    
			RMS =11.6489;
			BDT_cut=0.85;
		}	
		else if (MassPoint.Contains("450")) {
			Mean=446.732;    
			RMS =12.4305;	
			BDT_cut=0.85;
		}	
		else if (MassPoint.Contains("475")) {
			Mean=471.277;    
			RMS =13.3335;
			BDT_cut=0.8;
		}	
		else if (MassPoint.Contains("500")) {
			Mean=495.738;    
			RMS =14.1286;
			BDT_cut=0.75;
		}	
		else if (MassPoint.Contains("550")) {
			Mean=544.868;    
			RMS =15.6412;
			BDT_cut=0.6;
		}	
		else if (MassPoint.Contains("600")) {
			Mean=594.493;    
			RMS =16.6684;
			BDT_cut=0.45;
		}	
		else if (MassPoint.Contains("700")) {
			Mean=693.149;    
			RMS =18.8885;
			BDT_cut=0.2;
		}	
		else if (MassPoint.Contains("800")) {
			Mean=790.638;    
			RMS =21.3055;
			BDT_cut=0.1;
		}	
		else if (MassPoint.Contains("900")) {
			Mean=888.031;    
			RMS =23.4402;
			BDT_cut=0.2;
		}	
		else if (MassPoint.Contains("1000")) {
			Mean=983.98;    
			RMS =26.4073;
			BDT_cut=0.05;
		}	

		if (Res_BDT_final<=BDT_cut) continue;
		inSB.cutFlow->Fill(6.5);
		//if (MassPoint.Contains("1000") || MassPoint.Contains("900") ) {
		//	if (inSB.BCal_m_yyjj_tilde/1000. <= Mean - 4*RMS) continue;
		//	if (inSB.BCal_m_yyjj_tilde/1000. >= Mean + 4*RMS) continue;
		//}
		//else {
		//	if (inSB.BCal_m_yyjj_tilde/1000. <= Mean - 2*RMS) continue;
		//	if (inSB.BCal_m_yyjj_tilde/1000. >= Mean + 2*RMS) continue;
		//}
		inSB.cutFlow->Fill(7.5);
		//if (inSB.BCal_m_jj <= 80000.) continue;
		//if (inSB.BCal_m_jj >= 140000.) continue;
		inSB.cutFlow->Fill(8.5);
		if (inSB.fMap["m_yy"] <= 105000.) continue; // already applied as MassCut
		if (inSB.fMap["m_yy"] >= 160000.) continue;
		inSB.cutFlow->Fill(9.5);
		

		// fill 2x2D Sideband objects
		for(int ih=0; ih<(int)histo_names.size(); ih++){

			Str hname = histo_names[ih];
			//std::cout<<"PRINT OUT hname ="<<hname<<"***************************"<<std::endl;      
			// // Weight
			float fill_val = -999.;
			float w = 1.;
			if(inSB.isMC){
				if (inSB.runNumber == 284500) {	
					w *= inSB.crossSectionBRfilterEff * inSB.Lumi * inSB.weight / (1.0466197e+08 / 72958848. * 72995456.); // Sum_of_Weight h026
				}
				else if (inSB.runNumber == 300000) {
					w *= inSB.crossSectionBRfilterEff * inSB.Lumi* inSB.weight / ( 1.3104286e+08 / 90140264. * 90295200.);
				}
				else if (inSB.runNumber == 310000) {	
					w *= inSB.crossSectionBRfilterEff * inSB.Lumi* inSB.weight / ( 1.7252878e+08 / 1.2434026e+08  * 1.2459507e+08 );
				}
				//============== yybb weight =====================
				w *= inSB.yybb_weight ;

				//============= h026 FJvt weight ================
				w *= inSB.weightFJvt ;

			}

			if( hname == "m_yy" || hname == "inclusive" ||  hname == "yybb_nonRes_XGBoost_btag77_BCal_Cat" || hname == "yybb_nonRes_XGBoost_btag77_85_BCal_Cat" ) {

				if ( hname == "m_yy" ||  hname == "inclusive") {	  
					fill_val = (float)inSB.fMap[histo_vars[hname]]/var_Scale[histo_vars[hname]];
				}

				if (hname.Contains("yybb_nonRes_XGBoost_btag77_BCal_Cat") ) {
					fill_val = inSB.iMap["yybb_nonRes_XGBoost_btag77_BCal_Cat"]/1000%10;
					//cout<<"fill_val= "<<fill_val<<endl;
				}

				if (hname.Contains("yybb_nonRes_XGBoost_btag77_85_BCal_Cat") ) {
					fill_val = inSB.iMap["yybb_nonRes_XGBoost_btag77_85_BCal_Cat"]/1000%10;
				}

				if(fill_val != -999.){
					//cout<<"Zihang fill_var "<<fill_val<<endl;
					SideBands[ih]->Fill(fill_val,w);
				}
			}
			else if(histo_2dSel[hname]!=""){

				fill_val = inSB.fMap[histo_vars[hname]]/var_Scale[histo_vars[hname]];

				/////////////////////////////////////////////////////////////////////////
				if(histo_2dSel[hname]=="N_cateAlan=1" && inSB.iMap["N_cateAlan"]==1){ 
					std::cout<<"N_cateAlan_1tag_mgg_5GeV"<<histo_2dSel[hname]<<std::endl;
					fill_val = (float)inSB.fMap["m_yy"]/1000.;
					SideBands[ih]->Fill(fill_val,w);
				}
				if(histo_2dSel[hname]=="N_cateAlan=2" && inSB.iMap["N_cateAlan"]==2)
					SideBands[ih]->Fill(fill_val,w);
				if(histo_2dSel[hname]=="N_cateAlan=3" && inSB.iMap["N_cateAlan"]==3)
					SideBands[ih]->Fill(fill_val,w);
				if(histo_2dSel[hname]=="N_cateAlan=4" && inSB.iMap["N_cateAlan"]==4)
					SideBands[ih]->Fill(fill_val,w);
				///////////////////////////////////////////////////////////////////////////

				if(histo_2dSel[hname]=="N_j30_0" && inSB.iMap[histo_vars["N_j_30"]]==0)
					SideBands[ih]->Fill(fill_val,w);
				if(histo_2dSel[hname]=="N_j30_1" && inSB.iMap[histo_vars["N_j_30"]]==1)
					SideBands[ih]->Fill(fill_val,w);
				if(histo_2dSel[hname]=="N_j30_2" && inSB.iMap[histo_vars["N_j_30"]]==2)
					SideBands[ih]->Fill(fill_val,w);
				if(histo_2dSel[hname]=="N_j30_3" && inSB.iMap[histo_vars["N_j_30"]]>=3)
					SideBands[ih]->Fill(fill_val,w);

				if(histo_2dSel[hname]=="cosTS_yy_1" && 
						(inSB.fMap[histo_vars["cosTS_yy"]]>=0.0 && inSB.fMap[histo_vars["cosTS_yy"]]<0.5) )
					SideBands[ih]->Fill(fill_val,w);

				if(histo_2dSel[hname]=="cosTS_yy_2" && 
						(inSB.fMap[histo_vars["cosTS_yy"]]>=0.5 && inSB.fMap[histo_vars["cosTS_yy"]]<1.0) )
					SideBands[ih]->Fill(fill_val,w);

				///////////////////////////////////////////////////////////////////////////

				if(histo_2dSel[hname].Contains("catCoup")&&!histo_2dSel[hname].Contains("catCoupBDT"))
					if( inSB.iMap["catCoup_Moriond2017"] == histo_2dSel_catIndex[hname][0] ) 	   
						SideBands[ih]->Fill(fill_val,w);
				//cout<<" ------- > "<<histo_2dSel[hname]<<endl;
				if(histo_2dSel[hname].Contains("catCoupBDT")){
					//cout<<"here"<<endl;
					for ( int bin:histo_2dSel_catIndex[hname] ){
						//cout<<" bin "<<bin<<endl; 
						if( inSB.iMap["catCoup_Moriond2017BDT"] == bin ) {
							SideBands[ih]->Fill(fill_val,w);
						}
					}
				}
				///////////////////////////////////////////////////////////////////////////

			}
			else{

				float fill_val = inSB.fMap[histo_vars[hname]];
				if(fill_val>-1) fill_val /= var_Scale[histo_vars[hname]];

				if(hname.Contains("Dphi_yy_jj")&&fill_val!=-99.) 
					fill_val = TMath::Pi() - fabs ( fill_val );

				if ( hname.Contains("pT_j2") ) {
					fill_val = inSB.iMap[histo_vars["N_j_30"]] == 0 ? -99.0 
						: inSB.iMap[histo_vars["N_j_30"]] == 1 ? 1.0 : fill_val;
					//if ( inSB.iMap[histo_vars["N_j_30"]] == 0 ) fill_val = -99.0;
					//if ( inSB.iMap[histo_vars["N_j_30"]] == 1 ) fill_val = 1.0;
				}
				if ( hname.Contains("pT_j3") ) {
					if ( inSB.iMap[histo_vars["N_j_30"]] < 2 ) fill_val = -99.0;
					if ( inSB.iMap[histo_vars["N_j_30"]] == 2 ) fill_val = 1.0;
				}

				if ( hname.Contains("Dy_j_j") ) {
					fill_val = inSB.iMap[histo_vars["N_j_30"]] < 2 ? -99.0 : fill_val;
				}


				//cout<<" value : "<<inSB.fMap[histo_vars[hname]]<<endl;
				//cout<<" scale : "<<var_Scale[histo_vars[hname]]<<endl;
				//cout<<" fill val : "<<fill_val<<endl;	
				SideBands[ih]->Fill(fill_val,w);

			}
		}

	}
	inSB.cutFlow->Print("all");
	//std::cout<<"**********************************************************NTOY INSIDE = "<<seed<<std::endl; 

	return EL::StatusCode::SUCCESS;
}



EL::StatusCode DiPhotonBkg2x2DSB :: execute ()
{
	////////////////////////
	// fill then solve 2x2D sidebands
	////////////////////////
	// nominal
	fillSideBand (SBin_mcNom,SBin_mcNom.SB_truth,"LoosePrime3");
	WriteMCResults(SBin_mcNom);
	fillSideBand (SBin_data,SBin_data.SB_data,"LoosePrime3");
	SolveData(SBin_data,SBin_mcNom);    

	//abort();
	// lp5
	fillSideBand (SBin_mc_lp5,SBin_mc_lp5.SB_truth,"LoosePrime5");
	WriteMCResults(SBin_mc_lp5);
	fillSideBand (SBin_data_lp5,SBin_data_lp5.SB_data,"LoosePrime5");
	SolveData(SBin_data_lp5,SBin_mc_lp5);
	// lp2
	fillSideBand (SBin_mc_lp2,SBin_mc_lp2.SB_truth,"LoosePrime2");
	WriteMCResults(SBin_mc_lp2);
	fillSideBand (SBin_data_lp2,SBin_data_lp2.SB_data,"LoosePrime2");
	SolveData(SBin_data_lp2,SBin_mc_lp2);
	//// generator
	//fillSideBand (SBin_mcSyst,SBin_mcSyst.SB_truth,"LoosePrime3");  
	//WriteMCResults(SBin_mcSyst);
	//fillSideBand (SBin_data_gen,SBin_data_gen.SB_data,"LoosePrime3");
	//SolveData(SBin_data_gen,SBin_mcSyst);
	//// full sim / no fudge factors
	//fillSideBand (SBin_mcFullSim,SBin_mcFullSim.SB_truth,"NoFudgeFactor");
	//WriteMCResults(SBin_mcFullSim);  
	//fillSideBand (SBin_data_noFF,SBin_data_noFF.SB_data,"LoosePrime3");
	//SolveData(SBin_data_noFF,SBin_mcFullSim);

	////////////////////////
	// save cutflows
	////////////////////////


	m_fout->cd();
	SBin_data.cutFlow->Write();
	SBin_data_lp5.cutFlow->Write();
	SBin_data_lp2.cutFlow->Write();
	//SBin_data_gen.cutFlow->Write();
	//SBin_data_noFF.cutFlow->Write();
	SBin_mcNom.cutFlow->Write();
	SBin_mc_lp5.cutFlow->Write();
	SBin_mc_lp2.cutFlow->Write();
	//SBin_mcSyst.cutFlow->Write();
	//SBin_mcFullSim.cutFlow->Write();
	/*
	   delete  SBin_data.cutFlow;
	   delete SBin_data_lp5.cutFlow;
	   delete SBin_data_lp3.cutFlow;
	   delete SBin_data_lp2.cutFlow;
	   delete SBin_data_gen.cutFlow;
	   delete SBin_data_noFF.cutFlow;
	   delete SBin_mcNom.cutFlow;
	   delete SBin_mc_lp5.cutFlow;
	   delete SBin_mc_lp3.cutFlow;
	   delete SBin_mc_lp2.cutFlow;
	   delete SBin_mcSyst.cutFlow;
	   delete SBin_mcFullSim.cutFlow;
	   */
	gROOT->cd();


	////////////////////////
	// evaluate systematics
	////////////////////////

	Str MassPoint = m_config->GetValue("MassPoint","350GeV");
	ofstream *out = new ofstream(MassPoint+"testOUT.tex", ios::out );

	for(int ih=0; ih<(int)histo_names.size(); ih++){

		// set nominal result
		SB2x2D *sb = SBin_data.SB_data[ih];
		SB2x2D *sb_temp = SBin_data.SB_data[ih];

		Str name = sb->GetName();
		bool printLast = true;
		if( name.Contains("mgg") || name.Contains("m_yy") ) printLast = false;

		// Initialize must be done before summing any systematic
		// ZIHANG 
		sb->InitSystematics(); sb_temp->InitSystematics();

		vector<SB2x2D*> SB2, SB2_temp;    

		//////////////////////////////////////////////
		//////////////////////////////////////////////
		/// ZIHANG LP5
		cout<<"ADDING SYSTEMATIC: LP5"<<endl;
		SB2.push_back(SBin_data_lp5.SB_data[ih]);
		SB2_temp.push_back(SBin_data_lp5.SB_data[ih]);

		sb_temp->AddSystematics(SB2_temp,0);

		if(printLast)
			sb_temp->PrintLastSystematics("TITI",out,"LoosePrime5");
		saveSystGraphs(sb_temp,"LoosePrime5");
		SB2_temp.clear();

		//////////////////////////////////////////////
		//// ZIHANG LP2
		cout<<"ADDING SYSTEMATIC: LP2"<<endl;
		SB2.push_back(SBin_data_lp2.SB_data[ih]);
		SB2_temp.push_back(SBin_data_lp2.SB_data[ih]);

		sb_temp->AddSystematics(SB2_temp,0);

		if(printLast)
			sb_temp->PrintLastSystematics("TITI",out,"LoosePrime2");
		saveSystGraphs(sb_temp,"LoosePrime2");
		SB2_temp.clear();

		//////////////////////////////////////////////

		//cout<<"ADDING SYSTEMATIC: Control Region"<<endl;
		//sb->AddSystematics(SB2,0);
		//if(printLast)
		//	sb->PrintLastSystematics("TITI",out,"CRdefinition");
		//saveSystGraphs(sb,"CRdefinition");
		//SB2.clear();
		//SB2_temp.clear();

		/////////////////////////////////////////////////
		///////////////////////////////////////////////////

		//cout<<"ADDING SYSTEMATIC: Generator"<<endl;
		//SB2.push_back(SBin_data_gen.SB_data[ih]);
		//SB2_temp.push_back(SBin_data_gen.SB_data[ih]);
		//sb_temp->AddSystematics(SB2_temp,0);
		//if(printLast) sb_temp->PrintLastSystematics("TITI",out,"Generator");
		//saveSystGraphs(sb_temp,"Generator");
		//SB2_temp.clear();

		//cout<<"ADDING SYSTEMATIC: Generator"<<endl;
		//sb->AddSystematics(SB2,1);
		//if(printLast) sb->PrintLastSystematics("TITI",out,"Generator");
		//saveSystGraphs(sb,"Generator");
		//SB2.clear();

		////////////////////////////////////////////////
		////////////////////////////////////////////////

		//cout<<"ADDING SYSTEMATIC: NoFudgeFactor"<<endl;
		//SB2.push_back(SBin_data_noFF.SB_data[ih]);
		//SB2_temp.push_back(SBin_data_noFF.SB_data[ih]);
		//sb_temp->AddSystematics(SB2_temp,0);
		//if(printLast)
		//	sb_temp->PrintLastSystematics("TITI",out,"NoFudgeFactor");
		//saveSystGraphs(sb_temp,"NoFudgeFactor");
		//SB2_temp.clear();

		//cout<<"ADDING SYSTEMATIC: NoFudgeFactor"<<endl;
		//sb->AddSystematics(SB2,1);
		//if(printLast)
		//	sb->PrintLastSystematics("TITI",out,"NoFudgeFactor");
		//saveSystGraphs(sb,"NoFudgeFactor");
		//SB2.clear();

		/*
		 *  Combining systematics
		 */

		//// ZIHANG
		cout<<"ADDING SYSTEMATICS TOT"<<endl;
		sb->EndSystematics();
		if(printLast) sb->PrintSystematics("TITI",out,"total");
		saveAllGraphs(sb);

	}

	return EL::StatusCode::SUCCESS;
}


EL::StatusCode DiPhotonBkg2x2DSB :: finalize ()
{
	return EL::StatusCode::SUCCESS;
}


EL::StatusCode DiPhotonBkg2x2DSB :: WriteMCResults (SBInputs &inSB)
{
	m_fout->cd("MC");

	cout<<"=========================================================="<<endl;
	cout<<"                    PRINTING MC SIDEBAND                  "<<endl;
	cout<<"=========================================================="<<endl;

	for(int ihist=0;ihist<(int)histo_names.size(); ihist++) {
		SB2x2DTruth *sb = inSB.SB_truth[ihist];
		cout<<"-------------------------------------------"<<endl;
		cout<<" >>>>>>>  Histo name: "<<histo_names[ihist]<<endl;
		cout<<" >>>>>>>  Sideband object name: "<<sb->GetName()<<endl;
		cout<<"-------------------------------------------"<<endl;
		sb->Print(SBin_mcNom.catMC+" truth raw");
		sb->Write("",TObject::kOverwrite);
		for(int ip=E1I;ip<=E2T;ip++) {
			TH1D * h = sb->GetTruthHist(ip);
			if(h) h->Write("",TObject::kOverwrite);
		}
		for(int ip=EP1I;ip<=EP2T;ip++) {
			TH1D * h = sb->GetTruthHist(ip);
			if(h) h->Write("",TObject::kOverwrite);
		}
		for(int ip=X1G;ip<=X2G;ip++) {
			TH1D * h = sb->GetTruthHist(ip);
			if(h) h->Write("",TObject::kOverwrite);
		}
		for(int ip=XIGG;ip<=XIGG;ip++) {
			TH1D * h = sb->GetTruthHist(ip);
			if(h) h->Write("",TObject::kOverwrite);
		}
		for(int ip=XTGG;ip<=XTGG;ip++) {
			TH1D * h = sb->GetTruthHist(ip);
			if(h) h->Write("",TObject::kOverwrite);
		}
	}
	cout<<"=========================================================="<<endl;
	cout<<"=========================================================="<<endl;
	cout<<"=========================================================="<<endl;
	return EL::StatusCode::SUCCESS;
}


TGraphAsymmErrors* NormalizeGraphBinWidth(TGraphAsymmErrors *g , TH1* h)
{
	TGraphAsymmErrors *gtmp = g;
	g = (TGraphAsymmErrors*)g->Clone();
	for(int i=0;i<gtmp->GetN();i++){
		double width = h->GetBinWidth(i+1);
		g->SetPoint(i,gtmp->GetX()[i],gtmp->GetY()[i]/width);
		g->SetPointEYhigh(i,gtmp->GetEYhigh()[i]/width);
		g->SetPointEYlow(i,gtmp->GetEYlow()[i]/width);
	}
	return g;
}


TH1D* NormalizeHistoBinWidth(TH1D *h)
{
	TH1D *htmp = h;
	h = (TH1D *) h->Clone();
	for(int i=0;i<=htmp->GetNbinsX()+1;i++){
		double width = htmp->GetBinWidth(i);
		h->SetBinContent(i,htmp->GetBinContent(i)/width);
		h->SetBinError(i,htmp->GetBinError(i)/width);
	}
	return h;
}


EL::StatusCode DiPhotonBkg2x2DSB :: saveSystGraphs (SB2x2D *sb, Str systN)
{
	m_fout->cd("Data");
	for(int i=WGGTITI;i<=PURITYTITI;i++) {
		TH1D *h               = sb->GetHist(i);
		TGraphAsymmErrors* g  = sb->GetLastSystematics(i);
		Str gname         = g->GetName();
		g->SetName( Form("%s_%s",g->GetName(),systN.Data()));
		gname.ToLower();
		if(doRenormalize &&
				gname.Contains("w") &&
				!gname.Contains("low") &&
				!gname.Contains("fwd") )
			g = NormalizeGraphBinWidth(g,h);
		g->Write("",TObject::kOverwrite);
	}
	return EL::StatusCode::SUCCESS;
}


EL::StatusCode DiPhotonBkg2x2DSB :: saveAllGraphs(SB2x2D *sb)
{
	m_fout->cd("Data");
	for(int i=WGGTITI;i<=PURITYTITI;i++) {
		int nAverage = 0;
		TGraphAsymmErrors* g  = sb->GetSystematics(i,nAverage,false); // Syst only
		TGraphAsymmErrors* g2 = sb->GetSystematics(i,nAverage,true); // Stat+Syst
		TGraphAsymmErrors* g3 = sb->GetError(i); // Stat only
		TH1D *h               = sb->GetHist(i); 
		Str gname         = g->GetName();
		gname.ToLower();
		if(doRenormalize && gname.Contains("w") &&
				!gname.Contains("low") && !gname.Contains("fwd") )
		{
			g = NormalizeGraphBinWidth(g,h);
			g2= NormalizeGraphBinWidth(g2,h);
			g3= NormalizeGraphBinWidth(g3,h);
			h = NormalizeHistoBinWidth(h);
		}
		h->Write("",TObject::kOverwrite);
		g->Write("",TObject::kOverwrite);
		g2->Write("",TObject::kOverwrite);
		g3->Write("",TObject::kOverwrite);
	}
	return EL::StatusCode::SUCCESS;
}


