#include "HGamBackgroundStudies/HGam2x2DInputMaker.h"
#include "HGamAnalysisFramework/HGamVariables.h"
#include "HGamAnalysisFramework/VarHandler.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include <EventLoop/Worker.h>

using namespace std;


// this is needed to distribute the algorithm to the workers
ClassImp(HGam2x2DInputMaker)

	StrV Vectorize(Str str, Str sep = " ") {
		StrV result; TObjArray *strings = str.Tokenize(sep.Data());
		if(strings->GetEntries()==0) return result;
		TIter istr(strings);
		while (TObjString* os=(TObjString*)istr())
			if (os->GetString()[0]!='#') result.push_back(os->GetString());
			else break;
		delete strings;
		return result;
	}

VecF VectorizeF(Str str, Str sep = " ") {
	VecF result; StrV vecS = Vectorize(str,sep);
	for (uint i=0;i<vecS.size();++i)
		result.push_back(atof(vecS[i]));
	return result;
}



HGam2x2DInputMaker::HGam2x2DInputMaker(const char *name)
	:
		HgammaAnalysis(name){
			//_cutflowHistN=cfname;

		}


EL::StatusCode HGam2x2DInputMaker::createOutput()
{       


	m_fout = new TFile(TString(Form("%s",wk()->getOutputFile("MxAOD")->GetName())).ReplaceAll("sample.root","sideband2x2d_input.root"),"recreate");
	//cout<<"TESTING FILE NEAME == "<<_cutflowHistN<<endl;
	// TH1 *cutflow = dynamic_cast<TH1*>(wk()->inputFile()->Get(_cutflowHistN));
	// if ( cutflow!= NULL )cutflow->Write();

	// Save all noDalitz_weighted cutflows
	TIter nextiter(wk()->inputFile()->GetListOfKeys());
	TKey *key = NULL;
	while ((key = (TKey*)nextiter())) {
		TClass *cl = gROOT->GetClass(key->GetClassName());
		if (!cl->InheritsFrom("TH1")) continue;
		TH1 *h = (TH1*)key->ReadObj();
		if (h != NULL) {
			TString name(h->GetName());


			if (name.Contains("noDalitz_weighted")) {
				h->Write();
			}

			if (name.Contains("CutFlow_Run")) {
				h->Write();
			}
		}
	}


	InitialiseMiniTree();
	return EL::StatusCode::SUCCESS;
}


EL::StatusCode HGam2x2DInputMaker::InitialiseMiniTree() {

	m_mini_tree = new TTree("SidebandInputs","SidebandInputs");
	m_mini_tree->SetDirectory(m_fout);
	m_mini_tree->SetAutoFlush(100000);


	MakeOBranches({"isPassedMassCut", "isPassedRelPtCuts", "passCrackVetoCleaning"});
	//	"isPassedJetEventClean","isPassedTriggerMatch"});

	MakeIBranches({"yybb_btag77_85_BCal_cutFlow", "yybb_btag77_BCal_cutFlow"});
	MakeFBranches({"y1_topoetcone20","y2_topoetcone20","y1_ptcone20","y2_ptcone20"});
	MakeFBranches({"y1_pt", "y2_pt"});
	//MakeFBranches({"y1_eta","y2_eta", "y1_phi", "y2_phi", "y1_pt", "y2_pt"});
	MakeIBranches({"y1_isIso", "y2_isIso"});
	//MakeIBranches({"y1_convType","y2_convType", "y1_isIso", "y2_isIso"});
	//MakeIBranches({"y1_pdgId", "y2_pdgId"});
	MakeUIBranches({"y1_isEMTight","y2_isEMTight","y1_isEMTight_nofudge","y2_isEMTight_nofudge"});
	MakeIBranches({"y1_isTight","y2_isTight","y1_isTight_nofudge","y2_isTight_nofudge"});
	//MakeFBranches({"y1_truth_e", "y1_truth_px", "y1_truth_py", "y1_truth_pz", "y2_truth_e", "y2_truth_px", "y2_truth_py", "y2_truth_pz"});
	//MakeFBranches({"y1_dR", "y2_dR"});
	MakeFBranches({"weight", "yybb_weight", "crossSectionBRfilterEff", "Lumi", "weightFJvt"});

	MakeIBranches({"N_j_central", "N_lep", "N_j_b77", "N_j_b85"});
	MakeFBranches({"m_yy","BCal_m_yyjj_tilde", "BCal_m_jj", "BDT_BCal_yy_Score", "BDT_BCal_ttH_Score"});
	MakeIBranches({"runNumber"});
	MakeIBranches({"yybb_nonRes_XGBoost_btag77_85_BCal_Cat", "yybb_nonRes_XGBoost_btag77_BCal_Cat"});
	MakeIBranches({"yybb_btag77_BCal_vbf_selected", "yybb_btag77_85_BCal_vbf_selected"});
	return EL::StatusCode::SUCCESS;
	}

EL::StatusCode HGam2x2DInputMaker::execute()
{
	// Reset all branch variables
	resetBranches();

	HgammaAnalysis::execute();

	xAOD::PhotonContainer photons = photonHandler()->getCorrectedContainer();
	xAOD::PhotonContainer selphotons = photonHandler()->applyPreSelection(photons);

	if(photons[0]->pt()!=selphotons[0]->pt()) {std::cout<<"PROBLEM WITH PHOTONS"<<std::endl; abort();}
	//std::cout<<">>>>>>>>>>> HTXS_Higgs_pt :: "<<eventHandler()->getVar<float>("HTXS_Higgs_pt")<<std::endl;

	//_oMap["isPassedTriggerMatch"] = (int)eventHandler()->getVar< char >("isPassedTriggerMatch");
	//_oMap["isPassed"] = var::isPassed();
	//_oMap["isPassedPID"] = (bool)eventHandler()->getVar< char >("isPassedPID");
	//_oMap["isPassedJetEventClean"] = var::isPassedJetEventClean();
	//_oMap["isPassedIsolation"] = (bool)eventHandler()->getVar< char >("isPassedIsolation");
	_oMap["isPassedRelPtCuts"] = (bool)eventHandler()->getVar< char >("isPassedRelPtCuts");
	_oMap["isPassedMassCut"] = (bool)eventHandler()->getVar< char >("isPassedMassCut");
	_oMap["passCrackVetoCleaning"] = (bool)eventHandler()->getVar< char >("passCrackVetoCleaning");
	bool isPassedRelPtCuts = (bool)eventHandler()->getVar< char >("isPassedRelPtCuts");
	bool isPassedMassCut = (bool)eventHandler()->getVar< char >("isPassedMassCut");
	bool passCrackVetoCleaning = (bool)eventHandler()->getVar< char >("passCrackVetoCleaning");

	const xAOD::EventInfo *HgameventInfo = 0;
	if (!event()->retrieve(HgameventInfo, "HGamEventInfo").isSuccess())
	{
		cout<<"problem with HGamEventInfo"<<endl;
	}


	_iMap["yybb_btag77_85_BCal_cutFlow"] = HgameventInfo->auxdata<int>("yybb_btag77_85_BCal_cutFlow");
	_iMap["yybb_btag77_BCal_cutFlow"] = HgameventInfo->auxdata<int>("yybb_btag77_BCal_cutFlow");
	int yybb_btag77_BCal_cutFlow = HgameventInfo->auxdata<int>("yybb_btag77_BCal_cutFlow");


	// do higgs hf stuff

	//_fMap["weight_lumiXsec"] = HG::isMC()?lumiXsecWeight():1.0;

	int runNumber = (int)(eventInfo()->runNumber());
	_iMap["runNumber"] = runNumber;
	
	//cout<<"RUNNUMBER=  "<<runNumber<<" cutFLow= "<<yybb_btag77_BCal_cutFlow<<endl;
	//cout<<"ZIHANG passCrackVetoCleaning= "<<passCrackVetoCleaning<<endl;
	if (runNumber==276262 || runNumber==302737) {
		if (!(passCrackVetoCleaning)) {return EL::StatusCode::SUCCESS; }
	}
	if (runNumber==284500) {
		if (!(passCrackVetoCleaning)) {
			return EL::StatusCode::SUCCESS;
		}
	}

	if (!(isPassedRelPtCuts)) return EL::StatusCode::SUCCESS;
	if (!(isPassedMassCut)) return EL::StatusCode::SUCCESS;
	if (yybb_btag77_BCal_cutFlow!=6) return EL::StatusCode::SUCCESS;

	_fMap["weight"] = var::weight();
	_fMap["crossSectionBRfilterEff"]  = eventHandler()->getVar< float >("crossSectionBRfilterEff");
	_fMap["yybb_weight"] = eventHandler()->getVar< float >("yybb_weight");
	_fMap["weightFJvt"] = HgameventInfo->auxdata<float>("weightFJvt");

	_iMap["N_lep"] = (int)var::N_lep();
	_iMap["N_j_central"] = (int)var::N_j_central();

	//_iMap["y1_convType"] = photons[0]->auxdata< int >("conversionType"); 
	_fMap["y1_pt"] = photons[0]->pt();
	//_fMap["y1_eta"] = photons[0]->eta();   
	//_fMap["y1_phi"] = photons[0]->phi(); 
	_uiMap["y1_isEMTight"] = photons[0]->auxdata< unsigned int >("isEMTight");
	_uiMap["y1_isEMTight_nofudge"] = photons[0]->auxdata< unsigned int >("isEMTight_nofudge");
	_iMap["y1_isTight"] = photons[0]->auxdata< char >("isTight");
	_iMap["y1_isTight_nofudge"] = photons[0]->auxdata< char >("isTight_nofudge");
	_iMap["y1_isIso"] = photons[0]->auxdata< char >("isIsoFixedCutLoose");
	//_iMap["y2_convType"] = photons[1]->auxdata< int >("conversionType");
	_fMap["y2_pt"] = photons[1]->pt();
	//_fMap["y2_eta"] = photons[1]->eta();   
	//_fMap["y2_phi"] = photons[1]->phi();
	_uiMap["y2_isEMTight"] = photons[1]->auxdata< unsigned int >("isEMTight");
	_uiMap["y2_isEMTight_nofudge"] = photons[1]->auxdata< unsigned int >("isEMTight_nofudge");
	_iMap["y2_isTight"] = photons[1]->auxdata< char >("isTight");
	_iMap["y2_isTight_nofudge"] = photons[1]->auxdata< char >("isTight_nofudge");
	_iMap["y2_isIso"] = photons[1]->auxdata< char >("isIsoFixedCutLoose");
	_fMap["y1_topoetcone20"] = photons[0]->auxdata< float >("topoetcone20");
	_fMap["y1_ptcone20"] = photons[0]->auxdata< float >("ptcone20");
	_fMap["y2_topoetcone20"] = photons[1]->auxdata< float >("topoetcone20");
	_fMap["y2_ptcone20"] = photons[1]->auxdata< float >("ptcone20");
	_iMap["yybb_nonRes_XGBoost_btag77_85_BCal_Cat"] = HgameventInfo->auxdata<int>("yybb_nonRes_XGBoost_btag77_85_BCal_Cat");
	_iMap["yybb_nonRes_XGBoost_btag77_BCal_Cat"]    = HgameventInfo->auxdata<int>("yybb_nonRes_XGBoost_btag77_BCal_Cat");
	_fMap["m_yy"] = var::m_yy();  
	_fMap["BCal_m_jj"] =HgameventInfo->auxdata<float>("yybb_BCal_m_jj");  
	_fMap["BCal_m_yyjj_tilde"] = HgameventInfo->auxdata<float>("yybb_BCal_m_yyjj_tilde");  
	//_iMap["yybb_btag77_BCal_vbf_selected"] = HgameventInfo->auxdata<int>("yybb_btag77_BCal_vbf_selected");
	//_iMap["yybb_btag77_85_BCal_vbf_selected"] = HgameventInfo->auxdata<int>("yybb_btag77_85_BCal_vbf_selected");

	_fMap["BDT_BCal_ttH_Score"] = HgameventInfo->auxdata<float>("yybb_Res_BDT_BCal_ttH_Score");
	_fMap["BDT_BCal_yy_Score"] = HgameventInfo->auxdata<float>("yybb_Res_BDT_BCal_yy_Score");


	float lumi;
	if (HG::isMC() && runNumber == 284500) lumi=36.20766*1000;
	else if (HG::isMC() && runNumber == 300000) lumi=44.3074*1000;
	else if (HG::isMC() && runNumber == 310000) lumi=58.4501*1000;
	else lumi=1;

	_fMap["Lumi"] = lumi; 

	// ntag stuff
	xAOD::JetContainer allJets = jetHandler()->getCorrectedContainer();
	xAOD::JetContainer jets = jetHandler()->applySelection(allJets);

	int nBJets85=0, nBJets77=0;
	for (auto jet : jets) {
		if (jet->auxdata<char>("DL1r_FixedCutBEff_85")) { nBJets85++; }
		if (jet->auxdata<char>("DL1r_FixedCutBEff_77")) { nBJets77++; }
	}
	_iMap["N_j_b77"] = nBJets77;
	_iMap["N_j_b85"] = nBJets85;

	if (nBJets77!=2) return EL::StatusCode::SUCCESS;

	//if (HG::isMC() ) {
	//
	//	xAOD::TruthParticleContainer truthPh = truthHandler()->getPhotons();
	//	_fMap["y1_truth_e"] = truthPh[0]->e();	
	//	_fMap["y1_truth_px"] = truthPh[0]->px();	
	//	_fMap["y1_truth_py"] = truthPh[0]->py();	
	//	_fMap["y1_truth_pz"] = truthPh[0]->pz();	
	//	_fMap["y2_truth_e"] = truthPh[1]->e();	
	//	_fMap["y2_truth_px"] = truthPh[1]->px();	
	//	_fMap["y2_truth_py"] = truthPh[1]->py();	
	//	_fMap["y2_truth_pz"] = truthPh[1]->pz();	
	//
	//
	//	TLorentzVector y1_reco, y2_reco, y1_truth, y2_truth;
	//	y1_reco.SetPtEtaPhiM(photons[0]->pt(), photons[0]->eta(), photons[0]->phi(), photons[0]->m()); 
	//	y2_reco.SetPtEtaPhiM(photons[1]->pt(), photons[1]->eta(), photons[1]->phi(), photons[1]->m());
	//	y1_truth.SetPxPyPzE(truthPh[0]->px(), truthPh[0]->py(), truthPh[0]->pz(), truthPh[0]->e()); 
	//	y2_truth.SetPxPyPzE(truthPh[1]->px(), truthPh[1]->py(), truthPh[1]->pz(), truthPh[1]->e());
	//	_fMap["y1_dR"] = y1_reco.DeltaR(y1_truth); 
	//	_fMap["y2_dR"] = y2_reco.DeltaR(y2_truth); 
	//
	//}
	//else {
	//
	//	_fMap["y1_truth_e"] = -999.;
	//	_fMap["y1_truth_px"] = -999.;
	//	_fMap["y1_truth_py"] = -999.;
	//	_fMap["y1_truth_pz"] = -999.;
	//	_fMap["y2_truth_e"]  = -999.;
	//	_fMap["y2_truth_px"] = -999.;
	//	_fMap["y2_truth_py"] = -999.;
	//	_fMap["y2_truth_pz"] = -999.;
	//	_fMap["y1_dR"] = -999.; 
	//	_fMap["y2_dR"] = -999.;
	//
	//}

	m_mini_tree->Fill();

	return EL::StatusCode::SUCCESS;
}

void HGam2x2DInputMaker::WriteOutput() {


	m_fout->cd();
	WriteMiniTree();
	m_fout->Close();
}

void HGam2x2DInputMaker::WriteMiniTree() {

	m_fout->cd();
	m_mini_tree->Write();

}


EL::StatusCode HGam2x2DInputMaker::finalize()
{
	HgammaAnalysis::finalize();
	WriteOutput();
	return EL::StatusCode::SUCCESS;
}
