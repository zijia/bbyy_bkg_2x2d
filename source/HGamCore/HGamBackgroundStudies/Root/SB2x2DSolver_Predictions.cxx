#include "HGamBackgroundStudies/SB2x2DSolver.h"

#define SQUARE(x) ((x)*(x))

namespace SB {

  
double SB2x2DSolver::GetNTotalPrediction()
{
  double xgg = 1/(ep1I*ep1T*ep2I*ep2T*x1g*x2g*xIgg*xTgg);
  double xgj = 1/(e1I*e1T*f2I*f2T*x1g*x2j*xIgj*xTgj);
  double xjg = 1/(e2I*e2T*f1I*f1T*x1j*x2g*xIjg*xTjg);
  double xjj = 1/(fp1I*fp1T*fp2I*fp2T*x1j*x2j*xIjj*xTjj);
  
  //std::cout << WggTITI << "*" << xgg << " + " << WgjTITI << "*" << xgj << " + " << WjgTITI << "*" << xjg << " + " << WjjTITI << "*" << xjj << std::endl;
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNAAPrediction()
{
  return WggTITI + WgjTITI + WjgTITI + WjjTITI;
}

double SB2x2DSolver::GetNABPrediction()
{
  double xgg = -1 + 1/(ep2I*x2g*xIgg);
  double xgj = -1 + 1/(f2I*x2j*xIgj);
  double xjg = -1 + 1/(e2I*x2g*xIjg);
  double xjj = -1 + 1/(fp2I*x2j*xIjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNACPrediction()
{
  double xgg = -1 + 1/(ep2T*x2g*xTgg);
  double xgj = -1 + 1/(f2T*x2j*xTgj);
  double xjg = -1 + 1/(e2T*x2g*xTjg);
  double xjj = -1 + 1/(fp2T*x2j*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNADPrediction()
{
  double xgg = (1 - ep2T*xTgg + ep2I*xIgg*(-1 + ep2T*x2g*xTgg))/(ep2I*ep2T*x2g*xIgg*xTgg);
  double xgj = (1 - f2T*xTgj + f2I*xIgj*(-1 + f2T*x2j*xTgj))/(f2I*f2T*x2j*xIgj*xTgj);
  double xjg = (1 - e2T*xTjg + e2I*xIjg*(-1 + e2T*x2g*xTjg))/(e2I*e2T*x2g*xIjg*xTjg);
  double xjj = (1 - fp2T*xTjj + fp2I*xIjj*(-1 + fp2T*x2j*xTjj))/(fp2I*fp2T*x2j*xIjj*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNBAPrediction()
{
  double xgg = -1 + 1/(ep1I*x1g*xIgg);
  double xgj = -1 + 1/(e1I*x1g*xIgj);
  double xjg = -1 + 1/(f1I*x1j*xIjg);
  double xjj = -1 + 1/(fp1I*x1j*xIjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNBBPrediction()
{
  double xgg = (1 - ep2I*x2g + ep1I*x1g*(-1 + ep2I*x2g*xIgg))/(ep1I*ep2I*x1g*x2g*xIgg);
  double xgj = (1 - f2I*x2j + e1I*x1g*(-1 + f2I*x2j*xIgj))/(e1I*f2I*x1g*x2j*xIgj);
  double xjg = (1 - e2I*x2g + f1I*x1j*(-1 + e2I*x2g*xIjg))/(e2I*f1I*x1j*x2g*xIjg);
  double xjj = (1 - fp2I*x2j + fp1I*x1j*(-1 + fp2I*x2j*xIjj))/(fp1I*fp2I*x1j*x2j*xIjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNBCPrediction()
{
  double xgg = ((-1 + ep1I*x1g*xIgg)*(-1 + ep2T*x2g*xTgg))/(ep1I*ep2T*x1g*x2g*xIgg*xTgg);
  double xgj = ((-1 + e1I*x1g*xIgj)*(-1 + f2T*x2j*xTgj))/(e1I*f2T*x1g*x2j*xIgj*xTgj);
  double xjg = ((-1 + f1I*x1j*xIjg)*(-1 + e2T*x2g*xTjg))/(e2T*f1I*x1j*x2g*xIjg*xTjg);
  double xjj = ((-1 + fp1I*x1j*xIjj)*(-1 + fp2T*x2j*xTjj))/(fp1I*fp2T*x1j*x2j*xIjj*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNBDPrediction()
{
  double xgg = ((-1 + ep1I*x1g)*(-1 + ep2T*xTgg) - ep2I*(-1 + ep1I*x1g*xIgg)*(-1 + ep2T*x2g*xTgg))/(ep1I*ep2I*ep2T*x1g*x2g*xIgg*xTgg);
  double xgj = ((-1 + e1I*x1g)*(-1 + f2T*xTgj) - f2I*(-1 + e1I*x1g*xIgj)*(-1 + f2T*x2j*xTgj))/(e1I*f2I*f2T*x1g*x2j*xIgj*xTgj);
  double xjg = ((-1 + f1I*x1j)*(-1 + e2T*xTjg) - e2I*(-1 + f1I*x1j*xIjg)*(-1 + e2T*x2g*xTjg))/(e2I*e2T*f1I*x1j*x2g*xIjg*xTjg);
  double xjj = ((-1 + fp1I*x1j)*(-1 + fp2T*xTjj) - fp2I*(-1 + fp1I*x1j*xIjj)*(-1 + fp2T*x2j*xTjj))/(fp1I*fp2I*fp2T*x1j*x2j*xIjj*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNCAPrediction()
{
  double xgg = -1 + 1/(ep1T*x1g*xTgg);
  double xgj = -1 + 1/(e1T*x1g*xTgj);
  double xjg = -1 + 1/(f1T*x1j*xTjg);
  double xjj = -1 + 1/(fp1T*x1j*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNCBPrediction()
{
  double xgg = ((-1 + ep2I*x2g*xIgg)*(-1 + ep1T*x1g*xTgg))/(ep1T*ep2I*x1g*x2g*xIgg*xTgg);
  double xgj = ((-1 + f2I*x2j*xIgj)*(-1 + e1T*x1g*xTgj))/(e1T*f2I*x1g*x2j*xIgj*xTgj);
  double xjg = ((-1 + e2I*x2g*xIjg)*(-1 + f1T*x1j*xTjg))/(e2I*f1T*x1j*x2g*xIjg*xTjg);
  double xjj = ((-1 + fp2I*x2j*xIjj)*(-1 + fp1T*x1j*xTjj))/(fp1T*fp2I*x1j*x2j*xIjj*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNCCPrediction()
{
  double xgg = (1 - ep2T*x2g + ep1T*x1g*(-1 + ep2T*x2g*xTgg))/(ep1T*ep2T*x1g*x2g*xTgg);
  double xgj = (1 - f2T*x2j + e1T*x1g*(-1 + f2T*x2j*xTgj))/(e1T*f2T*x1g*x2j*xTgj);
  double xjg = (1 - e2T*x2g + f1T*x1j*(-1 + e2T*x2g*xTjg))/(e2T*f1T*x1j*x2g*xTjg);
  double xjj = (1 - fp2T*x2j + fp1T*x1j*(-1 + fp2T*x2j*xTjj))/(fp1T*fp2T*x1j*x2j*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNCDPrediction()
{
  double xgg = ((-1 + ep1T*x1g)*(-1 + ep2I*xIgg) - ep2T*(-1 + ep2I*x2g*xIgg)*(-1 + ep1T*x1g*xTgg))/(ep1T*ep2I*ep2T*x1g*x2g*xIgg*xTgg);
  double xgj = ((-1 + e1T*x1g)*(-1 + f2I*xIgj) - f2T*(-1 + f2I*x2j*xIgj)*(-1 + e1T*x1g*xTgj))/(e1T*f2I*f2T*x1g*x2j*xIgj*xTgj);
  double xjg = ((-1 + f1T*x1j)*(-1 + e2I*xIjg) - e2T*(-1 + e2I*x2g*xIjg)*(-1 + f1T*x1j*xTjg))/(e2I*e2T*f1T*x1j*x2g*xIjg*xTjg);
  double xjj = ((-1 + fp1T*x1j)*(-1 + fp2I*xIjj) - fp2T*(-1 + fp2I*x2j*xIjj)*(-1 + fp1T*x1j*xTjj))/(fp1T*fp2I*fp2T*x1j*x2j*xIjj*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNDAPrediction()
{
  double xgg = (1 - ep1T*xTgg + ep1I*xIgg*(-1 + ep1T*x1g*xTgg))/(ep1I*ep1T*x1g*xIgg*xTgg);
  double xgj = (1 - e1T*xTgj + e1I*xIgj*(-1 + e1T*x1g*xTgj))/(e1I*e1T*x1g*xIgj*xTgj);
  double xjg = (1 - f1T*xTjg + f1I*xIjg*(-1 + f1T*x1j*xTjg))/(f1I*f1T*x1j*xIjg*xTjg);
  double xjj = (1 - fp1T*xTjj + fp1I*xIjj*(-1 + fp1T*x1j*xTjj))/(fp1I*fp1T*x1j*xIjj*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNDBPrediction()
{
  double xgg = ((-1 + ep2I*x2g)*(-1 + ep1T*xTgg) - ep1I*(-1 + ep2I*x2g*xIgg)*(-1 + ep1T*x1g*xTgg))/(ep1I*ep1T*ep2I*x1g*x2g*xIgg*xTgg);
  double xgj = ((-1 + f2I*x2j)*(-1 + e1T*xTgj) - e1I*(-1 + f2I*x2j*xIgj)*(-1 + e1T*x1g*xTgj))/(e1I*e1T*f2I*x1g*x2j*xIgj*xTgj);
  double xjg = ((-1 + e2I*x2g)*(-1 + f1T*xTjg) - f1I*(-1 + e2I*x2g*xIjg)*(-1 + f1T*x1j*xTjg))/(e2I*f1I*f1T*x1j*x2g*xIjg*xTjg);
  double xjj = ((-1 + fp2I*x2j)*(-1 + fp1T*xTjj) - fp1I*(-1 + fp2I*x2j*xIjj)*(-1 + fp1T*x1j*xTjj))/(fp1I*fp1T*fp2I*x1j*x2j*xIjj*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNDCPrediction()
{
  double xgg = ((-1 + ep2T*x2g)*(-1 + ep1I*xIgg) - ep1T*(-1 + ep1I*x1g*xIgg)*(-1 + ep2T*x2g*xTgg))/(ep1I*ep1T*ep2T*x1g*x2g*xIgg*xTgg);
  double xgj = ((-1 + f2T*x2j)*(-1 + e1I*xIgj) - e1T*(-1 + e1I*x1g*xIgj)*(-1 + f2T*x2j*xTgj))/(e1I*e1T*f2T*x1g*x2j*xIgj*xTgj);
  double xjg = ((-1 + e2T*x2g)*(-1 + f1I*xIjg) - f1T*(-1 + f1I*x1j*xIjg)*(-1 + e2T*x2g*xTjg))/(e2T*f1I*f1T*x1j*x2g*xIjg*xTjg);
  double xjj = ((-1 + fp2T*x2j)*(-1 + fp1I*xIjj) - fp1T*(-1 + fp1I*x1j*xIjj)*(-1 + fp2T*x2j*xTjj))/(fp1I*fp1T*fp2T*x1j*x2j*xIjj*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}

double SB2x2DSolver::GetNDDPrediction()
{
  double xgg = (1 - ep1T - ep2T + ep1T*ep2T*xTgg + ep2I*(-1 + ep1T + ep2T*x2g - ep1T*ep2T*x2g*xTgg) + ep1I*((-1 + ep1T*x1g)*(1 - ep2I*xIgg) + ep2T*(-1 + ep2I*x2g*xIgg)*(-1 + ep1T*x1g*xTgg)))/(ep1I*ep1T*ep2I*ep2T*x1g*x2g*xIgg*xTgg);
  double xgj = (1 - e1T - f2T + e1T*f2T*xTgj + f2I*(-1 + e1T + f2T*x2j - e1T*f2T*x2j*xTgj) + e1I*((-1 + e1T*x1g)*(1 - f2I*xIgj) + f2T*(-1 + f2I*x2j*xIgj)*(-1 + e1T*x1g*xTgj)))/(e1I*e1T*f2I*f2T*x1g*x2j*xIgj*xTgj);
  double xjg = (1 - e2T - f1T + e2T*f1T*xTjg + e2I*(-1 + f1T + e2T*x2g - e2T*f1T*x2g*xTjg) + f1I*((-1 + f1T*x1j)*(1 - e2I*xIjg) + e2T*(-1 + e2I*x2g*xIjg)*(-1 + f1T*x1j*xTjg)))/(e2I*e2T*f1I*f1T*x1j*x2g*xIjg*xTjg);
  double xjj = (1 - fp1T - fp2T + fp1T*fp2T*xTjj + fp2I*(-1 + fp1T + fp2T*x2j - fp1T*fp2T*x2j*xTjj) + fp1I*((-1 + fp1T*x1j)*(1 - fp2I*xIjj) + fp2T*(-1 + fp2I*x2j*xIjj)*(-1 + fp1T*x1j*xTjj)))/(fp1I*fp1T*fp2I*fp2T*x1j*x2j*xIjj*xTjj);
  return WggTITI*xgg + WgjTITI*xgj + WjgTITI*xjg + WjjTITI*xjj;
}


}
