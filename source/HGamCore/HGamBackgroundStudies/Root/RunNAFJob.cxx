#include "HGamBackgroundStudies/RunNAFJob.h"   
#include "HGamAnalysisFramework/Config.h"
#include <SampleHandler/SampleLocal.h>
#include <SampleHandler/ToolsSplit.h>
#include <EventLoop/GEDriver.h> 


namespace HG { 

  StrV parseArgumentsPrime(HG::Config *conf, int argc, char **argv)
  { // code taken from HGamAnalysisFramework
    StrV files;
    for (int argi=1; argi<argc; ++argi) {
      Str arg=argv[argi];   
      // 1. Check if argument is a configuration file. If so add it!
      if ( arg.EndsWith(".cfg") || arg.EndsWith(".config" )) {
	conf->addFile(arg); continue;
      }
      // 2. If the argument contains ".root", i.e. *.root*, add it to files to run over
      if (arg.Contains(".root")) {
	files.push_back(arg); continue;
      }      
      // 3. If the arguemnt ends with colon, add a new argument
      if (arg.EndsWith(":")) {
	Str key(arg); key.ReplaceAll(":","");
	conf->setValue(key,argv[++argi]); continue;
      }
      if ( arg.Contains("CutFlow") ) continue;
       
      // if we get here, the arguemnt cannot be understood
      HG::fatal("Cannot interpret argument: "+arg);
    }
    return files;
  }
  
void subimtJobsNAF(HgammaAnalysis *alg, int argc, char** argv){
    
    HG::Config conf;
    StrV files = parseArgumentsPrime(&conf, argc, argv);
    // TEnv uses value from first file it's specified in.                                           
    // If specified, read in additional configuration                                               
    if (conf.isDefined("Include"))                                                                  
      for (Str cfg : conf.getStrV("Include"))                                                   
	conf.addFile(cfg);                                                                          
    
    // Fill unspecified values from default config, specified here.                                 
    if (!conf.isDefined("BaseConfig")) {                                                            
      HG::fatal("You must specify a base configuration file, option: BaseConfig. Exiting.");       
    } else {                                                                                        
      conf.addFile(PathResolverFindCalibFile(conf.getStr("BaseConfig").Data()));                   
    } 
    
    Str progName = argv[0];                                                                     
    Str sampleName = conf.getStr("SampleName","sample");                                        
    Str submitDir  = conf.getStr("OutputDir",progName+"_DATE");  
    if (submitDir.Contains("DATE")) {                                                               
      TDatime now = TDatime();                                                                      
      submitDir.ReplaceAll("DATE",Form("%d.%.2d.%.2d_%.2d.%.2d.%.2d",                               
				       now.GetYear(),now.GetMonth(),now.GetDay(),                   
				       now.GetHour(),now.GetMinute(),now.GetSecond()));             
      conf.setValue("OutputDir", submitDir.Data());                                                 
    } 
    
    alg->setConfig(conf);
    
    SH::SampleHandler sh;
    EL::Job job;
    job.algsAdd(alg);
    
    // add to the input file list if specified
    if (conf.isDefined("InputFile"))
      for (auto file : conf.getStrV("InputFile")) files.push_back(file);
    
    if (files.size()) {
      // If one or several root files are specified as arugment, run over these
      std::auto_ptr<SH::SampleLocal> sample (new SH::SampleLocal (sampleName.Data()));
      printf("\n  %s\n","InputFiles:");
      for ( Str file : files ) {
	printf("    %s\n",file.Data());
	sample->add (file.Data());
      }
      
      // add the files to the sample handler
      sh.add (sample.release());
      
    }
    
    
    sh.setMetaString ("nc_tree", "CollectionTree");
    job.sampleHandler(sh);
    
    // Set number of events to be processed
    if (conf.isDefined("NumEvents"))
      job.options()->setDouble(EL::Job::optMaxEvents, conf.getInt("NumEvents"));
    
    // Set number of events to be skipped
    if (conf.isDefined("SkipEvents"))
      job.options()->setDouble(EL::Job::optSkipEvents, conf.getInt("SkipEvents"));
    
    // Set number of events per worker
    if (conf.isDefined("EventsPerWorker"))
      job.options()->setDouble(EL::Job::optEventsPerWorker, conf.getInt("EventsPerWorker"));
    
    
    job.options()->setString(EL::Job::optSubmitFlags,
			     "-q long.q -l os=sld6 -l site=hh -l h_vmem=4G");
    
    EL::GEDriver driver;
    driver.submit(job, submitDir.Data());
    
  }
  

}
