//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Feb 15 12:32:53 2021 by ROOT version 6.18/04
// from TTree CollectionTree/xAOD event tree
// found on file: /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/data15/data15_13TeV.DS1_3220ipb.physics_Main.MxAOD.p4205.h026.root
//////////////////////////////////////////////////////////

#ifndef data_h
#define data_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "AthContainers/DataVector.h"
#include "AuxContainerBase.h"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "AthContainers/DataVector.h"
#include "AthContainers/DataVector.h"
#include "AthContainers/DataVector.h"
#include "AthContainers/DataVector.h"
#include "AthContainers/DataVector.h"
#include "versions/MissingETContainer_v1.h"
#include "vector"
#include "vector"
#include "vector"
#include "versions/EventInfo_v1.h"
#include "AuxInfoBase.h"
#include "vector"
#include "versions/EventAuxInfo_v1.h"
#include "vector"
#include "ElementLinkBase.h"

class data {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxHGamPhotonsAux = 1;
   static constexpr Int_t kMaxHGamElectronsAux = 1;
   static constexpr Int_t kMaxBTagging_HGamAntiKt4PFlowCustomVtxHggAux = 1;
   static constexpr Int_t kMaxHGamAntiKt4PFlowCustomVtxHggJetsAux = 1;
   static constexpr Int_t kMaxBTagging_HGamAntiKt4EMPFlow_BTagging201903Aux = 1;
   static constexpr Int_t kMaxHGamAntiKt4EMPFlowJets_BTagging201903Aux = 1;
   static constexpr Int_t kMaxHGamMuonsAux = 1;
   static constexpr Int_t kMaxHGamTauJetsAux = 1;
   static constexpr Int_t kMaxHGamMET_Reference_AntiKt4EMPFlowAux = 1;
   static constexpr Int_t kMaxHGamEventInfoAux = 1;
   static constexpr Int_t kMaxEventInfoAux = 1;
   static constexpr Int_t kMaxHGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink = 15;
   static constexpr Int_t kMaxHGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink = 15;

   // Declaration of leaf types
   DataVector<xAOD::Photon_v1> *HGamPhotons;
   xAOD::AuxContainerBase *HGamPhotonsAux_;
   vector<float>   *HGamPhotonsAuxDyn_DeltaE;
   vector<float>   *HGamPhotonsAuxDyn_Eratio;
   vector<float>   *HGamPhotonsAuxDyn_Rconv;
   vector<float>   *HGamPhotonsAuxDyn_Reta;
   vector<float>   *HGamPhotonsAuxDyn_Rhad;
   vector<float>   *HGamPhotonsAuxDyn_Rhad1;
   vector<float>   *HGamPhotonsAuxDyn_Rphi;
   vector<unsigned short> *HGamPhotonsAuxDyn_author;
   vector<float>   *HGamPhotonsAuxDyn_cl_E;
   vector<float>   *HGamPhotonsAuxDyn_cl_E_TileGap3;
   vector<float>   *HGamPhotonsAuxDyn_cl_Es0;
   vector<float>   *HGamPhotonsAuxDyn_cl_Es1;
   vector<float>   *HGamPhotonsAuxDyn_cl_Es2;
   vector<float>   *HGamPhotonsAuxDyn_cl_Es3;
   vector<float>   *HGamPhotonsAuxDyn_cl_eta;
   vector<float>   *HGamPhotonsAuxDyn_cl_etaCalo;
   vector<float>   *HGamPhotonsAuxDyn_cl_phiCalo;
   vector<float>   *HGamPhotonsAuxDyn_cl_ratioEs1Es2;
   vector<int>     *HGamPhotonsAuxDyn_conversionType;
   vector<float>   *HGamPhotonsAuxDyn_convtrk1nPixHits;
   vector<float>   *HGamPhotonsAuxDyn_convtrk1nSCTHits;
   vector<float>   *HGamPhotonsAuxDyn_convtrk2nPixHits;
   vector<float>   *HGamPhotonsAuxDyn_convtrk2nSCTHits;
   vector<float>   *HGamPhotonsAuxDyn_e277;
   vector<float>   *HGamPhotonsAuxDyn_eta;
   vector<float>   *HGamPhotonsAuxDyn_eta_s1;
   vector<float>   *HGamPhotonsAuxDyn_eta_s2;
   vector<float>   *HGamPhotonsAuxDyn_f1;
   vector<float>   *HGamPhotonsAuxDyn_fracs1;
   vector<unsigned int> *HGamPhotonsAuxDyn_isEMTight;
   vector<char>    *HGamPhotonsAuxDyn_isIsoFixedCutLoose;
   vector<char>    *HGamPhotonsAuxDyn_isIsoFixedCutLooseCaloOnly;
   vector<char>    *HGamPhotonsAuxDyn_isIsoFixedCutTight;
   vector<char>    *HGamPhotonsAuxDyn_isIsoFixedCutTightCaloOnly;
   vector<char>    *HGamPhotonsAuxDyn_isTight;
   vector<float>   *HGamPhotonsAuxDyn_m;
   vector<float>   *HGamPhotonsAuxDyn_maxEcell_energy;
   vector<float>   *HGamPhotonsAuxDyn_maxEcell_eta;
   vector<int>     *HGamPhotonsAuxDyn_maxEcell_gain;
   vector<unsigned long> *HGamPhotonsAuxDyn_maxEcell_onlId;
   vector<float>   *HGamPhotonsAuxDyn_maxEcell_phi;
   vector<float>   *HGamPhotonsAuxDyn_maxEcell_time;
   vector<float>   *HGamPhotonsAuxDyn_maxEcell_x;
   vector<float>   *HGamPhotonsAuxDyn_maxEcell_y;
   vector<float>   *HGamPhotonsAuxDyn_maxEcell_z;
   vector<float>   *HGamPhotonsAuxDyn_phi;
   vector<float>   *HGamPhotonsAuxDyn_pt;
   vector<float>   *HGamPhotonsAuxDyn_pt1conv;
   vector<float>   *HGamPhotonsAuxDyn_pt2conv;
   vector<float>   *HGamPhotonsAuxDyn_pt_s2;
   vector<float>   *HGamPhotonsAuxDyn_ptcone20;
   vector<float>   *HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000;
   vector<float>   *HGamPhotonsAuxDyn_ptcone40;
   vector<float>   *HGamPhotonsAuxDyn_ptconv;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_Es0;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_Es1;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_Es2;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_Es3;
   vector<float>   *HGamPhotonsAuxDyn_rawcl_ratioEs1Es2;
   vector<float>   *HGamPhotonsAuxDyn_relEreso;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone20;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone20_DDcorr;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone20_SC;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone40;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone40_DDcorr;
   vector<float>   *HGamPhotonsAuxDyn_topoetcone40_SC;
   vector<float>   *HGamPhotonsAuxDyn_truthRconv;
   vector<float>   *HGamPhotonsAuxDyn_weta1;
   vector<float>   *HGamPhotonsAuxDyn_weta2;
   vector<float>   *HGamPhotonsAuxDyn_wtots1;
   vector<float>   *HGamPhotonsAuxDyn_zconv;
   vector<float>   *HGamPhotonsAuxDyn_zvertex;
   DataVector<xAOD::Electron_v1> *HGamElectrons;
   xAOD::AuxContainerBase *HGamElectronsAux_;
   DataVector<xAOD::BTagging_v1> *BTagging_HGamAntiKt4PFlowCustomVtxHgg;
   xAOD::AuxContainerBase *BTagging_HGamAntiKt4PFlowCustomVtxHggAux_;
   DataVector<xAOD::Jet_v1> *HGamAntiKt4PFlowCustomVtxHggJets;
   xAOD::AuxContainerBase *HGamAntiKt4PFlowCustomVtxHggJetsAux_;
   DataVector<xAOD::BTagging_v1> *BTagging_HGamAntiKt4EMPFlow_BTagging201903;
   xAOD::AuxContainerBase *BTagging_HGamAntiKt4EMPFlow_BTagging201903Aux_;
   DataVector<xAOD::Jet_v1> *HGamAntiKt4EMPFlowJets_BTagging201903;
   xAOD::AuxContainerBase *HGamAntiKt4EMPFlowJets_BTagging201903Aux_;
   DataVector<xAOD::Muon_v1> *HGamMuons;
   xAOD::AuxContainerBase *HGamMuonsAux_;
   DataVector<xAOD::TauJet_v3> *HGamTauJets;
   xAOD::AuxContainerBase *HGamTauJetsAux_;
   xAOD::MissingETContainer_v1 *HGamMET_Reference_AntiKt4EMPFlow;
   xAOD::AuxContainerBase *HGamMET_Reference_AntiKt4EMPFlowAux_;
   vector<double>  *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx;
   vector<double>  *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy;
   vector<string>  *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name;
   vector<ULong64_t> *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source;
   vector<double>  *HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet;
   xAOD::EventInfo_v1 *HGamEventInfo;
   xAOD::AuxInfoBase *HGamEventInfoAux_;
   Float_t         HGamEventInfoAuxDyn_DR_y_y;
   Float_t         HGamEventInfoAuxDyn_DRmin_y_j;
   Float_t         HGamEventInfoAuxDyn_DRmin_y_j_2;
   Float_t         HGamEventInfoAuxDyn_Deta_j_j;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j_30;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j_30_signed;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j_50;
   Float_t         HGamEventInfoAuxDyn_Dphi_j_j_50_signed;
   Float_t         HGamEventInfoAuxDyn_Dphi_y_y;
   Float_t         HGamEventInfoAuxDyn_Dphi_yy_jj;
   Float_t         HGamEventInfoAuxDyn_Dphi_yy_jj_30;
   Float_t         HGamEventInfoAuxDyn_Dphi_yy_jj_50;
   Float_t         HGamEventInfoAuxDyn_Dy_j_j;
   Float_t         HGamEventInfoAuxDyn_Dy_j_j_30;
   Float_t         HGamEventInfoAuxDyn_Dy_y_y;
   Float_t         HGamEventInfoAuxDyn_Dy_yy_jj;
   Float_t         HGamEventInfoAuxDyn_Dy_yy_jj_30;
   Float_t         HGamEventInfoAuxDyn_E_y1;
   Float_t         HGamEventInfoAuxDyn_E_y2;
   Float_t         HGamEventInfoAuxDyn_HT_30;
   Float_t         HGamEventInfoAuxDyn_HTall_30;
   Int_t           HGamEventInfoAuxDyn_Hc_cutFlowAllJet;
   Int_t           HGamEventInfoAuxDyn_Hc_cutFlowLeadJet;
   vector<int>     *HGamEventInfoAuxDyn_Hc_jet_truthlabel;
   Int_t           HGamEventInfoAuxDyn_Hc_leadJet_truthLabel;
   Int_t           HGamEventInfoAuxDyn_Hc_m_yy_cat;
   vector<float>   *HGamEventInfoAuxDyn_Hc_y1_j_dr;
   vector<float>   *HGamEventInfoAuxDyn_Hc_y2_j_dr;
   vector<float>   *HGamEventInfoAuxDyn_HggPrimaryVerticesScore;
   vector<float>   *HGamEventInfoAuxDyn_HggPrimaryVerticesZ;
   Int_t           HGamEventInfoAuxDyn_HiggsHF_cutFlow;
   Int_t           HGamEventInfoAuxDyn_NLoosePhotons;
   Int_t           HGamEventInfoAuxDyn_N_e;
   Int_t           HGamEventInfoAuxDyn_N_j;
   Int_t           HGamEventInfoAuxDyn_N_j_30;
   Int_t           HGamEventInfoAuxDyn_N_j_50;
   Int_t           HGamEventInfoAuxDyn_N_j_btag;
   Int_t           HGamEventInfoAuxDyn_N_j_btag30;
   Int_t           HGamEventInfoAuxDyn_N_j_central;
   Int_t           HGamEventInfoAuxDyn_N_j_central30;
   Int_t           HGamEventInfoAuxDyn_N_lep;
   Int_t           HGamEventInfoAuxDyn_N_lep_15;
   Int_t           HGamEventInfoAuxDyn_N_mu;
   Float_t         HGamEventInfoAuxDyn_Zepp;
   Int_t           HGamEventInfoAuxDyn_catCoup_GlobalICHEP;
   Int_t           HGamEventInfoAuxDyn_catCoup_HybridICHEP;
   Int_t           HGamEventInfoAuxDyn_catCoup_MonoH_2var;
   Int_t           HGamEventInfoAuxDyn_catCoup_Moriond2017BDT;
   Int_t           HGamEventInfoAuxDyn_catCoup_XGBoost_ttH;
   Int_t           HGamEventInfoAuxDyn_catCoup_XGBoost_ttHCP;
   Int_t           HGamEventInfoAuxDyn_catLowHighMyy_conv;
   Int_t           HGamEventInfoAuxDyn_catMass_Run1;
   Int_t           HGamEventInfoAuxDyn_catMass_conv;
   Int_t           HGamEventInfoAuxDyn_catMass_eta;
   Int_t           HGamEventInfoAuxDyn_catMass_mu;
   Int_t           HGamEventInfoAuxDyn_catMass_pT;
   Char_t          HGamEventInfoAuxDyn_catXS_HiggsHF;
   Char_t          HGamEventInfoAuxDyn_catXS_VBF;
   Char_t          HGamEventInfoAuxDyn_catXS_nbjet;
   Char_t          HGamEventInfoAuxDyn_catXS_ttH;
   Float_t         HGamEventInfoAuxDyn_cosTS_yy;
   Float_t         HGamEventInfoAuxDyn_cosTS_yyjj;
   Int_t           HGamEventInfoAuxDyn_cutFlow;
   Float_t         HGamEventInfoAuxDyn_eta_hybridtop2;
   Float_t         HGamEventInfoAuxDyn_eta_recotop1;
   vector<float>   *HGamEventInfoAuxDyn_fcnc_Score;
   Int_t           HGamEventInfoAuxDyn_fcnc_cat;
   Int_t           HGamEventInfoAuxDyn_fcnc_cutFlow;
   Float_t         HGamEventInfoAuxDyn_fcnc_etm;
   vector<int>     *HGamEventInfoAuxDyn_fcnc_icat;
   Int_t           HGamEventInfoAuxDyn_fcnc_idLep;
   vector<int>     *HGamEventInfoAuxDyn_fcnc_jetInd;
   Float_t         HGamEventInfoAuxDyn_fcnc_mT;
   vector<float>   *HGamEventInfoAuxDyn_fcnc_mTop1;
   vector<float>   *HGamEventInfoAuxDyn_fcnc_mTop2;
   Int_t           HGamEventInfoAuxDyn_fcnc_nbjet;
   vector<short>   *HGamEventInfoAuxDyn_fcnc_ncbTop12;
   Int_t           HGamEventInfoAuxDyn_fcnc_njet;
   Int_t           HGamEventInfoAuxDyn_fcnc_njet30;
   Int_t           HGamEventInfoAuxDyn_fcnc_ntopc;
   Float_t         HGamEventInfoAuxDyn_fcnc_phim;
   Float_t         HGamEventInfoAuxDyn_hardestVertexPhi;
   Float_t         HGamEventInfoAuxDyn_hardestVertexSumPt2;
   Float_t         HGamEventInfoAuxDyn_hardestVertexZ;
   vector<int>     *HGamEventInfoAuxDyn_idx_jets_recotop1;
   vector<int>     *HGamEventInfoAuxDyn_idx_jets_recotop2;
   Char_t          HGamEventInfoAuxDyn_isPassed;
   Char_t          HGamEventInfoAuxDyn_isPassedBasic;
   Char_t          HGamEventInfoAuxDyn_isPassedHighMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedIsolation;
   Char_t          HGamEventInfoAuxDyn_isPassedIsolationHighMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedIsolationLowMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedJetEventClean;
   Char_t          HGamEventInfoAuxDyn_isPassedLowMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedMassCut;
   Char_t          HGamEventInfoAuxDyn_isPassedPID;
   Char_t          HGamEventInfoAuxDyn_isPassedPreselection;
   Char_t          HGamEventInfoAuxDyn_isPassedPtCutsLowHighMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedRelPtCuts;
   Char_t          HGamEventInfoAuxDyn_isPassedTrigMatchLowHighMyy;
   Char_t          HGamEventInfoAuxDyn_isPassedTriggerMatch;
   Float_t         HGamEventInfoAuxDyn_m_alljet;
   Float_t         HGamEventInfoAuxDyn_m_alljet_30;
   Float_t         HGamEventInfoAuxDyn_m_ee;
   Float_t         HGamEventInfoAuxDyn_m_hybridtop2;
   Float_t         HGamEventInfoAuxDyn_m_jj;
   Float_t         HGamEventInfoAuxDyn_m_jj_30;
   Float_t         HGamEventInfoAuxDyn_m_jj_50;
   Float_t         HGamEventInfoAuxDyn_m_mumu;
   Float_t         HGamEventInfoAuxDyn_m_recotop1;
   Float_t         HGamEventInfoAuxDyn_m_yy;
   Float_t         HGamEventInfoAuxDyn_m_yy_hardestVertex;
   Float_t         HGamEventInfoAuxDyn_m_yy_resolution;
   Float_t         HGamEventInfoAuxDyn_m_yy_zCommon;
   Float_t         HGamEventInfoAuxDyn_m_yyj;
   Float_t         HGamEventInfoAuxDyn_m_yyj_30;
   Float_t         HGamEventInfoAuxDyn_m_yyjj;
   Float_t         HGamEventInfoAuxDyn_massTrans;
   Float_t         HGamEventInfoAuxDyn_maxTau_yyj_30;
   Float_t         HGamEventInfoAuxDyn_met_TST;
   Int_t           HGamEventInfoAuxDyn_met_cat;
   Float_t         HGamEventInfoAuxDyn_met_hardVertexTST;
   Float_t         HGamEventInfoAuxDyn_met_weight;
   Float_t         HGamEventInfoAuxDyn_mu;
   Int_t           HGamEventInfoAuxDyn_multiClassCatCoup_GlobalICHEP;
   Int_t           HGamEventInfoAuxDyn_multiClassCatCoup_HybridICHEP;
   Int_t           HGamEventInfoAuxDyn_numberOfPrimaryVertices;
   Float_t         HGamEventInfoAuxDyn_pT_hard;
   Float_t         HGamEventInfoAuxDyn_pT_hybridtop2;
   Float_t         HGamEventInfoAuxDyn_pT_j1;
   Float_t         HGamEventInfoAuxDyn_pT_j1_30;
   Float_t         HGamEventInfoAuxDyn_pT_j2;
   Float_t         HGamEventInfoAuxDyn_pT_j2_30;
   Float_t         HGamEventInfoAuxDyn_pT_j3_30;
   Float_t         HGamEventInfoAuxDyn_pT_jj;
   Float_t         HGamEventInfoAuxDyn_pT_recotop1;
   Float_t         HGamEventInfoAuxDyn_pT_y1;
   Float_t         HGamEventInfoAuxDyn_pT_y2;
   Float_t         HGamEventInfoAuxDyn_pT_yy;
   Float_t         HGamEventInfoAuxDyn_pT_yyj;
   Float_t         HGamEventInfoAuxDyn_pT_yyj_30;
   Float_t         HGamEventInfoAuxDyn_pT_yyjj;
   Float_t         HGamEventInfoAuxDyn_pT_yyjj_30;
   Float_t         HGamEventInfoAuxDyn_pT_yyjj_50;
   Float_t         HGamEventInfoAuxDyn_pTlepMET;
   Float_t         HGamEventInfoAuxDyn_pTt_yy;
   Char_t          HGamEventInfoAuxDyn_passCrackVetoCleaning;
   Char_t          HGamEventInfoAuxDyn_passMeyCut;
   Float_t         HGamEventInfoAuxDyn_phiStar_yy;
   Float_t         HGamEventInfoAuxDyn_phi_TST;
   Float_t         HGamEventInfoAuxDyn_phi_hardVertexTST;
   Float_t         HGamEventInfoAuxDyn_phi_hybridtop2;
   Float_t         HGamEventInfoAuxDyn_phi_recotop1;
   Float_t         HGamEventInfoAuxDyn_pileupVertexPhi;
   Float_t         HGamEventInfoAuxDyn_pileupVertexSumPt2;
   Float_t         HGamEventInfoAuxDyn_pileupVertexZ;
   Float_t         HGamEventInfoAuxDyn_pt_llmax;
   Float_t         HGamEventInfoAuxDyn_scoreBinaryCatCoup_GlobalICHEP;
   Float_t         HGamEventInfoAuxDyn_scoreBinaryCatCoup_HybridICHEP;
   Float_t         HGamEventInfoAuxDyn_score_MonoH_2var;
   Float_t         HGamEventInfoAuxDyn_score_WH_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_score_ZH_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_score_recotop1;
   Float_t         HGamEventInfoAuxDyn_score_recotop2;
   Float_t         HGamEventInfoAuxDyn_score_tHjb_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_score_tWH_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_score_ttH;
   Float_t         HGamEventInfoAuxDyn_score_ttHCP;
   Float_t         HGamEventInfoAuxDyn_score_ttH_ICHEP2020;
   Float_t         HGamEventInfoAuxDyn_selectedVertexPhi;
   Float_t         HGamEventInfoAuxDyn_selectedVertexSumPt2;
   Float_t         HGamEventInfoAuxDyn_selectedVertexZ;
   Float_t         HGamEventInfoAuxDyn_sumTau_yyj_30;
   Float_t         HGamEventInfoAuxDyn_sumet_TST;
   Float_t         HGamEventInfoAuxDyn_sumet_hardVertexTST;
   Float_t         HGamEventInfoAuxDyn_weight;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_GlobalICHEP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_HybridICHEP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_MonoH_2var;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_Moriond2017BDT;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFGlobalICHEP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFHybridICHEP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFMonoH_2var;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFMoriond2017BDT;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttH;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttHCP;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttH;
   Float_t         HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttHCP;
   Float_t         HGamEventInfoAuxDyn_weightCatXS_HiggsHF;
   Float_t         HGamEventInfoAuxDyn_weightCatXS_nbjet;
   Float_t         HGamEventInfoAuxDyn_weightCatXS_ttH;
   Float_t         HGamEventInfoAuxDyn_weightFJvt;
   Float_t         HGamEventInfoAuxDyn_weightFJvt_30;
   Float_t         HGamEventInfoAuxDyn_weightInitial;
   Float_t         HGamEventInfoAuxDyn_weightJvt;
   Float_t         HGamEventInfoAuxDyn_weightJvt_30;
   Float_t         HGamEventInfoAuxDyn_weightJvt_50;
   Float_t         HGamEventInfoAuxDyn_weightLowHighMyy;
   Float_t         HGamEventInfoAuxDyn_weightN_lep;
   Float_t         HGamEventInfoAuxDyn_weightN_lep_15;
   Float_t         HGamEventInfoAuxDyn_weightSF;
   Float_t         HGamEventInfoAuxDyn_yAbs_j1;
   Float_t         HGamEventInfoAuxDyn_yAbs_j1_30;
   Float_t         HGamEventInfoAuxDyn_yAbs_j2;
   Float_t         HGamEventInfoAuxDyn_yAbs_j2_30;
   Float_t         HGamEventInfoAuxDyn_yAbs_yy;
   Int_t           HGamEventInfoAuxDyn_yyb_cutFlow;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_m_jj;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_m_yyjj;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_cnstrnd;
   Float_t         HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_tilde;
   Float_t         HGamEventInfoAuxDyn_yybb_BReg_m_jj;
   Float_t         HGamEventInfoAuxDyn_yybb_BReg_m_yyjj;
   Float_t         HGamEventInfoAuxDyn_yybb_BReg_m_yyjj_cnstrnd;
   Float_t         HGamEventInfoAuxDyn_yybb_BReg_m_yyjj_tilde;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_jj;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_yyjj;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_yyjj_cnstrnd;
   Float_t         HGamEventInfoAuxDyn_yybb_KF_m_yyjj_tilde;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_yy_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_BReg_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_BReg_yy_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_KF_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_KF_yy_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_ttH_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_Res_BDT_yy_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_85_BCal_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_85_BReg_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_85_KF_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_85_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_BCal_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_BReg_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_KF_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_cutFlow;
   Int_t           HGamEventInfoAuxDyn_yybb_candidate_jet1;
   Int_t           HGamEventInfoAuxDyn_yybb_candidate_jet2;
   Float_t         HGamEventInfoAuxDyn_yybb_m_jj;
   Float_t         HGamEventInfoAuxDyn_yybb_m_yyjj;
   Float_t         HGamEventInfoAuxDyn_yybb_m_yyjj_cnstrnd;
   Float_t         HGamEventInfoAuxDyn_yybb_m_yyjj_tilde;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noHT_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noHT_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_withTop_lowMass_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noHT_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noHT_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_withTop_lowMass_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noHT_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noHT_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noMbb_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noMbb_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_withTop_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noHT_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noHT_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noMbb_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noMbb_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_withTop_lowMass_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noHT_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noHT_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noMbb_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noMbb_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_lowMass_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noHT_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noHT_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noMbb_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noMbb_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_withTop_lowMass_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_Cat;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noHT_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noHT_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noMbb_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noMbb_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_withTop_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noHT_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noHT_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noMbb_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noMbb_lowMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_withTop_highMass_Score;
   Float_t         HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_withTop_lowMass_Score;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_BCal_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_BReg_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_KF_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_BCal_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_BReg_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_KF_Cat;
   Int_t           HGamEventInfoAuxDyn_yybb_vbf_jet1;
   Int_t           HGamEventInfoAuxDyn_yybb_vbf_jet2;
   Float_t         HGamEventInfoAuxDyn_yybb_weight;
   Float_t         HGamEventInfoAuxDyn_zCommon;
   xAOD::EventInfo_v1 *EventInfo;
 //xAOD::EventAuxInfo_v1 *EventInfoAux_;
   xAOD::EventAuxInfo_v1 *EventInfoAux_xAOD__AuxInfoBase;
   UInt_t          EventInfoAux_runNumber;
   ULong64_t       EventInfoAux_eventNumber;
   UInt_t          EventInfoAux_lumiBlock;
   UInt_t          EventInfoAux_timeStamp;
   UInt_t          EventInfoAux_timeStampNSOffset;
   UInt_t          EventInfoAux_bcid;
   UInt_t          EventInfoAux_detectorMask0;
   UInt_t          EventInfoAux_detectorMask1;
   UInt_t          EventInfoAux_detectorMask2;
   UInt_t          EventInfoAux_detectorMask3;
   vector<pair<string,string> > EventInfoAux_detDescrTags;
   UInt_t          EventInfoAux_eventTypeBitmask;
   UInt_t          EventInfoAux_statusElement;
   UInt_t          EventInfoAux_extendedLevel1ID;
   UShort_t        EventInfoAux_level1TriggerType;
   vector<string>  EventInfoAux_streamTagNames;
   vector<string>  EventInfoAux_streamTagTypes;
   vector<char>    EventInfoAux_streamTagObeysLumiblock;
   Float_t         EventInfoAux_actualInteractionsPerCrossing;
   Float_t         EventInfoAux_averageInteractionsPerCrossing;
   UInt_t          EventInfoAux_pixelFlags;
   UInt_t          EventInfoAux_sctFlags;
   UInt_t          EventInfoAux_trtFlags;
   UInt_t          EventInfoAux_larFlags;
   UInt_t          EventInfoAux_tileFlags;
   UInt_t          EventInfoAux_muonFlags;
   UInt_t          EventInfoAux_forwardDetFlags;
   UInt_t          EventInfoAux_coreFlags;
   UInt_t          EventInfoAux_backgroundFlags;
   UInt_t          EventInfoAux_lumiFlags;
   Float_t         EventInfoAux_beamPosX;
   Float_t         EventInfoAux_beamPosY;
   Float_t         EventInfoAux_beamPosZ;
   Float_t         EventInfoAux_beamPosSigmaX;
   Float_t         EventInfoAux_beamPosSigmaY;
   Float_t         EventInfoAux_beamPosSigmaZ;
   Float_t         EventInfoAux_beamPosSigmaXY;
   Float_t         EventInfoAux_beamTiltXZ;
   Float_t         EventInfoAux_beamTiltYZ;
   UInt_t          EventInfoAux_beamStatus;
   Char_t          EventInfoAuxDyn_DFCommonJets_eventClean_LooseBad;
   Char_t          EventInfoAuxDyn_DFCommonJets_isBadBatman;
   Float_t         EventInfoAuxDyn_centralEventShapeDensity;
   Float_t         EventInfoAuxDyn_forwardEventShapeDensity;
   Char_t          EventInfoAuxDyn_passTrig_HLT_2g20_loose;
   Char_t          EventInfoAuxDyn_passTrig_HLT_2g20_tight;
   Char_t          EventInfoAuxDyn_passTrig_HLT_2g20_tight_icalotight_L12EM15VHI;
   Char_t          EventInfoAuxDyn_passTrig_HLT_2g20_tight_icalovloose_L12EM15VHI;
   Char_t          EventInfoAuxDyn_passTrig_HLT_2g22_tight;
   Char_t          EventInfoAuxDyn_passTrig_HLT_2g22_tight_L12EM15VHI;
   Char_t          EventInfoAuxDyn_passTrig_HLT_g120_loose;
   Char_t          EventInfoAuxDyn_passTrig_HLT_g140_loose;
   Char_t          EventInfoAuxDyn_passTrig_HLT_g35_loose_g25_loose;
   Char_t          EventInfoAuxDyn_passTrig_HLT_g35_medium_g25_medium_L12EM20VH;
   vector<double>  *BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pb;
   vector<double>  *BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pc;
   vector<double>  *BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pu;
   vector<double>  *BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pb;
   vector<double>  *BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pc;
   vector<double>  *BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pu;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_CorrJvf;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DFCommonJets_fJvt;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_60;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_70;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_77;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_85;
   vector<int>     *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_bin;
   vector<double>  *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pb;
   vector<double>  *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pc;
   vector<double>  *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pu;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_60;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_70;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_77;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_85;
   vector<int>     *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_bin;
   vector<double>  *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pb;
   vector<double>  *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pc;
   vector<double>  *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pu;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DetectorEta;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_85;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Jvt;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_NNRegPtSF;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_eta;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_m;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_phi;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_pt;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_PtRecoSF;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Rpt;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_85;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_fjvt;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_jvt;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Timing;
   Int_t           HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_;
   UInt_t          HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_m_persKey[kMaxHGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink];   //[HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.btaggingLink_]
   UInt_t          HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_m_persIndex[kMaxHGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink];   //[HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.btaggingLink_]
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_eta;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_m;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_overlapTau;
   vector<char>    *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_passFJVT;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_phi;
   vector<float>   *HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_pt;
   vector<double>  *BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pb;
   vector<double>  *BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pc;
   vector<double>  *BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pu;
   vector<double>  *BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pb;
   vector<double>  *BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pc;
   vector<double>  *BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pu;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_60;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_70;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_77;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_85;
   vector<int>     *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_bin;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_60;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_70;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_77;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_85;
   vector<int>     *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_bin;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DetectorEta;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_Jvt;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_60;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_70;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_77;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_85;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_jvt;
   Int_t           HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_;
   UInt_t          HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_m_persKey[kMaxHGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink];   //[HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.btaggingLink_]
   UInt_t          HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_m_persIndex[kMaxHGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink];   //[HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.btaggingLink_]
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_eta;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_m;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_overlapTau;
   vector<char>    *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_passFJVT;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_phi;
   vector<float>   *HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_pt;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_BDT;
   Int_t           HGamEventInfoAuxDyn_HiggsHF_BDT_cat;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dEta_yy;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dPhi_yy_leadBtagjet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_y1_leadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_y1_subleadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_y2_leadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_y2_subleadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_dR_yy;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_effmass_yyjets;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_foxw1_yyjets;
   Int_t           HGamEventInfoAuxDyn_HiggsHF_leadBtagJet_btagbin;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_eta;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_pt;
   Int_t           HGamEventInfoAuxDyn_HiggsHF_truth_label_LeadBtagJet;
   Float_t         HGamEventInfoAuxDyn_HiggsHF_weight;
   Int_t           HGamEventInfoAuxDyn_yyb_SelectedForwardJet;
   Int_t           HGamEventInfoAuxDyn_yyb_SelectedTaggedJet;
   Float_t         HGamEventInfoAuxDyn_yyb_VLQmass;
   Int_t           HGamEventInfoAuxDyn_yyb_m_yy_cat;
   Int_t           HGamEventInfoAuxDyn_yyb_truth_label_forwardjet;
   Int_t           HGamEventInfoAuxDyn_yyb_truth_label_taggedjet;
   Float_t         HGamEventInfoAuxDyn_yyb_weight;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_BCal_vbf_selected;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_BReg_vbf_selected;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_KF_vbf_selected;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_vbf_selected;
   vector<float>   *HGamTauJetsAuxDyn_BDTEleScore;
   vector<float>   *HGamTauJetsAuxDyn_BDTEleScoreSigTrans;
   vector<float>   *HGamTauJetsAuxDyn_BDTEleScoreSigTrans_retuned;
   vector<float>   *HGamTauJetsAuxDyn_RNNJetScore;
   vector<float>   *HGamTauJetsAuxDyn_RNNJetScoreSigTrans;
   vector<float>   *HGamTauJetsAuxDyn_charge;
   vector<float>   *HGamTauJetsAuxDyn_eta;
   vector<int>     *HGamTauJetsAuxDyn_nTracks;
   vector<char>    *HGamTauJetsAuxDyn_overlapJet;
   vector<float>   *HGamTauJetsAuxDyn_phi;
   vector<float>   *HGamTauJetsAuxDyn_pt;
   Float_t         HGamEventInfoAuxDyn_fcnc_fJvtweight;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_85_BCal_vbf_selected;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_85_BReg_vbf_selected;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_85_KF_vbf_selected;
   Int_t           HGamEventInfoAuxDyn_yybb_btag77_85_vbf_selected;
   vector<float>   *HGamMuonsAuxDyn_charge;
   vector<float>   *HGamMuonsAuxDyn_eta;
   vector<char>    *HGamMuonsAuxDyn_isBad;
   vector<char>    *HGamMuonsAuxDyn_isTight;
   vector<unsigned short> *HGamMuonsAuxDyn_muonType;
   vector<float>   *HGamMuonsAuxDyn_neflowisol20;
   vector<char>    *HGamMuonsAuxDyn_passIPCut;
   vector<float>   *HGamMuonsAuxDyn_phi;
   vector<float>   *HGamMuonsAuxDyn_pt;
   vector<float>   *HGamMuonsAuxDyn_ptcone20_TightTTVA_pt500;
   vector<float>   *HGamMuonsAuxDyn_ptvarcone30_TightTTVA_pt500;
   vector<unsigned short> *HGamElectronsAuxDyn_author;
   vector<float>   *HGamElectronsAuxDyn_charge;
   vector<float>   *HGamElectronsAuxDyn_eta;
   vector<float>   *HGamElectronsAuxDyn_eta_s2;
   vector<char>    *HGamElectronsAuxDyn_isTight;
   vector<float>   *HGamElectronsAuxDyn_m;
   vector<float>   *HGamElectronsAuxDyn_phi;
   vector<float>   *HGamElectronsAuxDyn_pt;
   vector<float>   *HGamElectronsAuxDyn_ptvarcone20_TightTTVA_pt1000;
   vector<float>   *HGamElectronsAuxDyn_topoetcone20;

   // List of branches
   TBranch        *b_HGamPhotons;   //!
   TBranch        *b_HGamPhotonsAux_;   //!
   TBranch        *b_HGamPhotonsAuxDyn_DeltaE;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Eratio;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Rconv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Reta;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Rhad;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Rhad1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_Rphi;   //!
   TBranch        *b_HGamPhotonsAuxDyn_author;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_E;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_E_TileGap3;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_Es0;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_Es1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_Es2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_Es3;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_eta;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_etaCalo;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_phiCalo;   //!
   TBranch        *b_HGamPhotonsAuxDyn_cl_ratioEs1Es2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_conversionType;   //!
   TBranch        *b_HGamPhotonsAuxDyn_convtrk1nPixHits;   //!
   TBranch        *b_HGamPhotonsAuxDyn_convtrk1nSCTHits;   //!
   TBranch        *b_HGamPhotonsAuxDyn_convtrk2nPixHits;   //!
   TBranch        *b_HGamPhotonsAuxDyn_convtrk2nSCTHits;   //!
   TBranch        *b_HGamPhotonsAuxDyn_e277;   //!
   TBranch        *b_HGamPhotonsAuxDyn_eta;   //!
   TBranch        *b_HGamPhotonsAuxDyn_eta_s1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_eta_s2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_f1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_fracs1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isEMTight;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isIsoFixedCutLoose;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isIsoFixedCutLooseCaloOnly;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isIsoFixedCutTight;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isIsoFixedCutTightCaloOnly;   //!
   TBranch        *b_HGamPhotonsAuxDyn_isTight;   //!
   TBranch        *b_HGamPhotonsAuxDyn_m;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_energy;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_eta;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_gain;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_onlId;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_phi;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_time;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_x;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_y;   //!
   TBranch        *b_HGamPhotonsAuxDyn_maxEcell_z;   //!
   TBranch        *b_HGamPhotonsAuxDyn_phi;   //!
   TBranch        *b_HGamPhotonsAuxDyn_pt;   //!
   TBranch        *b_HGamPhotonsAuxDyn_pt1conv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_pt2conv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_pt_s2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_ptcone20;   //!
   TBranch        *b_HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000;   //!
   TBranch        *b_HGamPhotonsAuxDyn_ptcone40;   //!
   TBranch        *b_HGamPhotonsAuxDyn_ptconv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_Es0;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_Es1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_Es2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_Es3;   //!
   TBranch        *b_HGamPhotonsAuxDyn_rawcl_ratioEs1Es2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_relEreso;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone20;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone20_DDcorr;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone20_SC;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone40;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone40_DDcorr;   //!
   TBranch        *b_HGamPhotonsAuxDyn_topoetcone40_SC;   //!
   TBranch        *b_HGamPhotonsAuxDyn_truthRconv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_weta1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_weta2;   //!
   TBranch        *b_HGamPhotonsAuxDyn_wtots1;   //!
   TBranch        *b_HGamPhotonsAuxDyn_zconv;   //!
   TBranch        *b_HGamPhotonsAuxDyn_zvertex;   //!
   TBranch        *b_HGamElectrons;   //!
   TBranch        *b_HGamElectronsAux_;   //!
   TBranch        *b_BTagging_HGamAntiKt4PFlowCustomVtxHgg;   //!
   TBranch        *b_BTagging_HGamAntiKt4PFlowCustomVtxHggAux_;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJets;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAux_;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlow_BTagging201903;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlow_BTagging201903Aux_;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903Aux_;   //!
   TBranch        *b_HGamMuons;   //!
   TBranch        *b_HGamMuonsAux_;   //!
   TBranch        *b_HGamTauJets;   //!
   TBranch        *b_HGamTauJetsAux_;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlow;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAux_;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source;   //!
   TBranch        *b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet;   //!
   TBranch        *b_HGamEventInfo;   //!
   TBranch        *b_HGamEventInfoAux_;   //!
   TBranch        *b_HGamEventInfoAuxDyn_DR_y_y;   //!
   TBranch        *b_HGamEventInfoAuxDyn_DRmin_y_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_DRmin_y_j_2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Deta_j_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j_30_signed;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_j_j_50_signed;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_y_y;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_yy_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_yy_jj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dphi_yy_jj_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_j_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_j_j_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_y_y;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_yy_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Dy_yy_jj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_E_y1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_E_y2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HT_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HTall_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_cutFlowAllJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_cutFlowLeadJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_jet_truthlabel;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_leadJet_truthLabel;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_m_yy_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_y1_j_dr;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_y2_j_dr;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HggPrimaryVerticesScore;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HggPrimaryVerticesZ;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_NLoosePhotons;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_e;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_btag;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_btag30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_central;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_j_central30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_lep;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_lep_15;   //!
   TBranch        *b_HGamEventInfoAuxDyn_N_mu;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Zepp;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_GlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_HybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_MonoH_2var;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_Moriond2017BDT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_XGBoost_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catCoup_XGBoost_ttHCP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catLowHighMyy_conv;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_Run1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_conv;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_eta;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_mu;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catMass_pT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catXS_HiggsHF;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catXS_VBF;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catXS_nbjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_catXS_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_cosTS_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_cosTS_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_eta_hybridtop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_eta_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_etm;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_icat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_idLep;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_jetInd;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_mT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_mTop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_mTop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_nbjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_ncbTop12;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_njet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_njet30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_ntopc;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_phim;   //!
   TBranch        *b_HGamEventInfoAuxDyn_hardestVertexPhi;   //!
   TBranch        *b_HGamEventInfoAuxDyn_hardestVertexSumPt2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_hardestVertexZ;   //!
   TBranch        *b_HGamEventInfoAuxDyn_idx_jets_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_idx_jets_recotop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassed;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedBasic;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedIsolation;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedIsolationHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedIsolationLowMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedJetEventClean;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedLowMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedMassCut;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedPID;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedPreselection;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedPtCutsLowHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedRelPtCuts;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedTrigMatchLowHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassedTriggerMatch;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_alljet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_alljet_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_ee;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_hybridtop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_jj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_jj_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_mumu;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy_hardestVertex;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy_resolution;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy_zCommon;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yyj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yyj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_massTrans;   //!
   TBranch        *b_HGamEventInfoAuxDyn_maxTau_yyj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_met_TST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_met_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_met_hardVertexTST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_met_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_mu;   //!
   TBranch        *b_HGamEventInfoAuxDyn_multiClassCatCoup_GlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_multiClassCatCoup_HybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_numberOfPrimaryVertices;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_hard;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_hybridtop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j1_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j2_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_j3_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_y1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_y2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyjj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pT_yyjj_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pTlepMET;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pTt_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_passCrackVetoCleaning;   //!
   TBranch        *b_HGamEventInfoAuxDyn_passMeyCut;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phiStar_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phi_TST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phi_hardVertexTST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phi_hybridtop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_phi_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pileupVertexPhi;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pileupVertexSumPt2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pileupVertexZ;   //!
   TBranch        *b_HGamEventInfoAuxDyn_pt_llmax;   //!
   TBranch        *b_HGamEventInfoAuxDyn_scoreBinaryCatCoup_GlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_scoreBinaryCatCoup_HybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_MonoH_2var;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_WH_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_ZH_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_recotop1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_recotop2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_tHjb_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_tWH_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_ttHCP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_score_ttH_ICHEP2020;   //!
   TBranch        *b_HGamEventInfoAuxDyn_selectedVertexPhi;   //!
   TBranch        *b_HGamEventInfoAuxDyn_selectedVertexSumPt2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_selectedVertexZ;   //!
   TBranch        *b_HGamEventInfoAuxDyn_sumTau_yyj_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_sumet_TST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_sumet_hardVertexTST;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_GlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_HybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_MonoH_2var;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_Moriond2017BDT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFGlobalICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFHybridICHEP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFMonoH_2var;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFMoriond2017BDT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttHCP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttHCP;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatXS_HiggsHF;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatXS_nbjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightCatXS_ttH;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightFJvt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightFJvt_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightInitial;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightJvt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightJvt_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightJvt_50;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightLowHighMyy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightN_lep;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightN_lep_15;   //!
   TBranch        *b_HGamEventInfoAuxDyn_weightSF;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_j1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_j1_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_j2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_j2_30;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yAbs_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_m_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_cnstrnd;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_tilde;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BReg_m_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BReg_m_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BReg_m_yyjj_cnstrnd;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_BReg_m_yyjj_tilde;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj_cnstrnd;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj_tilde;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_BReg_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_BReg_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_KF_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_KF_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_ttH_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_Res_BDT_yy_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_85_BCal_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_85_BReg_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_85_KF_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_85_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_BCal_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_BReg_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_KF_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_cutFlow;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_candidate_jet1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_candidate_jet2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_m_jj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_m_yyjj;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_m_yyjj_cnstrnd;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_m_yyjj_tilde;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noHT_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noHT_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noHT_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noHT_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noHT_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noHT_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noMbb_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noMbb_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noHT_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noHT_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noMbb_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noMbb_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noHT_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noHT_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noMbb_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noMbb_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noHT_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noHT_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noMbb_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noMbb_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noHT_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noHT_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noMbb_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noMbb_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noHT_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noHT_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noMbb_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noMbb_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_withTop_highMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_withTop_lowMass_Score;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_BCal_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_BReg_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_KF_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_BCal_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_BReg_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_KF_Cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_vbf_jet1;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_vbf_jet2;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_zCommon;   //!
   TBranch        *b_EventInfo;   //!
   TBranch        *b_EventInfoAux_xAOD__AuxInfoBase;   //!
   TBranch        *b_EventInfoAux_runNumber;   //!
   TBranch        *b_EventInfoAux_eventNumber;   //!
   TBranch        *b_EventInfoAux_lumiBlock;   //!
   TBranch        *b_EventInfoAux_timeStamp;   //!
   TBranch        *b_EventInfoAux_timeStampNSOffset;   //!
   TBranch        *b_EventInfoAux_bcid;   //!
   TBranch        *b_EventInfoAux_detectorMask0;   //!
   TBranch        *b_EventInfoAux_detectorMask1;   //!
   TBranch        *b_EventInfoAux_detectorMask2;   //!
   TBranch        *b_EventInfoAux_detectorMask3;   //!
   TBranch        *b_EventInfoAux_detDescrTags;   //!
   TBranch        *b_EventInfoAux_eventTypeBitmask;   //!
   TBranch        *b_EventInfoAux_statusElement;   //!
   TBranch        *b_EventInfoAux_extendedLevel1ID;   //!
   TBranch        *b_EventInfoAux_level1TriggerType;   //!
   TBranch        *b_EventInfoAux_streamTagNames;   //!
   TBranch        *b_EventInfoAux_streamTagTypes;   //!
   TBranch        *b_EventInfoAux_streamTagObeysLumiblock;   //!
   TBranch        *b_EventInfoAux_actualInteractionsPerCrossing;   //!
   TBranch        *b_EventInfoAux_averageInteractionsPerCrossing;   //!
   TBranch        *b_EventInfoAux_pixelFlags;   //!
   TBranch        *b_EventInfoAux_sctFlags;   //!
   TBranch        *b_EventInfoAux_trtFlags;   //!
   TBranch        *b_EventInfoAux_larFlags;   //!
   TBranch        *b_EventInfoAux_tileFlags;   //!
   TBranch        *b_EventInfoAux_muonFlags;   //!
   TBranch        *b_EventInfoAux_forwardDetFlags;   //!
   TBranch        *b_EventInfoAux_coreFlags;   //!
   TBranch        *b_EventInfoAux_backgroundFlags;   //!
   TBranch        *b_EventInfoAux_lumiFlags;   //!
   TBranch        *b_EventInfoAux_beamPosX;   //!
   TBranch        *b_EventInfoAux_beamPosY;   //!
   TBranch        *b_EventInfoAux_beamPosZ;   //!
   TBranch        *b_EventInfoAux_beamPosSigmaX;   //!
   TBranch        *b_EventInfoAux_beamPosSigmaY;   //!
   TBranch        *b_EventInfoAux_beamPosSigmaZ;   //!
   TBranch        *b_EventInfoAux_beamPosSigmaXY;   //!
   TBranch        *b_EventInfoAux_beamTiltXZ;   //!
   TBranch        *b_EventInfoAux_beamTiltYZ;   //!
   TBranch        *b_EventInfoAux_beamStatus;   //!
   TBranch        *b_EventInfoAuxDyn_DFCommonJets_eventClean_LooseBad;   //!
   TBranch        *b_EventInfoAuxDyn_DFCommonJets_isBadBatman;   //!
   TBranch        *b_EventInfoAuxDyn_centralEventShapeDensity;   //!
   TBranch        *b_EventInfoAuxDyn_forwardEventShapeDensity;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_2g20_loose;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_2g20_tight;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_2g20_tight_icalotight_L12EM15VHI;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_2g20_tight_icalovloose_L12EM15VHI;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_2g22_tight;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_2g22_tight_L12EM15VHI;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_g120_loose;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_g140_loose;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_g35_loose_g25_loose;   //!
   TBranch        *b_EventInfoAuxDyn_passTrig_HLT_g35_medium_g25_medium_L12EM20VH;   //!
   TBranch        *b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pb;   //!
   TBranch        *b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pc;   //!
   TBranch        *b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pu;   //!
   TBranch        *b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pb;   //!
   TBranch        *b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pc;   //!
   TBranch        *b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pu;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_CorrJvf;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DFCommonJets_fJvt;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_bin;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pb;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pc;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pu;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_bin;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pb;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pc;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pu;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DetectorEta;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Jvt;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_NNRegPtSF;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_eta;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_m;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_phi;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_pt;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_PtRecoSF;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Rpt;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_fjvt;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_jvt;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Timing;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_m_persKey;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_m_persIndex;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_eta;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_m;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_overlapTau;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_passFJVT;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_phi;   //!
   TBranch        *b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_pt;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pb;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pc;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pu;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pb;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pc;   //!
   TBranch        *b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pu;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_bin;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_bin;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DetectorEta;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_Jvt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_60;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_70;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_77;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_85;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_jvt;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_m_persKey;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_m_persIndex;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_eta;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_m;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_overlapTau;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_passFJVT;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_phi;   //!
   TBranch        *b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_pt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_BDT;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_BDT_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dEta_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dPhi_yy_leadBtagjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_y1_leadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_y1_subleadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_y2_leadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_y2_subleadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_dR_yy;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_effmass_yyjets;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_foxw1_yyjets;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_leadBtagJet_btagbin;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_eta;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_pt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_truth_label_LeadBtagJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_HiggsHF_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_SelectedForwardJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_SelectedTaggedJet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_VLQmass;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_m_yy_cat;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_truth_label_forwardjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_truth_label_taggedjet;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yyb_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_BCal_vbf_selected;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_BReg_vbf_selected;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_KF_vbf_selected;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_vbf_selected;   //!
   TBranch        *b_HGamTauJetsAuxDyn_BDTEleScore;   //!
   TBranch        *b_HGamTauJetsAuxDyn_BDTEleScoreSigTrans;   //!
   TBranch        *b_HGamTauJetsAuxDyn_BDTEleScoreSigTrans_retuned;   //!
   TBranch        *b_HGamTauJetsAuxDyn_RNNJetScore;   //!
   TBranch        *b_HGamTauJetsAuxDyn_RNNJetScoreSigTrans;   //!
   TBranch        *b_HGamTauJetsAuxDyn_charge;   //!
   TBranch        *b_HGamTauJetsAuxDyn_eta;   //!
   TBranch        *b_HGamTauJetsAuxDyn_nTracks;   //!
   TBranch        *b_HGamTauJetsAuxDyn_overlapJet;   //!
   TBranch        *b_HGamTauJetsAuxDyn_phi;   //!
   TBranch        *b_HGamTauJetsAuxDyn_pt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_fcnc_fJvtweight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_85_BCal_vbf_selected;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_85_BReg_vbf_selected;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_85_KF_vbf_selected;   //!
   TBranch        *b_HGamEventInfoAuxDyn_yybb_btag77_85_vbf_selected;   //!
   TBranch        *b_HGamMuonsAuxDyn_charge;   //!
   TBranch        *b_HGamMuonsAuxDyn_eta;   //!
   TBranch        *b_HGamMuonsAuxDyn_isBad;   //!
   TBranch        *b_HGamMuonsAuxDyn_isTight;   //!
   TBranch        *b_HGamMuonsAuxDyn_muonType;   //!
   TBranch        *b_HGamMuonsAuxDyn_neflowisol20;   //!
   TBranch        *b_HGamMuonsAuxDyn_passIPCut;   //!
   TBranch        *b_HGamMuonsAuxDyn_phi;   //!
   TBranch        *b_HGamMuonsAuxDyn_pt;   //!
   TBranch        *b_HGamMuonsAuxDyn_ptcone20_TightTTVA_pt500;   //!
   TBranch        *b_HGamMuonsAuxDyn_ptvarcone30_TightTTVA_pt500;   //!
   TBranch        *b_HGamElectronsAuxDyn_author;   //!
   TBranch        *b_HGamElectronsAuxDyn_charge;   //!
   TBranch        *b_HGamElectronsAuxDyn_eta;   //!
   TBranch        *b_HGamElectronsAuxDyn_eta_s2;   //!
   TBranch        *b_HGamElectronsAuxDyn_isTight;   //!
   TBranch        *b_HGamElectronsAuxDyn_m;   //!
   TBranch        *b_HGamElectronsAuxDyn_phi;   //!
   TBranch        *b_HGamElectronsAuxDyn_pt;   //!
   TBranch        *b_HGamElectronsAuxDyn_ptvarcone20_TightTTVA_pt1000;   //!
   TBranch        *b_HGamElectronsAuxDyn_topoetcone20;   //!

   data(TTree *tree=0);
   virtual ~data();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef data_cxx
data::data(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/data15/data15_13TeV.DS1_3220ipb.physics_Main.MxAOD.p4205.h026.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/data15/data15_13TeV.DS1_3220ipb.physics_Main.MxAOD.p4205.h026.root");
      }
      f->GetObject("CollectionTree",tree);

   }
   Init(tree);
}

data::~data()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t data::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t data::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void data::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   HGamPhotons = 0;
   HGamPhotonsAux_ = 0;
   HGamPhotonsAuxDyn_DeltaE = 0;
   HGamPhotonsAuxDyn_Eratio = 0;
   HGamPhotonsAuxDyn_Rconv = 0;
   HGamPhotonsAuxDyn_Reta = 0;
   HGamPhotonsAuxDyn_Rhad = 0;
   HGamPhotonsAuxDyn_Rhad1 = 0;
   HGamPhotonsAuxDyn_Rphi = 0;
   HGamPhotonsAuxDyn_author = 0;
   HGamPhotonsAuxDyn_cl_E = 0;
   HGamPhotonsAuxDyn_cl_E_TileGap3 = 0;
   HGamPhotonsAuxDyn_cl_Es0 = 0;
   HGamPhotonsAuxDyn_cl_Es1 = 0;
   HGamPhotonsAuxDyn_cl_Es2 = 0;
   HGamPhotonsAuxDyn_cl_Es3 = 0;
   HGamPhotonsAuxDyn_cl_eta = 0;
   HGamPhotonsAuxDyn_cl_etaCalo = 0;
   HGamPhotonsAuxDyn_cl_phiCalo = 0;
   HGamPhotonsAuxDyn_cl_ratioEs1Es2 = 0;
   HGamPhotonsAuxDyn_conversionType = 0;
   HGamPhotonsAuxDyn_convtrk1nPixHits = 0;
   HGamPhotonsAuxDyn_convtrk1nSCTHits = 0;
   HGamPhotonsAuxDyn_convtrk2nPixHits = 0;
   HGamPhotonsAuxDyn_convtrk2nSCTHits = 0;
   HGamPhotonsAuxDyn_e277 = 0;
   HGamPhotonsAuxDyn_eta = 0;
   HGamPhotonsAuxDyn_eta_s1 = 0;
   HGamPhotonsAuxDyn_eta_s2 = 0;
   HGamPhotonsAuxDyn_f1 = 0;
   HGamPhotonsAuxDyn_fracs1 = 0;
   HGamPhotonsAuxDyn_isEMTight = 0;
   HGamPhotonsAuxDyn_isIsoFixedCutLoose = 0;
   HGamPhotonsAuxDyn_isIsoFixedCutLooseCaloOnly = 0;
   HGamPhotonsAuxDyn_isIsoFixedCutTight = 0;
   HGamPhotonsAuxDyn_isIsoFixedCutTightCaloOnly = 0;
   HGamPhotonsAuxDyn_isTight = 0;
   HGamPhotonsAuxDyn_m = 0;
   HGamPhotonsAuxDyn_maxEcell_energy = 0;
   HGamPhotonsAuxDyn_maxEcell_eta = 0;
   HGamPhotonsAuxDyn_maxEcell_gain = 0;
   HGamPhotonsAuxDyn_maxEcell_onlId = 0;
   HGamPhotonsAuxDyn_maxEcell_phi = 0;
   HGamPhotonsAuxDyn_maxEcell_time = 0;
   HGamPhotonsAuxDyn_maxEcell_x = 0;
   HGamPhotonsAuxDyn_maxEcell_y = 0;
   HGamPhotonsAuxDyn_maxEcell_z = 0;
   HGamPhotonsAuxDyn_phi = 0;
   HGamPhotonsAuxDyn_pt = 0;
   HGamPhotonsAuxDyn_pt1conv = 0;
   HGamPhotonsAuxDyn_pt2conv = 0;
   HGamPhotonsAuxDyn_pt_s2 = 0;
   HGamPhotonsAuxDyn_ptcone20 = 0;
   HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000 = 0;
   HGamPhotonsAuxDyn_ptcone40 = 0;
   HGamPhotonsAuxDyn_ptconv = 0;
   HGamPhotonsAuxDyn_rawcl_Es0 = 0;
   HGamPhotonsAuxDyn_rawcl_Es1 = 0;
   HGamPhotonsAuxDyn_rawcl_Es2 = 0;
   HGamPhotonsAuxDyn_rawcl_Es3 = 0;
   HGamPhotonsAuxDyn_rawcl_ratioEs1Es2 = 0;
   HGamPhotonsAuxDyn_relEreso = 0;
   HGamPhotonsAuxDyn_topoetcone20 = 0;
   HGamPhotonsAuxDyn_topoetcone20_DDcorr = 0;
   HGamPhotonsAuxDyn_topoetcone20_SC = 0;
   HGamPhotonsAuxDyn_topoetcone40 = 0;
   HGamPhotonsAuxDyn_topoetcone40_DDcorr = 0;
   HGamPhotonsAuxDyn_topoetcone40_SC = 0;
   HGamPhotonsAuxDyn_truthRconv = 0;
   HGamPhotonsAuxDyn_weta1 = 0;
   HGamPhotonsAuxDyn_weta2 = 0;
   HGamPhotonsAuxDyn_wtots1 = 0;
   HGamPhotonsAuxDyn_zconv = 0;
   HGamPhotonsAuxDyn_zvertex = 0;
   HGamElectrons = 0;
   HGamElectronsAux_ = 0;
   BTagging_HGamAntiKt4PFlowCustomVtxHgg = 0;
   BTagging_HGamAntiKt4PFlowCustomVtxHggAux_ = 0;
   HGamAntiKt4PFlowCustomVtxHggJets = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAux_ = 0;
   BTagging_HGamAntiKt4EMPFlow_BTagging201903 = 0;
   BTagging_HGamAntiKt4EMPFlow_BTagging201903Aux_ = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903Aux_ = 0;
   HGamMuons = 0;
   HGamMuonsAux_ = 0;
   HGamTauJets = 0;
   HGamTauJetsAux_ = 0;
   HGamMET_Reference_AntiKt4EMPFlow = 0;
   HGamMET_Reference_AntiKt4EMPFlowAux_ = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source = 0;
   HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet = 0;
   HGamEventInfo = 0;
   HGamEventInfoAux_ = 0;
   HGamEventInfoAuxDyn_Hc_jet_truthlabel = 0;
   HGamEventInfoAuxDyn_Hc_y1_j_dr = 0;
   HGamEventInfoAuxDyn_Hc_y2_j_dr = 0;
   HGamEventInfoAuxDyn_HggPrimaryVerticesScore = 0;
   HGamEventInfoAuxDyn_HggPrimaryVerticesZ = 0;
   HGamEventInfoAuxDyn_fcnc_Score = 0;
   HGamEventInfoAuxDyn_fcnc_icat = 0;
   HGamEventInfoAuxDyn_fcnc_jetInd = 0;
   HGamEventInfoAuxDyn_fcnc_mTop1 = 0;
   HGamEventInfoAuxDyn_fcnc_mTop2 = 0;
   HGamEventInfoAuxDyn_fcnc_ncbTop12 = 0;
   HGamEventInfoAuxDyn_idx_jets_recotop1 = 0;
   HGamEventInfoAuxDyn_idx_jets_recotop2 = 0;
   EventInfo = 0;
   EventInfoAux_xAOD__AuxInfoBase = 0;
   BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pb = 0;
   BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pc = 0;
   BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pu = 0;
   BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pb = 0;
   BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pc = 0;
   BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pu = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_CorrJvf = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DFCommonJets_fJvt = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_60 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_70 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_77 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_85 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_bin = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pb = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pc = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pu = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_60 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_70 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_77 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_85 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_bin = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pb = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pc = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pu = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DetectorEta = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_60 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_70 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_77 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_85 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Jvt = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_NNRegPtSF = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_eta = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_m = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_phi = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_pt = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_PtRecoSF = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Rpt = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_60 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_70 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_77 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_85 = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_fjvt = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_jvt = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Timing = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_eta = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_m = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_overlapTau = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_passFJVT = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_phi = 0;
   HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_pt = 0;
   BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pb = 0;
   BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pc = 0;
   BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pu = 0;
   BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pb = 0;
   BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pc = 0;
   BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pu = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_bin = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_bin = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DetectorEta = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_Jvt = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_60 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_70 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_77 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_85 = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_jvt = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_eta = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_m = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_overlapTau = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_passFJVT = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_phi = 0;
   HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_pt = 0;
   HGamTauJetsAuxDyn_BDTEleScore = 0;
   HGamTauJetsAuxDyn_BDTEleScoreSigTrans = 0;
   HGamTauJetsAuxDyn_BDTEleScoreSigTrans_retuned = 0;
   HGamTauJetsAuxDyn_RNNJetScore = 0;
   HGamTauJetsAuxDyn_RNNJetScoreSigTrans = 0;
   HGamTauJetsAuxDyn_charge = 0;
   HGamTauJetsAuxDyn_eta = 0;
   HGamTauJetsAuxDyn_nTracks = 0;
   HGamTauJetsAuxDyn_overlapJet = 0;
   HGamTauJetsAuxDyn_phi = 0;
   HGamTauJetsAuxDyn_pt = 0;
   HGamMuonsAuxDyn_charge = 0;
   HGamMuonsAuxDyn_eta = 0;
   HGamMuonsAuxDyn_isBad = 0;
   HGamMuonsAuxDyn_isTight = 0;
   HGamMuonsAuxDyn_muonType = 0;
   HGamMuonsAuxDyn_neflowisol20 = 0;
   HGamMuonsAuxDyn_passIPCut = 0;
   HGamMuonsAuxDyn_phi = 0;
   HGamMuonsAuxDyn_pt = 0;
   HGamMuonsAuxDyn_ptcone20_TightTTVA_pt500 = 0;
   HGamMuonsAuxDyn_ptvarcone30_TightTTVA_pt500 = 0;
   HGamElectronsAuxDyn_author = 0;
   HGamElectronsAuxDyn_charge = 0;
   HGamElectronsAuxDyn_eta = 0;
   HGamElectronsAuxDyn_eta_s2 = 0;
   HGamElectronsAuxDyn_isTight = 0;
   HGamElectronsAuxDyn_m = 0;
   HGamElectronsAuxDyn_phi = 0;
   HGamElectronsAuxDyn_pt = 0;
   HGamElectronsAuxDyn_ptvarcone20_TightTTVA_pt1000 = 0;
   HGamElectronsAuxDyn_topoetcone20 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("HGamPhotons", &HGamPhotons, &b_HGamPhotons);
   fChain->SetBranchAddress("HGamPhotonsAux.", &HGamPhotonsAux_, &b_HGamPhotonsAux_);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.DeltaE", &HGamPhotonsAuxDyn_DeltaE, &b_HGamPhotonsAuxDyn_DeltaE);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Eratio", &HGamPhotonsAuxDyn_Eratio, &b_HGamPhotonsAuxDyn_Eratio);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Rconv", &HGamPhotonsAuxDyn_Rconv, &b_HGamPhotonsAuxDyn_Rconv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Reta", &HGamPhotonsAuxDyn_Reta, &b_HGamPhotonsAuxDyn_Reta);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Rhad", &HGamPhotonsAuxDyn_Rhad, &b_HGamPhotonsAuxDyn_Rhad);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Rhad1", &HGamPhotonsAuxDyn_Rhad1, &b_HGamPhotonsAuxDyn_Rhad1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.Rphi", &HGamPhotonsAuxDyn_Rphi, &b_HGamPhotonsAuxDyn_Rphi);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.author", &HGamPhotonsAuxDyn_author, &b_HGamPhotonsAuxDyn_author);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_E", &HGamPhotonsAuxDyn_cl_E, &b_HGamPhotonsAuxDyn_cl_E);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_E_TileGap3", &HGamPhotonsAuxDyn_cl_E_TileGap3, &b_HGamPhotonsAuxDyn_cl_E_TileGap3);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_Es0", &HGamPhotonsAuxDyn_cl_Es0, &b_HGamPhotonsAuxDyn_cl_Es0);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_Es1", &HGamPhotonsAuxDyn_cl_Es1, &b_HGamPhotonsAuxDyn_cl_Es1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_Es2", &HGamPhotonsAuxDyn_cl_Es2, &b_HGamPhotonsAuxDyn_cl_Es2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_Es3", &HGamPhotonsAuxDyn_cl_Es3, &b_HGamPhotonsAuxDyn_cl_Es3);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_eta", &HGamPhotonsAuxDyn_cl_eta, &b_HGamPhotonsAuxDyn_cl_eta);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_etaCalo", &HGamPhotonsAuxDyn_cl_etaCalo, &b_HGamPhotonsAuxDyn_cl_etaCalo);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_phiCalo", &HGamPhotonsAuxDyn_cl_phiCalo, &b_HGamPhotonsAuxDyn_cl_phiCalo);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.cl_ratioEs1Es2", &HGamPhotonsAuxDyn_cl_ratioEs1Es2, &b_HGamPhotonsAuxDyn_cl_ratioEs1Es2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.conversionType", &HGamPhotonsAuxDyn_conversionType, &b_HGamPhotonsAuxDyn_conversionType);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.convtrk1nPixHits", &HGamPhotonsAuxDyn_convtrk1nPixHits, &b_HGamPhotonsAuxDyn_convtrk1nPixHits);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.convtrk1nSCTHits", &HGamPhotonsAuxDyn_convtrk1nSCTHits, &b_HGamPhotonsAuxDyn_convtrk1nSCTHits);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.convtrk2nPixHits", &HGamPhotonsAuxDyn_convtrk2nPixHits, &b_HGamPhotonsAuxDyn_convtrk2nPixHits);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.convtrk2nSCTHits", &HGamPhotonsAuxDyn_convtrk2nSCTHits, &b_HGamPhotonsAuxDyn_convtrk2nSCTHits);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.e277", &HGamPhotonsAuxDyn_e277, &b_HGamPhotonsAuxDyn_e277);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.eta", &HGamPhotonsAuxDyn_eta, &b_HGamPhotonsAuxDyn_eta);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.eta_s1", &HGamPhotonsAuxDyn_eta_s1, &b_HGamPhotonsAuxDyn_eta_s1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.eta_s2", &HGamPhotonsAuxDyn_eta_s2, &b_HGamPhotonsAuxDyn_eta_s2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.f1", &HGamPhotonsAuxDyn_f1, &b_HGamPhotonsAuxDyn_f1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.fracs1", &HGamPhotonsAuxDyn_fracs1, &b_HGamPhotonsAuxDyn_fracs1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isEMTight", &HGamPhotonsAuxDyn_isEMTight, &b_HGamPhotonsAuxDyn_isEMTight);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isIsoFixedCutLoose", &HGamPhotonsAuxDyn_isIsoFixedCutLoose, &b_HGamPhotonsAuxDyn_isIsoFixedCutLoose);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isIsoFixedCutLooseCaloOnly", &HGamPhotonsAuxDyn_isIsoFixedCutLooseCaloOnly, &b_HGamPhotonsAuxDyn_isIsoFixedCutLooseCaloOnly);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isIsoFixedCutTight", &HGamPhotonsAuxDyn_isIsoFixedCutTight, &b_HGamPhotonsAuxDyn_isIsoFixedCutTight);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isIsoFixedCutTightCaloOnly", &HGamPhotonsAuxDyn_isIsoFixedCutTightCaloOnly, &b_HGamPhotonsAuxDyn_isIsoFixedCutTightCaloOnly);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.isTight", &HGamPhotonsAuxDyn_isTight, &b_HGamPhotonsAuxDyn_isTight);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.m", &HGamPhotonsAuxDyn_m, &b_HGamPhotonsAuxDyn_m);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_energy", &HGamPhotonsAuxDyn_maxEcell_energy, &b_HGamPhotonsAuxDyn_maxEcell_energy);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_eta", &HGamPhotonsAuxDyn_maxEcell_eta, &b_HGamPhotonsAuxDyn_maxEcell_eta);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_gain", &HGamPhotonsAuxDyn_maxEcell_gain, &b_HGamPhotonsAuxDyn_maxEcell_gain);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_onlId", &HGamPhotonsAuxDyn_maxEcell_onlId, &b_HGamPhotonsAuxDyn_maxEcell_onlId);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_phi", &HGamPhotonsAuxDyn_maxEcell_phi, &b_HGamPhotonsAuxDyn_maxEcell_phi);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_time", &HGamPhotonsAuxDyn_maxEcell_time, &b_HGamPhotonsAuxDyn_maxEcell_time);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_x", &HGamPhotonsAuxDyn_maxEcell_x, &b_HGamPhotonsAuxDyn_maxEcell_x);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_y", &HGamPhotonsAuxDyn_maxEcell_y, &b_HGamPhotonsAuxDyn_maxEcell_y);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.maxEcell_z", &HGamPhotonsAuxDyn_maxEcell_z, &b_HGamPhotonsAuxDyn_maxEcell_z);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.phi", &HGamPhotonsAuxDyn_phi, &b_HGamPhotonsAuxDyn_phi);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.pt", &HGamPhotonsAuxDyn_pt, &b_HGamPhotonsAuxDyn_pt);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.pt1conv", &HGamPhotonsAuxDyn_pt1conv, &b_HGamPhotonsAuxDyn_pt1conv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.pt2conv", &HGamPhotonsAuxDyn_pt2conv, &b_HGamPhotonsAuxDyn_pt2conv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.pt_s2", &HGamPhotonsAuxDyn_pt_s2, &b_HGamPhotonsAuxDyn_pt_s2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.ptcone20", &HGamPhotonsAuxDyn_ptcone20, &b_HGamPhotonsAuxDyn_ptcone20);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.ptcone20_TightTTVA_pt1000", &HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000, &b_HGamPhotonsAuxDyn_ptcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.ptcone40", &HGamPhotonsAuxDyn_ptcone40, &b_HGamPhotonsAuxDyn_ptcone40);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.ptconv", &HGamPhotonsAuxDyn_ptconv, &b_HGamPhotonsAuxDyn_ptconv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_Es0", &HGamPhotonsAuxDyn_rawcl_Es0, &b_HGamPhotonsAuxDyn_rawcl_Es0);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_Es1", &HGamPhotonsAuxDyn_rawcl_Es1, &b_HGamPhotonsAuxDyn_rawcl_Es1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_Es2", &HGamPhotonsAuxDyn_rawcl_Es2, &b_HGamPhotonsAuxDyn_rawcl_Es2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_Es3", &HGamPhotonsAuxDyn_rawcl_Es3, &b_HGamPhotonsAuxDyn_rawcl_Es3);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.rawcl_ratioEs1Es2", &HGamPhotonsAuxDyn_rawcl_ratioEs1Es2, &b_HGamPhotonsAuxDyn_rawcl_ratioEs1Es2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.relEreso", &HGamPhotonsAuxDyn_relEreso, &b_HGamPhotonsAuxDyn_relEreso);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone20", &HGamPhotonsAuxDyn_topoetcone20, &b_HGamPhotonsAuxDyn_topoetcone20);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone20_DDcorr", &HGamPhotonsAuxDyn_topoetcone20_DDcorr, &b_HGamPhotonsAuxDyn_topoetcone20_DDcorr);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone20_SC", &HGamPhotonsAuxDyn_topoetcone20_SC, &b_HGamPhotonsAuxDyn_topoetcone20_SC);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone40", &HGamPhotonsAuxDyn_topoetcone40, &b_HGamPhotonsAuxDyn_topoetcone40);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone40_DDcorr", &HGamPhotonsAuxDyn_topoetcone40_DDcorr, &b_HGamPhotonsAuxDyn_topoetcone40_DDcorr);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.topoetcone40_SC", &HGamPhotonsAuxDyn_topoetcone40_SC, &b_HGamPhotonsAuxDyn_topoetcone40_SC);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.truthRconv", &HGamPhotonsAuxDyn_truthRconv, &b_HGamPhotonsAuxDyn_truthRconv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.weta1", &HGamPhotonsAuxDyn_weta1, &b_HGamPhotonsAuxDyn_weta1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.weta2", &HGamPhotonsAuxDyn_weta2, &b_HGamPhotonsAuxDyn_weta2);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.wtots1", &HGamPhotonsAuxDyn_wtots1, &b_HGamPhotonsAuxDyn_wtots1);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.zconv", &HGamPhotonsAuxDyn_zconv, &b_HGamPhotonsAuxDyn_zconv);
   fChain->SetBranchAddress("HGamPhotonsAuxDyn.zvertex", &HGamPhotonsAuxDyn_zvertex, &b_HGamPhotonsAuxDyn_zvertex);
   fChain->SetBranchAddress("HGamElectrons", &HGamElectrons, &b_HGamElectrons);
   fChain->SetBranchAddress("HGamElectronsAux.", &HGamElectronsAux_, &b_HGamElectronsAux_);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4PFlowCustomVtxHgg", &BTagging_HGamAntiKt4PFlowCustomVtxHgg, &b_BTagging_HGamAntiKt4PFlowCustomVtxHgg);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4PFlowCustomVtxHggAux.", &BTagging_HGamAntiKt4PFlowCustomVtxHggAux_, &b_BTagging_HGamAntiKt4PFlowCustomVtxHggAux_);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJets", &HGamAntiKt4PFlowCustomVtxHggJets, &b_HGamAntiKt4PFlowCustomVtxHggJets);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAux.", &HGamAntiKt4PFlowCustomVtxHggJetsAux_, &b_HGamAntiKt4PFlowCustomVtxHggJetsAux_);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlow_BTagging201903", &BTagging_HGamAntiKt4EMPFlow_BTagging201903, &b_BTagging_HGamAntiKt4EMPFlow_BTagging201903);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlow_BTagging201903Aux.", &BTagging_HGamAntiKt4EMPFlow_BTagging201903Aux_, &b_BTagging_HGamAntiKt4EMPFlow_BTagging201903Aux_);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903", &HGamAntiKt4EMPFlowJets_BTagging201903, &b_HGamAntiKt4EMPFlowJets_BTagging201903);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903Aux.", &HGamAntiKt4EMPFlowJets_BTagging201903Aux_, &b_HGamAntiKt4EMPFlowJets_BTagging201903Aux_);
   fChain->SetBranchAddress("HGamMuons", &HGamMuons, &b_HGamMuons);
   fChain->SetBranchAddress("HGamMuonsAux.", &HGamMuonsAux_, &b_HGamMuonsAux_);
   fChain->SetBranchAddress("HGamTauJets", &HGamTauJets, &b_HGamTauJets);
   fChain->SetBranchAddress("HGamTauJetsAux.", &HGamTauJetsAux_, &b_HGamTauJetsAux_);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlow", &HGamMET_Reference_AntiKt4EMPFlow, &b_HGamMET_Reference_AntiKt4EMPFlow);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAux.", &HGamMET_Reference_AntiKt4EMPFlowAux_, &b_HGamMET_Reference_AntiKt4EMPFlowAux_);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.mpx", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpx);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.mpy", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_mpy);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.name", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_name);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.source", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_source);
   fChain->SetBranchAddress("HGamMET_Reference_AntiKt4EMPFlowAuxDyn.sumet", &HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet, &b_HGamMET_Reference_AntiKt4EMPFlowAuxDyn_sumet);
   fChain->SetBranchAddress("HGamEventInfo", &HGamEventInfo, &b_HGamEventInfo);
   fChain->SetBranchAddress("HGamEventInfoAux.", &HGamEventInfoAux_, &b_HGamEventInfoAux_);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.DR_y_y", &HGamEventInfoAuxDyn_DR_y_y, &b_HGamEventInfoAuxDyn_DR_y_y);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.DRmin_y_j", &HGamEventInfoAuxDyn_DRmin_y_j, &b_HGamEventInfoAuxDyn_DRmin_y_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.DRmin_y_j_2", &HGamEventInfoAuxDyn_DRmin_y_j_2, &b_HGamEventInfoAuxDyn_DRmin_y_j_2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Deta_j_j", &HGamEventInfoAuxDyn_Deta_j_j, &b_HGamEventInfoAuxDyn_Deta_j_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j", &HGamEventInfoAuxDyn_Dphi_j_j, &b_HGamEventInfoAuxDyn_Dphi_j_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j_30", &HGamEventInfoAuxDyn_Dphi_j_j_30, &b_HGamEventInfoAuxDyn_Dphi_j_j_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j_30_signed", &HGamEventInfoAuxDyn_Dphi_j_j_30_signed, &b_HGamEventInfoAuxDyn_Dphi_j_j_30_signed);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j_50", &HGamEventInfoAuxDyn_Dphi_j_j_50, &b_HGamEventInfoAuxDyn_Dphi_j_j_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_j_j_50_signed", &HGamEventInfoAuxDyn_Dphi_j_j_50_signed, &b_HGamEventInfoAuxDyn_Dphi_j_j_50_signed);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_y_y", &HGamEventInfoAuxDyn_Dphi_y_y, &b_HGamEventInfoAuxDyn_Dphi_y_y);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_yy_jj", &HGamEventInfoAuxDyn_Dphi_yy_jj, &b_HGamEventInfoAuxDyn_Dphi_yy_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_yy_jj_30", &HGamEventInfoAuxDyn_Dphi_yy_jj_30, &b_HGamEventInfoAuxDyn_Dphi_yy_jj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dphi_yy_jj_50", &HGamEventInfoAuxDyn_Dphi_yy_jj_50, &b_HGamEventInfoAuxDyn_Dphi_yy_jj_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_j_j", &HGamEventInfoAuxDyn_Dy_j_j, &b_HGamEventInfoAuxDyn_Dy_j_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_j_j_30", &HGamEventInfoAuxDyn_Dy_j_j_30, &b_HGamEventInfoAuxDyn_Dy_j_j_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_y_y", &HGamEventInfoAuxDyn_Dy_y_y, &b_HGamEventInfoAuxDyn_Dy_y_y);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_yy_jj", &HGamEventInfoAuxDyn_Dy_yy_jj, &b_HGamEventInfoAuxDyn_Dy_yy_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Dy_yy_jj_30", &HGamEventInfoAuxDyn_Dy_yy_jj_30, &b_HGamEventInfoAuxDyn_Dy_yy_jj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.E_y1", &HGamEventInfoAuxDyn_E_y1, &b_HGamEventInfoAuxDyn_E_y1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.E_y2", &HGamEventInfoAuxDyn_E_y2, &b_HGamEventInfoAuxDyn_E_y2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HT_30", &HGamEventInfoAuxDyn_HT_30, &b_HGamEventInfoAuxDyn_HT_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HTall_30", &HGamEventInfoAuxDyn_HTall_30, &b_HGamEventInfoAuxDyn_HTall_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_cutFlowAllJet", &HGamEventInfoAuxDyn_Hc_cutFlowAllJet, &b_HGamEventInfoAuxDyn_Hc_cutFlowAllJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_cutFlowLeadJet", &HGamEventInfoAuxDyn_Hc_cutFlowLeadJet, &b_HGamEventInfoAuxDyn_Hc_cutFlowLeadJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_jet_truthlabel", &HGamEventInfoAuxDyn_Hc_jet_truthlabel, &b_HGamEventInfoAuxDyn_Hc_jet_truthlabel);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_leadJet_truthLabel", &HGamEventInfoAuxDyn_Hc_leadJet_truthLabel, &b_HGamEventInfoAuxDyn_Hc_leadJet_truthLabel);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_m_yy_cat", &HGamEventInfoAuxDyn_Hc_m_yy_cat, &b_HGamEventInfoAuxDyn_Hc_m_yy_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_y1_j_dr", &HGamEventInfoAuxDyn_Hc_y1_j_dr, &b_HGamEventInfoAuxDyn_Hc_y1_j_dr);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_y2_j_dr", &HGamEventInfoAuxDyn_Hc_y2_j_dr, &b_HGamEventInfoAuxDyn_Hc_y2_j_dr);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HggPrimaryVerticesScore", &HGamEventInfoAuxDyn_HggPrimaryVerticesScore, &b_HGamEventInfoAuxDyn_HggPrimaryVerticesScore);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HggPrimaryVerticesZ", &HGamEventInfoAuxDyn_HggPrimaryVerticesZ, &b_HGamEventInfoAuxDyn_HggPrimaryVerticesZ);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_cutFlow", &HGamEventInfoAuxDyn_HiggsHF_cutFlow, &b_HGamEventInfoAuxDyn_HiggsHF_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.NLoosePhotons", &HGamEventInfoAuxDyn_NLoosePhotons, &b_HGamEventInfoAuxDyn_NLoosePhotons);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_e", &HGamEventInfoAuxDyn_N_e, &b_HGamEventInfoAuxDyn_N_e);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j", &HGamEventInfoAuxDyn_N_j, &b_HGamEventInfoAuxDyn_N_j);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_30", &HGamEventInfoAuxDyn_N_j_30, &b_HGamEventInfoAuxDyn_N_j_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_50", &HGamEventInfoAuxDyn_N_j_50, &b_HGamEventInfoAuxDyn_N_j_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_btag", &HGamEventInfoAuxDyn_N_j_btag, &b_HGamEventInfoAuxDyn_N_j_btag);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_btag30", &HGamEventInfoAuxDyn_N_j_btag30, &b_HGamEventInfoAuxDyn_N_j_btag30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_central", &HGamEventInfoAuxDyn_N_j_central, &b_HGamEventInfoAuxDyn_N_j_central);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_j_central30", &HGamEventInfoAuxDyn_N_j_central30, &b_HGamEventInfoAuxDyn_N_j_central30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_lep", &HGamEventInfoAuxDyn_N_lep, &b_HGamEventInfoAuxDyn_N_lep);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_lep_15", &HGamEventInfoAuxDyn_N_lep_15, &b_HGamEventInfoAuxDyn_N_lep_15);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.N_mu", &HGamEventInfoAuxDyn_N_mu, &b_HGamEventInfoAuxDyn_N_mu);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Zepp", &HGamEventInfoAuxDyn_Zepp, &b_HGamEventInfoAuxDyn_Zepp);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_GlobalICHEP", &HGamEventInfoAuxDyn_catCoup_GlobalICHEP, &b_HGamEventInfoAuxDyn_catCoup_GlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_HybridICHEP", &HGamEventInfoAuxDyn_catCoup_HybridICHEP, &b_HGamEventInfoAuxDyn_catCoup_HybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_MonoH_2var", &HGamEventInfoAuxDyn_catCoup_MonoH_2var, &b_HGamEventInfoAuxDyn_catCoup_MonoH_2var);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_Moriond2017BDT", &HGamEventInfoAuxDyn_catCoup_Moriond2017BDT, &b_HGamEventInfoAuxDyn_catCoup_Moriond2017BDT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_XGBoost_ttH", &HGamEventInfoAuxDyn_catCoup_XGBoost_ttH, &b_HGamEventInfoAuxDyn_catCoup_XGBoost_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catCoup_XGBoost_ttHCP", &HGamEventInfoAuxDyn_catCoup_XGBoost_ttHCP, &b_HGamEventInfoAuxDyn_catCoup_XGBoost_ttHCP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catLowHighMyy_conv", &HGamEventInfoAuxDyn_catLowHighMyy_conv, &b_HGamEventInfoAuxDyn_catLowHighMyy_conv);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_Run1", &HGamEventInfoAuxDyn_catMass_Run1, &b_HGamEventInfoAuxDyn_catMass_Run1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_conv", &HGamEventInfoAuxDyn_catMass_conv, &b_HGamEventInfoAuxDyn_catMass_conv);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_eta", &HGamEventInfoAuxDyn_catMass_eta, &b_HGamEventInfoAuxDyn_catMass_eta);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_mu", &HGamEventInfoAuxDyn_catMass_mu, &b_HGamEventInfoAuxDyn_catMass_mu);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catMass_pT", &HGamEventInfoAuxDyn_catMass_pT, &b_HGamEventInfoAuxDyn_catMass_pT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catXS_HiggsHF", &HGamEventInfoAuxDyn_catXS_HiggsHF, &b_HGamEventInfoAuxDyn_catXS_HiggsHF);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catXS_VBF", &HGamEventInfoAuxDyn_catXS_VBF, &b_HGamEventInfoAuxDyn_catXS_VBF);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catXS_nbjet", &HGamEventInfoAuxDyn_catXS_nbjet, &b_HGamEventInfoAuxDyn_catXS_nbjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.catXS_ttH", &HGamEventInfoAuxDyn_catXS_ttH, &b_HGamEventInfoAuxDyn_catXS_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.cosTS_yy", &HGamEventInfoAuxDyn_cosTS_yy, &b_HGamEventInfoAuxDyn_cosTS_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.cosTS_yyjj", &HGamEventInfoAuxDyn_cosTS_yyjj, &b_HGamEventInfoAuxDyn_cosTS_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.cutFlow", &HGamEventInfoAuxDyn_cutFlow, &b_HGamEventInfoAuxDyn_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.eta_hybridtop2", &HGamEventInfoAuxDyn_eta_hybridtop2, &b_HGamEventInfoAuxDyn_eta_hybridtop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.eta_recotop1", &HGamEventInfoAuxDyn_eta_recotop1, &b_HGamEventInfoAuxDyn_eta_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_Score", &HGamEventInfoAuxDyn_fcnc_Score, &b_HGamEventInfoAuxDyn_fcnc_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_cat", &HGamEventInfoAuxDyn_fcnc_cat, &b_HGamEventInfoAuxDyn_fcnc_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_cutFlow", &HGamEventInfoAuxDyn_fcnc_cutFlow, &b_HGamEventInfoAuxDyn_fcnc_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_etm", &HGamEventInfoAuxDyn_fcnc_etm, &b_HGamEventInfoAuxDyn_fcnc_etm);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_icat", &HGamEventInfoAuxDyn_fcnc_icat, &b_HGamEventInfoAuxDyn_fcnc_icat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_idLep", &HGamEventInfoAuxDyn_fcnc_idLep, &b_HGamEventInfoAuxDyn_fcnc_idLep);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_jetInd", &HGamEventInfoAuxDyn_fcnc_jetInd, &b_HGamEventInfoAuxDyn_fcnc_jetInd);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_mT", &HGamEventInfoAuxDyn_fcnc_mT, &b_HGamEventInfoAuxDyn_fcnc_mT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_mTop1", &HGamEventInfoAuxDyn_fcnc_mTop1, &b_HGamEventInfoAuxDyn_fcnc_mTop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_mTop2", &HGamEventInfoAuxDyn_fcnc_mTop2, &b_HGamEventInfoAuxDyn_fcnc_mTop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_nbjet", &HGamEventInfoAuxDyn_fcnc_nbjet, &b_HGamEventInfoAuxDyn_fcnc_nbjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_ncbTop12", &HGamEventInfoAuxDyn_fcnc_ncbTop12, &b_HGamEventInfoAuxDyn_fcnc_ncbTop12);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_njet", &HGamEventInfoAuxDyn_fcnc_njet, &b_HGamEventInfoAuxDyn_fcnc_njet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_njet30", &HGamEventInfoAuxDyn_fcnc_njet30, &b_HGamEventInfoAuxDyn_fcnc_njet30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_ntopc", &HGamEventInfoAuxDyn_fcnc_ntopc, &b_HGamEventInfoAuxDyn_fcnc_ntopc);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_phim", &HGamEventInfoAuxDyn_fcnc_phim, &b_HGamEventInfoAuxDyn_fcnc_phim);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.hardestVertexPhi", &HGamEventInfoAuxDyn_hardestVertexPhi, &b_HGamEventInfoAuxDyn_hardestVertexPhi);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.hardestVertexSumPt2", &HGamEventInfoAuxDyn_hardestVertexSumPt2, &b_HGamEventInfoAuxDyn_hardestVertexSumPt2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.hardestVertexZ", &HGamEventInfoAuxDyn_hardestVertexZ, &b_HGamEventInfoAuxDyn_hardestVertexZ);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.idx_jets_recotop1", &HGamEventInfoAuxDyn_idx_jets_recotop1, &b_HGamEventInfoAuxDyn_idx_jets_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.idx_jets_recotop2", &HGamEventInfoAuxDyn_idx_jets_recotop2, &b_HGamEventInfoAuxDyn_idx_jets_recotop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassed", &HGamEventInfoAuxDyn_isPassed, &b_HGamEventInfoAuxDyn_isPassed);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedBasic", &HGamEventInfoAuxDyn_isPassedBasic, &b_HGamEventInfoAuxDyn_isPassedBasic);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedHighMyy", &HGamEventInfoAuxDyn_isPassedHighMyy, &b_HGamEventInfoAuxDyn_isPassedHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedIsolation", &HGamEventInfoAuxDyn_isPassedIsolation, &b_HGamEventInfoAuxDyn_isPassedIsolation);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedIsolationHighMyy", &HGamEventInfoAuxDyn_isPassedIsolationHighMyy, &b_HGamEventInfoAuxDyn_isPassedIsolationHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedIsolationLowMyy", &HGamEventInfoAuxDyn_isPassedIsolationLowMyy, &b_HGamEventInfoAuxDyn_isPassedIsolationLowMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedJetEventClean", &HGamEventInfoAuxDyn_isPassedJetEventClean, &b_HGamEventInfoAuxDyn_isPassedJetEventClean);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedLowMyy", &HGamEventInfoAuxDyn_isPassedLowMyy, &b_HGamEventInfoAuxDyn_isPassedLowMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedMassCut", &HGamEventInfoAuxDyn_isPassedMassCut, &b_HGamEventInfoAuxDyn_isPassedMassCut);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedPID", &HGamEventInfoAuxDyn_isPassedPID, &b_HGamEventInfoAuxDyn_isPassedPID);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedPreselection", &HGamEventInfoAuxDyn_isPassedPreselection, &b_HGamEventInfoAuxDyn_isPassedPreselection);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedPtCutsLowHighMyy", &HGamEventInfoAuxDyn_isPassedPtCutsLowHighMyy, &b_HGamEventInfoAuxDyn_isPassedPtCutsLowHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedRelPtCuts", &HGamEventInfoAuxDyn_isPassedRelPtCuts, &b_HGamEventInfoAuxDyn_isPassedRelPtCuts);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedTrigMatchLowHighMyy", &HGamEventInfoAuxDyn_isPassedTrigMatchLowHighMyy, &b_HGamEventInfoAuxDyn_isPassedTrigMatchLowHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassedTriggerMatch", &HGamEventInfoAuxDyn_isPassedTriggerMatch, &b_HGamEventInfoAuxDyn_isPassedTriggerMatch);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_alljet", &HGamEventInfoAuxDyn_m_alljet, &b_HGamEventInfoAuxDyn_m_alljet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_alljet_30", &HGamEventInfoAuxDyn_m_alljet_30, &b_HGamEventInfoAuxDyn_m_alljet_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_ee", &HGamEventInfoAuxDyn_m_ee, &b_HGamEventInfoAuxDyn_m_ee);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_hybridtop2", &HGamEventInfoAuxDyn_m_hybridtop2, &b_HGamEventInfoAuxDyn_m_hybridtop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_jj", &HGamEventInfoAuxDyn_m_jj, &b_HGamEventInfoAuxDyn_m_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_jj_30", &HGamEventInfoAuxDyn_m_jj_30, &b_HGamEventInfoAuxDyn_m_jj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_jj_50", &HGamEventInfoAuxDyn_m_jj_50, &b_HGamEventInfoAuxDyn_m_jj_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_mumu", &HGamEventInfoAuxDyn_m_mumu, &b_HGamEventInfoAuxDyn_m_mumu);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_recotop1", &HGamEventInfoAuxDyn_m_recotop1, &b_HGamEventInfoAuxDyn_m_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy", &HGamEventInfoAuxDyn_m_yy, &b_HGamEventInfoAuxDyn_m_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy_hardestVertex", &HGamEventInfoAuxDyn_m_yy_hardestVertex, &b_HGamEventInfoAuxDyn_m_yy_hardestVertex);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy_resolution", &HGamEventInfoAuxDyn_m_yy_resolution, &b_HGamEventInfoAuxDyn_m_yy_resolution);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy_zCommon", &HGamEventInfoAuxDyn_m_yy_zCommon, &b_HGamEventInfoAuxDyn_m_yy_zCommon);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yyj", &HGamEventInfoAuxDyn_m_yyj, &b_HGamEventInfoAuxDyn_m_yyj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yyj_30", &HGamEventInfoAuxDyn_m_yyj_30, &b_HGamEventInfoAuxDyn_m_yyj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yyjj", &HGamEventInfoAuxDyn_m_yyjj, &b_HGamEventInfoAuxDyn_m_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.massTrans", &HGamEventInfoAuxDyn_massTrans, &b_HGamEventInfoAuxDyn_massTrans);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.maxTau_yyj_30", &HGamEventInfoAuxDyn_maxTau_yyj_30, &b_HGamEventInfoAuxDyn_maxTau_yyj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.met_TST", &HGamEventInfoAuxDyn_met_TST, &b_HGamEventInfoAuxDyn_met_TST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.met_cat", &HGamEventInfoAuxDyn_met_cat, &b_HGamEventInfoAuxDyn_met_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.met_hardVertexTST", &HGamEventInfoAuxDyn_met_hardVertexTST, &b_HGamEventInfoAuxDyn_met_hardVertexTST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.met_weight", &HGamEventInfoAuxDyn_met_weight, &b_HGamEventInfoAuxDyn_met_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.mu", &HGamEventInfoAuxDyn_mu, &b_HGamEventInfoAuxDyn_mu);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.multiClassCatCoup_GlobalICHEP", &HGamEventInfoAuxDyn_multiClassCatCoup_GlobalICHEP, &b_HGamEventInfoAuxDyn_multiClassCatCoup_GlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.multiClassCatCoup_HybridICHEP", &HGamEventInfoAuxDyn_multiClassCatCoup_HybridICHEP, &b_HGamEventInfoAuxDyn_multiClassCatCoup_HybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.numberOfPrimaryVertices", &HGamEventInfoAuxDyn_numberOfPrimaryVertices, &b_HGamEventInfoAuxDyn_numberOfPrimaryVertices);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_hard", &HGamEventInfoAuxDyn_pT_hard, &b_HGamEventInfoAuxDyn_pT_hard);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_hybridtop2", &HGamEventInfoAuxDyn_pT_hybridtop2, &b_HGamEventInfoAuxDyn_pT_hybridtop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j1", &HGamEventInfoAuxDyn_pT_j1, &b_HGamEventInfoAuxDyn_pT_j1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j1_30", &HGamEventInfoAuxDyn_pT_j1_30, &b_HGamEventInfoAuxDyn_pT_j1_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j2", &HGamEventInfoAuxDyn_pT_j2, &b_HGamEventInfoAuxDyn_pT_j2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j2_30", &HGamEventInfoAuxDyn_pT_j2_30, &b_HGamEventInfoAuxDyn_pT_j2_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_j3_30", &HGamEventInfoAuxDyn_pT_j3_30, &b_HGamEventInfoAuxDyn_pT_j3_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_jj", &HGamEventInfoAuxDyn_pT_jj, &b_HGamEventInfoAuxDyn_pT_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_recotop1", &HGamEventInfoAuxDyn_pT_recotop1, &b_HGamEventInfoAuxDyn_pT_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_y1", &HGamEventInfoAuxDyn_pT_y1, &b_HGamEventInfoAuxDyn_pT_y1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_y2", &HGamEventInfoAuxDyn_pT_y2, &b_HGamEventInfoAuxDyn_pT_y2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yy", &HGamEventInfoAuxDyn_pT_yy, &b_HGamEventInfoAuxDyn_pT_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyj", &HGamEventInfoAuxDyn_pT_yyj, &b_HGamEventInfoAuxDyn_pT_yyj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyj_30", &HGamEventInfoAuxDyn_pT_yyj_30, &b_HGamEventInfoAuxDyn_pT_yyj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyjj", &HGamEventInfoAuxDyn_pT_yyjj, &b_HGamEventInfoAuxDyn_pT_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyjj_30", &HGamEventInfoAuxDyn_pT_yyjj_30, &b_HGamEventInfoAuxDyn_pT_yyjj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pT_yyjj_50", &HGamEventInfoAuxDyn_pT_yyjj_50, &b_HGamEventInfoAuxDyn_pT_yyjj_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pTlepMET", &HGamEventInfoAuxDyn_pTlepMET, &b_HGamEventInfoAuxDyn_pTlepMET);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pTt_yy", &HGamEventInfoAuxDyn_pTt_yy, &b_HGamEventInfoAuxDyn_pTt_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.passCrackVetoCleaning", &HGamEventInfoAuxDyn_passCrackVetoCleaning, &b_HGamEventInfoAuxDyn_passCrackVetoCleaning);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.passMeyCut", &HGamEventInfoAuxDyn_passMeyCut, &b_HGamEventInfoAuxDyn_passMeyCut);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phiStar_yy", &HGamEventInfoAuxDyn_phiStar_yy, &b_HGamEventInfoAuxDyn_phiStar_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phi_TST", &HGamEventInfoAuxDyn_phi_TST, &b_HGamEventInfoAuxDyn_phi_TST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phi_hardVertexTST", &HGamEventInfoAuxDyn_phi_hardVertexTST, &b_HGamEventInfoAuxDyn_phi_hardVertexTST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phi_hybridtop2", &HGamEventInfoAuxDyn_phi_hybridtop2, &b_HGamEventInfoAuxDyn_phi_hybridtop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.phi_recotop1", &HGamEventInfoAuxDyn_phi_recotop1, &b_HGamEventInfoAuxDyn_phi_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pileupVertexPhi", &HGamEventInfoAuxDyn_pileupVertexPhi, &b_HGamEventInfoAuxDyn_pileupVertexPhi);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pileupVertexSumPt2", &HGamEventInfoAuxDyn_pileupVertexSumPt2, &b_HGamEventInfoAuxDyn_pileupVertexSumPt2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pileupVertexZ", &HGamEventInfoAuxDyn_pileupVertexZ, &b_HGamEventInfoAuxDyn_pileupVertexZ);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.pt_llmax", &HGamEventInfoAuxDyn_pt_llmax, &b_HGamEventInfoAuxDyn_pt_llmax);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.scoreBinaryCatCoup_GlobalICHEP", &HGamEventInfoAuxDyn_scoreBinaryCatCoup_GlobalICHEP, &b_HGamEventInfoAuxDyn_scoreBinaryCatCoup_GlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.scoreBinaryCatCoup_HybridICHEP", &HGamEventInfoAuxDyn_scoreBinaryCatCoup_HybridICHEP, &b_HGamEventInfoAuxDyn_scoreBinaryCatCoup_HybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_MonoH_2var", &HGamEventInfoAuxDyn_score_MonoH_2var, &b_HGamEventInfoAuxDyn_score_MonoH_2var);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_WH_ICHEP2020", &HGamEventInfoAuxDyn_score_WH_ICHEP2020, &b_HGamEventInfoAuxDyn_score_WH_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_ZH_ICHEP2020", &HGamEventInfoAuxDyn_score_ZH_ICHEP2020, &b_HGamEventInfoAuxDyn_score_ZH_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_recotop1", &HGamEventInfoAuxDyn_score_recotop1, &b_HGamEventInfoAuxDyn_score_recotop1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_recotop2", &HGamEventInfoAuxDyn_score_recotop2, &b_HGamEventInfoAuxDyn_score_recotop2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_tHjb_ICHEP2020", &HGamEventInfoAuxDyn_score_tHjb_ICHEP2020, &b_HGamEventInfoAuxDyn_score_tHjb_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_tWH_ICHEP2020", &HGamEventInfoAuxDyn_score_tWH_ICHEP2020, &b_HGamEventInfoAuxDyn_score_tWH_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_ttH", &HGamEventInfoAuxDyn_score_ttH, &b_HGamEventInfoAuxDyn_score_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_ttHCP", &HGamEventInfoAuxDyn_score_ttHCP, &b_HGamEventInfoAuxDyn_score_ttHCP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.score_ttH_ICHEP2020", &HGamEventInfoAuxDyn_score_ttH_ICHEP2020, &b_HGamEventInfoAuxDyn_score_ttH_ICHEP2020);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.selectedVertexPhi", &HGamEventInfoAuxDyn_selectedVertexPhi, &b_HGamEventInfoAuxDyn_selectedVertexPhi);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.selectedVertexSumPt2", &HGamEventInfoAuxDyn_selectedVertexSumPt2, &b_HGamEventInfoAuxDyn_selectedVertexSumPt2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.selectedVertexZ", &HGamEventInfoAuxDyn_selectedVertexZ, &b_HGamEventInfoAuxDyn_selectedVertexZ);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.sumTau_yyj_30", &HGamEventInfoAuxDyn_sumTau_yyj_30, &b_HGamEventInfoAuxDyn_sumTau_yyj_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.sumet_TST", &HGamEventInfoAuxDyn_sumet_TST, &b_HGamEventInfoAuxDyn_sumet_TST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.sumet_hardVertexTST", &HGamEventInfoAuxDyn_sumet_hardVertexTST, &b_HGamEventInfoAuxDyn_sumet_hardVertexTST);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weight", &HGamEventInfoAuxDyn_weight, &b_HGamEventInfoAuxDyn_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_GlobalICHEP", &HGamEventInfoAuxDyn_weightCatCoup_GlobalICHEP, &b_HGamEventInfoAuxDyn_weightCatCoup_GlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_HybridICHEP", &HGamEventInfoAuxDyn_weightCatCoup_HybridICHEP, &b_HGamEventInfoAuxDyn_weightCatCoup_HybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_MonoH_2var", &HGamEventInfoAuxDyn_weightCatCoup_MonoH_2var, &b_HGamEventInfoAuxDyn_weightCatCoup_MonoH_2var);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_Moriond2017BDT", &HGamEventInfoAuxDyn_weightCatCoup_Moriond2017BDT, &b_HGamEventInfoAuxDyn_weightCatCoup_Moriond2017BDT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFGlobalICHEP", &HGamEventInfoAuxDyn_weightCatCoup_SFGlobalICHEP, &b_HGamEventInfoAuxDyn_weightCatCoup_SFGlobalICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFHybridICHEP", &HGamEventInfoAuxDyn_weightCatCoup_SFHybridICHEP, &b_HGamEventInfoAuxDyn_weightCatCoup_SFHybridICHEP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFMonoH_2var", &HGamEventInfoAuxDyn_weightCatCoup_SFMonoH_2var, &b_HGamEventInfoAuxDyn_weightCatCoup_SFMonoH_2var);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFMoriond2017BDT", &HGamEventInfoAuxDyn_weightCatCoup_SFMoriond2017BDT, &b_HGamEventInfoAuxDyn_weightCatCoup_SFMoriond2017BDT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFXGBoost_ttH", &HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttH, &b_HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_SFXGBoost_ttHCP", &HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttHCP, &b_HGamEventInfoAuxDyn_weightCatCoup_SFXGBoost_ttHCP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_XGBoost_ttH", &HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttH, &b_HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatCoup_XGBoost_ttHCP", &HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttHCP, &b_HGamEventInfoAuxDyn_weightCatCoup_XGBoost_ttHCP);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatXS_HiggsHF", &HGamEventInfoAuxDyn_weightCatXS_HiggsHF, &b_HGamEventInfoAuxDyn_weightCatXS_HiggsHF);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatXS_nbjet", &HGamEventInfoAuxDyn_weightCatXS_nbjet, &b_HGamEventInfoAuxDyn_weightCatXS_nbjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightCatXS_ttH", &HGamEventInfoAuxDyn_weightCatXS_ttH, &b_HGamEventInfoAuxDyn_weightCatXS_ttH);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightFJvt", &HGamEventInfoAuxDyn_weightFJvt, &b_HGamEventInfoAuxDyn_weightFJvt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightFJvt_30", &HGamEventInfoAuxDyn_weightFJvt_30, &b_HGamEventInfoAuxDyn_weightFJvt_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightInitial", &HGamEventInfoAuxDyn_weightInitial, &b_HGamEventInfoAuxDyn_weightInitial);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightJvt", &HGamEventInfoAuxDyn_weightJvt, &b_HGamEventInfoAuxDyn_weightJvt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightJvt_30", &HGamEventInfoAuxDyn_weightJvt_30, &b_HGamEventInfoAuxDyn_weightJvt_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightJvt_50", &HGamEventInfoAuxDyn_weightJvt_50, &b_HGamEventInfoAuxDyn_weightJvt_50);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightLowHighMyy", &HGamEventInfoAuxDyn_weightLowHighMyy, &b_HGamEventInfoAuxDyn_weightLowHighMyy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightN_lep", &HGamEventInfoAuxDyn_weightN_lep, &b_HGamEventInfoAuxDyn_weightN_lep);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightN_lep_15", &HGamEventInfoAuxDyn_weightN_lep_15, &b_HGamEventInfoAuxDyn_weightN_lep_15);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weightSF", &HGamEventInfoAuxDyn_weightSF, &b_HGamEventInfoAuxDyn_weightSF);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_j1", &HGamEventInfoAuxDyn_yAbs_j1, &b_HGamEventInfoAuxDyn_yAbs_j1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_j1_30", &HGamEventInfoAuxDyn_yAbs_j1_30, &b_HGamEventInfoAuxDyn_yAbs_j1_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_j2", &HGamEventInfoAuxDyn_yAbs_j2, &b_HGamEventInfoAuxDyn_yAbs_j2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_j2_30", &HGamEventInfoAuxDyn_yAbs_j2_30, &b_HGamEventInfoAuxDyn_yAbs_j2_30);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yAbs_yy", &HGamEventInfoAuxDyn_yAbs_yy, &b_HGamEventInfoAuxDyn_yAbs_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_cutFlow", &HGamEventInfoAuxDyn_yyb_cutFlow, &b_HGamEventInfoAuxDyn_yyb_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_m_jj", &HGamEventInfoAuxDyn_yybb_BCal_m_jj, &b_HGamEventInfoAuxDyn_yybb_BCal_m_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_m_yyjj", &HGamEventInfoAuxDyn_yybb_BCal_m_yyjj, &b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_m_yyjj_cnstrnd", &HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_cnstrnd, &b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_cnstrnd);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BCal_m_yyjj_tilde", &HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_tilde, &b_HGamEventInfoAuxDyn_yybb_BCal_m_yyjj_tilde);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BReg_m_jj", &HGamEventInfoAuxDyn_yybb_BReg_m_jj, &b_HGamEventInfoAuxDyn_yybb_BReg_m_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BReg_m_yyjj", &HGamEventInfoAuxDyn_yybb_BReg_m_yyjj, &b_HGamEventInfoAuxDyn_yybb_BReg_m_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BReg_m_yyjj_cnstrnd", &HGamEventInfoAuxDyn_yybb_BReg_m_yyjj_cnstrnd, &b_HGamEventInfoAuxDyn_yybb_BReg_m_yyjj_cnstrnd);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_BReg_m_yyjj_tilde", &HGamEventInfoAuxDyn_yybb_BReg_m_yyjj_tilde, &b_HGamEventInfoAuxDyn_yybb_BReg_m_yyjj_tilde);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_jj", &HGamEventInfoAuxDyn_yybb_KF_m_jj, &b_HGamEventInfoAuxDyn_yybb_KF_m_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_yyjj", &HGamEventInfoAuxDyn_yybb_KF_m_yyjj, &b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_yyjj_cnstrnd", &HGamEventInfoAuxDyn_yybb_KF_m_yyjj_cnstrnd, &b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj_cnstrnd);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_KF_m_yyjj_tilde", &HGamEventInfoAuxDyn_yybb_KF_m_yyjj_tilde, &b_HGamEventInfoAuxDyn_yybb_KF_m_yyjj_tilde);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_BCal_ttH_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_BCal_yy_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_yy_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_BCal_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_BReg_ttH_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_BReg_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_BReg_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_BReg_yy_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_BReg_yy_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_BReg_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_KF_ttH_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_KF_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_KF_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_KF_yy_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_KF_yy_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_KF_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_ttH_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_ttH_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_ttH_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_Res_BDT_yy_Score", &HGamEventInfoAuxDyn_yybb_Res_BDT_yy_Score, &b_HGamEventInfoAuxDyn_yybb_Res_BDT_yy_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_85_BCal_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_85_BCal_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_85_BCal_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_85_BReg_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_85_BReg_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_85_BReg_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_85_KF_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_85_KF_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_85_KF_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_85_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_85_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_85_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_BCal_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_BCal_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_BCal_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_BReg_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_BReg_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_BReg_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_KF_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_KF_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_KF_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_cutFlow", &HGamEventInfoAuxDyn_yybb_btag77_cutFlow, &b_HGamEventInfoAuxDyn_yybb_btag77_cutFlow);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_candidate_jet1", &HGamEventInfoAuxDyn_yybb_candidate_jet1, &b_HGamEventInfoAuxDyn_yybb_candidate_jet1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_candidate_jet2", &HGamEventInfoAuxDyn_yybb_candidate_jet2, &b_HGamEventInfoAuxDyn_yybb_candidate_jet2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_m_jj", &HGamEventInfoAuxDyn_yybb_m_jj, &b_HGamEventInfoAuxDyn_yybb_m_jj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_m_yyjj", &HGamEventInfoAuxDyn_yybb_m_yyjj, &b_HGamEventInfoAuxDyn_yybb_m_yyjj);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_m_yyjj_cnstrnd", &HGamEventInfoAuxDyn_yybb_m_yyjj_cnstrnd, &b_HGamEventInfoAuxDyn_yybb_m_yyjj_cnstrnd);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_m_yyjj_tilde", &HGamEventInfoAuxDyn_yybb_m_yyjj_tilde, &b_HGamEventInfoAuxDyn_yybb_m_yyjj_tilde);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BCal_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BCal_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BCal_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BCal_noHT_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noHT_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noHT_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BCal_noHT_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noHT_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noHT_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_noMbb_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BCal_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BCal_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BCal_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BReg_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BReg_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BReg_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BReg_noHT_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noHT_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noHT_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BReg_noHT_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noHT_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noHT_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_noMbb_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BReg_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_BReg_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_BReg_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_KF_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_KF_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_KF_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_KF_noHT_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noHT_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noHT_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_KF_noHT_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noHT_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noHT_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_KF_noMbb_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noMbb_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noMbb_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_KF_noMbb_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noMbb_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_noMbb_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_KF_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_KF_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_KF_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_noHT_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noHT_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noHT_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_noHT_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noHT_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noHT_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_noMbb_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noMbb_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noMbb_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_noMbb_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noMbb_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_noMbb_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_85_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_85_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_noHT_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noHT_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noHT_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_noHT_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noHT_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noHT_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_noMbb_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noMbb_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noMbb_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_noMbb_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noMbb_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_noMbb_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BCal_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BReg_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BReg_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BReg_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BReg_noHT_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noHT_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noHT_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BReg_noHT_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noHT_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noHT_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BReg_noMbb_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noMbb_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noMbb_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BReg_noMbb_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noMbb_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_noMbb_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BReg_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BReg_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_BReg_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_KF_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_KF_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_KF_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_KF_noHT_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noHT_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noHT_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_KF_noHT_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noHT_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noHT_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_KF_noMbb_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noMbb_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noMbb_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_KF_noMbb_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noMbb_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_noMbb_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_KF_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_KF_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_KF_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_noHT_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noHT_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noHT_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_noHT_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noHT_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noHT_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_noMbb_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noMbb_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noMbb_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_noMbb_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noMbb_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_noMbb_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_withTop_highMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_withTop_highMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_withTop_highMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_withTop_lowMass_Score", &HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_withTop_lowMass_Score, &b_HGamEventInfoAuxDyn_yybb_nonRes_XGBoost_btag77_withTop_lowMass_Score);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_btag77_85_BCal_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_BCal_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_BCal_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_btag77_85_BReg_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_BReg_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_BReg_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_btag77_85_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_btag77_85_KF_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_KF_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_85_KF_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_btag77_BCal_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_BCal_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_BCal_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_btag77_BReg_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_BReg_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_BReg_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_btag77_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_nonRes_cutBased_btag77_KF_Cat", &HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_KF_Cat, &b_HGamEventInfoAuxDyn_yybb_nonRes_cutBased_btag77_KF_Cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_vbf_jet1", &HGamEventInfoAuxDyn_yybb_vbf_jet1, &b_HGamEventInfoAuxDyn_yybb_vbf_jet1);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_vbf_jet2", &HGamEventInfoAuxDyn_yybb_vbf_jet2, &b_HGamEventInfoAuxDyn_yybb_vbf_jet2);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_weight", &HGamEventInfoAuxDyn_yybb_weight, &b_HGamEventInfoAuxDyn_yybb_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.zCommon", &HGamEventInfoAuxDyn_zCommon, &b_HGamEventInfoAuxDyn_zCommon);
   fChain->SetBranchAddress("EventInfo", &EventInfo, &b_EventInfo);
   fChain->SetBranchAddress("EventInfoAux.xAOD::AuxInfoBase", &EventInfoAux_xAOD__AuxInfoBase, &b_EventInfoAux_xAOD__AuxInfoBase);
   fChain->SetBranchAddress("EventInfoAux.runNumber", &EventInfoAux_runNumber, &b_EventInfoAux_runNumber);
   fChain->SetBranchAddress("EventInfoAux.eventNumber", &EventInfoAux_eventNumber, &b_EventInfoAux_eventNumber);
   fChain->SetBranchAddress("EventInfoAux.lumiBlock", &EventInfoAux_lumiBlock, &b_EventInfoAux_lumiBlock);
   fChain->SetBranchAddress("EventInfoAux.timeStamp", &EventInfoAux_timeStamp, &b_EventInfoAux_timeStamp);
   fChain->SetBranchAddress("EventInfoAux.timeStampNSOffset", &EventInfoAux_timeStampNSOffset, &b_EventInfoAux_timeStampNSOffset);
   fChain->SetBranchAddress("EventInfoAux.bcid", &EventInfoAux_bcid, &b_EventInfoAux_bcid);
   fChain->SetBranchAddress("EventInfoAux.detectorMask0", &EventInfoAux_detectorMask0, &b_EventInfoAux_detectorMask0);
   fChain->SetBranchAddress("EventInfoAux.detectorMask1", &EventInfoAux_detectorMask1, &b_EventInfoAux_detectorMask1);
   fChain->SetBranchAddress("EventInfoAux.detectorMask2", &EventInfoAux_detectorMask2, &b_EventInfoAux_detectorMask2);
   fChain->SetBranchAddress("EventInfoAux.detectorMask3", &EventInfoAux_detectorMask3, &b_EventInfoAux_detectorMask3);
   fChain->SetBranchAddress("EventInfoAux.detDescrTags", &EventInfoAux_detDescrTags, &b_EventInfoAux_detDescrTags);
   fChain->SetBranchAddress("EventInfoAux.eventTypeBitmask", &EventInfoAux_eventTypeBitmask, &b_EventInfoAux_eventTypeBitmask);
   fChain->SetBranchAddress("EventInfoAux.statusElement", &EventInfoAux_statusElement, &b_EventInfoAux_statusElement);
   fChain->SetBranchAddress("EventInfoAux.extendedLevel1ID", &EventInfoAux_extendedLevel1ID, &b_EventInfoAux_extendedLevel1ID);
   fChain->SetBranchAddress("EventInfoAux.level1TriggerType", &EventInfoAux_level1TriggerType, &b_EventInfoAux_level1TriggerType);
   fChain->SetBranchAddress("EventInfoAux.streamTagNames", &EventInfoAux_streamTagNames, &b_EventInfoAux_streamTagNames);
   fChain->SetBranchAddress("EventInfoAux.streamTagTypes", &EventInfoAux_streamTagTypes, &b_EventInfoAux_streamTagTypes);
   fChain->SetBranchAddress("EventInfoAux.streamTagObeysLumiblock", &EventInfoAux_streamTagObeysLumiblock, &b_EventInfoAux_streamTagObeysLumiblock);
   fChain->SetBranchAddress("EventInfoAux.actualInteractionsPerCrossing", &EventInfoAux_actualInteractionsPerCrossing, &b_EventInfoAux_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("EventInfoAux.averageInteractionsPerCrossing", &EventInfoAux_averageInteractionsPerCrossing, &b_EventInfoAux_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("EventInfoAux.pixelFlags", &EventInfoAux_pixelFlags, &b_EventInfoAux_pixelFlags);
   fChain->SetBranchAddress("EventInfoAux.sctFlags", &EventInfoAux_sctFlags, &b_EventInfoAux_sctFlags);
   fChain->SetBranchAddress("EventInfoAux.trtFlags", &EventInfoAux_trtFlags, &b_EventInfoAux_trtFlags);
   fChain->SetBranchAddress("EventInfoAux.larFlags", &EventInfoAux_larFlags, &b_EventInfoAux_larFlags);
   fChain->SetBranchAddress("EventInfoAux.tileFlags", &EventInfoAux_tileFlags, &b_EventInfoAux_tileFlags);
   fChain->SetBranchAddress("EventInfoAux.muonFlags", &EventInfoAux_muonFlags, &b_EventInfoAux_muonFlags);
   fChain->SetBranchAddress("EventInfoAux.forwardDetFlags", &EventInfoAux_forwardDetFlags, &b_EventInfoAux_forwardDetFlags);
   fChain->SetBranchAddress("EventInfoAux.coreFlags", &EventInfoAux_coreFlags, &b_EventInfoAux_coreFlags);
   fChain->SetBranchAddress("EventInfoAux.backgroundFlags", &EventInfoAux_backgroundFlags, &b_EventInfoAux_backgroundFlags);
   fChain->SetBranchAddress("EventInfoAux.lumiFlags", &EventInfoAux_lumiFlags, &b_EventInfoAux_lumiFlags);
   fChain->SetBranchAddress("EventInfoAux.beamPosX", &EventInfoAux_beamPosX, &b_EventInfoAux_beamPosX);
   fChain->SetBranchAddress("EventInfoAux.beamPosY", &EventInfoAux_beamPosY, &b_EventInfoAux_beamPosY);
   fChain->SetBranchAddress("EventInfoAux.beamPosZ", &EventInfoAux_beamPosZ, &b_EventInfoAux_beamPosZ);
   fChain->SetBranchAddress("EventInfoAux.beamPosSigmaX", &EventInfoAux_beamPosSigmaX, &b_EventInfoAux_beamPosSigmaX);
   fChain->SetBranchAddress("EventInfoAux.beamPosSigmaY", &EventInfoAux_beamPosSigmaY, &b_EventInfoAux_beamPosSigmaY);
   fChain->SetBranchAddress("EventInfoAux.beamPosSigmaZ", &EventInfoAux_beamPosSigmaZ, &b_EventInfoAux_beamPosSigmaZ);
   fChain->SetBranchAddress("EventInfoAux.beamPosSigmaXY", &EventInfoAux_beamPosSigmaXY, &b_EventInfoAux_beamPosSigmaXY);
   fChain->SetBranchAddress("EventInfoAux.beamTiltXZ", &EventInfoAux_beamTiltXZ, &b_EventInfoAux_beamTiltXZ);
   fChain->SetBranchAddress("EventInfoAux.beamTiltYZ", &EventInfoAux_beamTiltYZ, &b_EventInfoAux_beamTiltYZ);
   fChain->SetBranchAddress("EventInfoAux.beamStatus", &EventInfoAux_beamStatus, &b_EventInfoAux_beamStatus);
   fChain->SetBranchAddress("EventInfoAuxDyn.DFCommonJets_eventClean_LooseBad", &EventInfoAuxDyn_DFCommonJets_eventClean_LooseBad, &b_EventInfoAuxDyn_DFCommonJets_eventClean_LooseBad);
   fChain->SetBranchAddress("EventInfoAuxDyn.DFCommonJets_isBadBatman", &EventInfoAuxDyn_DFCommonJets_isBadBatman, &b_EventInfoAuxDyn_DFCommonJets_isBadBatman);
   fChain->SetBranchAddress("EventInfoAuxDyn.centralEventShapeDensity", &EventInfoAuxDyn_centralEventShapeDensity, &b_EventInfoAuxDyn_centralEventShapeDensity);
   fChain->SetBranchAddress("EventInfoAuxDyn.forwardEventShapeDensity", &EventInfoAuxDyn_forwardEventShapeDensity, &b_EventInfoAuxDyn_forwardEventShapeDensity);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_2g20_loose", &EventInfoAuxDyn_passTrig_HLT_2g20_loose, &b_EventInfoAuxDyn_passTrig_HLT_2g20_loose);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_2g20_tight", &EventInfoAuxDyn_passTrig_HLT_2g20_tight, &b_EventInfoAuxDyn_passTrig_HLT_2g20_tight);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_2g20_tight_icalotight_L12EM15VHI", &EventInfoAuxDyn_passTrig_HLT_2g20_tight_icalotight_L12EM15VHI, &b_EventInfoAuxDyn_passTrig_HLT_2g20_tight_icalotight_L12EM15VHI);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_2g20_tight_icalovloose_L12EM15VHI", &EventInfoAuxDyn_passTrig_HLT_2g20_tight_icalovloose_L12EM15VHI, &b_EventInfoAuxDyn_passTrig_HLT_2g20_tight_icalovloose_L12EM15VHI);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_2g22_tight", &EventInfoAuxDyn_passTrig_HLT_2g22_tight, &b_EventInfoAuxDyn_passTrig_HLT_2g22_tight);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_2g22_tight_L12EM15VHI", &EventInfoAuxDyn_passTrig_HLT_2g22_tight_L12EM15VHI, &b_EventInfoAuxDyn_passTrig_HLT_2g22_tight_L12EM15VHI);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_g120_loose", &EventInfoAuxDyn_passTrig_HLT_g120_loose, &b_EventInfoAuxDyn_passTrig_HLT_g120_loose);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_g140_loose", &EventInfoAuxDyn_passTrig_HLT_g140_loose, &b_EventInfoAuxDyn_passTrig_HLT_g140_loose);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_g35_loose_g25_loose", &EventInfoAuxDyn_passTrig_HLT_g35_loose_g25_loose, &b_EventInfoAuxDyn_passTrig_HLT_g35_loose_g25_loose);
   fChain->SetBranchAddress("EventInfoAuxDyn.passTrig_HLT_g35_medium_g25_medium_L12EM20VH", &EventInfoAuxDyn_passTrig_HLT_g35_medium_g25_medium_L12EM20VH, &b_EventInfoAuxDyn_passTrig_HLT_g35_medium_g25_medium_L12EM20VH);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn.DL1_pb", &BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pb, &b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pb);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn.DL1_pc", &BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pc, &b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pc);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn.DL1_pu", &BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pu, &b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1_pu);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn.DL1r_pb", &BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pb, &b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pb);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn.DL1r_pc", &BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pc, &b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pc);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn.DL1r_pu", &BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pu, &b_BTagging_HGamAntiKt4PFlowCustomVtxHggAuxDyn_DL1r_pu);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.CorrJvf", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_CorrJvf, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_CorrJvf);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DFCommonJets_fJvt", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DFCommonJets_fJvt, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DFCommonJets_fJvt);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1_FixedCutBEff_60", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_60, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1_FixedCutBEff_70", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_70, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1_FixedCutBEff_77", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_77, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1_FixedCutBEff_85", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_85, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1_bin", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_bin, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_bin);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1_pb", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pb, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pb);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1_pc", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pc, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pc);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1_pu", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pu, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1_pu);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_60", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_60, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_70", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_70, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_77, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_85", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_85, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_bin", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_bin, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_bin);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_pb", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pb, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pb);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_pc", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pc, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pc);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_pu", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pu, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DL1r_pu);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DetectorEta", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DetectorEta, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_DetectorEta);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.Eff_DL1r_FixedCutBEff_60", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_60, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.Eff_DL1r_FixedCutBEff_70", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_70, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.Eff_DL1r_FixedCutBEff_77", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_77, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.Eff_DL1r_FixedCutBEff_85", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_85, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Eff_DL1r_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.Jvt", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Jvt, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Jvt);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.NNRegPtSF", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_NNRegPtSF, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_NNRegPtSF);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.OneMu_eta", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_eta, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_eta);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.OneMu_m", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_m, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_m);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.OneMu_phi", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_phi, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_phi);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.OneMu_pt", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_pt, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_OneMu_pt);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.PtRecoSF", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_PtRecoSF, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_PtRecoSF);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.Rpt", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Rpt, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Rpt);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.SF_DL1r_FixedCutBEff_60", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_60, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.SF_DL1r_FixedCutBEff_70", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_70, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.SF_DL1r_FixedCutBEff_77", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_77, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.SF_DL1r_FixedCutBEff_85", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_85, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_DL1r_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.SF_fjvt", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_fjvt, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_fjvt);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.SF_jvt", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_jvt, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_SF_jvt);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.Timing", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Timing, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_Timing);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.btaggingLink", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.btaggingLink.m_persKey", HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_m_persKey, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_m_persKey);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.btaggingLink.m_persIndex", HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_m_persIndex, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_btaggingLink_m_persIndex);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.eta", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_eta, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_eta);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.m", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_m, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_m);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.overlapTau", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_overlapTau, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_overlapTau);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.passFJVT", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_passFJVT, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_passFJVT);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.phi", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_phi, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_phi);
   fChain->SetBranchAddress("HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.pt", &HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_pt, &b_HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_pt);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn.DL1_pb", &BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pb, &b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pb);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn.DL1_pc", &BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pc, &b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pc);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn.DL1_pu", &BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pu, &b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1_pu);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn.DL1r_pb", &BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pb, &b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pb);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn.DL1r_pc", &BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pc, &b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pc);
   fChain->SetBranchAddress("BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn.DL1r_pu", &BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pu, &b_BTagging_HGamAntiKt4EMPFlow_BTagging201903AuxDyn_DL1r_pu);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1_FixedCutBEff_60", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1_FixedCutBEff_70", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1_FixedCutBEff_77", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1_FixedCutBEff_85", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1_bin", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_bin, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1_bin);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1r_FixedCutBEff_60", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1r_FixedCutBEff_70", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1r_FixedCutBEff_77", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1r_FixedCutBEff_85", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DL1r_bin", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_bin, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DL1r_bin);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.DetectorEta", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DetectorEta, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_DetectorEta);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.Jvt", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_Jvt, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_Jvt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.SF_DL1r_FixedCutBEff_60", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_60, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_60);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.SF_DL1r_FixedCutBEff_70", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_70, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_70);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.SF_DL1r_FixedCutBEff_77", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_77, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_77);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.SF_DL1r_FixedCutBEff_85", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_85, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_DL1r_FixedCutBEff_85);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.SF_jvt", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_jvt, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_SF_jvt);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.btaggingLink", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.btaggingLink.m_persKey", HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_m_persKey, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_m_persKey);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.btaggingLink.m_persIndex", HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_m_persIndex, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_btaggingLink_m_persIndex);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.eta", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_eta, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_eta);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.m", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_m, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_m);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.overlapTau", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_overlapTau, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_overlapTau);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.passFJVT", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_passFJVT, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_passFJVT);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.phi", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_phi, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_phi);
   fChain->SetBranchAddress("HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn.pt", &HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_pt, &b_HGamAntiKt4EMPFlowJets_BTagging201903AuxDyn_pt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_BDT", &HGamEventInfoAuxDyn_HiggsHF_BDT, &b_HGamEventInfoAuxDyn_HiggsHF_BDT);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_BDT_cat", &HGamEventInfoAuxDyn_HiggsHF_BDT_cat, &b_HGamEventInfoAuxDyn_HiggsHF_BDT_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dEta_yy", &HGamEventInfoAuxDyn_HiggsHF_dEta_yy, &b_HGamEventInfoAuxDyn_HiggsHF_dEta_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dPhi_yy_leadBtagjet", &HGamEventInfoAuxDyn_HiggsHF_dPhi_yy_leadBtagjet, &b_HGamEventInfoAuxDyn_HiggsHF_dPhi_yy_leadBtagjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_y1_leadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_dR_y1_leadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_dR_y1_leadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_y1_subleadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_dR_y1_subleadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_dR_y1_subleadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_y2_leadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_dR_y2_leadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_dR_y2_leadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_y2_subleadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_dR_y2_subleadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_dR_y2_subleadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_dR_yy", &HGamEventInfoAuxDyn_HiggsHF_dR_yy, &b_HGamEventInfoAuxDyn_HiggsHF_dR_yy);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_effmass_yyjets", &HGamEventInfoAuxDyn_HiggsHF_effmass_yyjets, &b_HGamEventInfoAuxDyn_HiggsHF_effmass_yyjets);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_foxw1_yyjets", &HGamEventInfoAuxDyn_HiggsHF_foxw1_yyjets, &b_HGamEventInfoAuxDyn_HiggsHF_foxw1_yyjets);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_leadBtagJet_btagbin", &HGamEventInfoAuxDyn_HiggsHF_leadBtagJet_btagbin, &b_HGamEventInfoAuxDyn_HiggsHF_leadBtagJet_btagbin);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_leadBtagjet_eta", &HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_eta, &b_HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_eta);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_leadBtagjet_pt", &HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_pt, &b_HGamEventInfoAuxDyn_HiggsHF_leadBtagjet_pt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_truth_label_LeadBtagJet", &HGamEventInfoAuxDyn_HiggsHF_truth_label_LeadBtagJet, &b_HGamEventInfoAuxDyn_HiggsHF_truth_label_LeadBtagJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.HiggsHF_weight", &HGamEventInfoAuxDyn_HiggsHF_weight, &b_HGamEventInfoAuxDyn_HiggsHF_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_SelectedForwardJet", &HGamEventInfoAuxDyn_yyb_SelectedForwardJet, &b_HGamEventInfoAuxDyn_yyb_SelectedForwardJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_SelectedTaggedJet", &HGamEventInfoAuxDyn_yyb_SelectedTaggedJet, &b_HGamEventInfoAuxDyn_yyb_SelectedTaggedJet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_VLQmass", &HGamEventInfoAuxDyn_yyb_VLQmass, &b_HGamEventInfoAuxDyn_yyb_VLQmass);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_m_yy_cat", &HGamEventInfoAuxDyn_yyb_m_yy_cat, &b_HGamEventInfoAuxDyn_yyb_m_yy_cat);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_truth_label_forwardjet", &HGamEventInfoAuxDyn_yyb_truth_label_forwardjet, &b_HGamEventInfoAuxDyn_yyb_truth_label_forwardjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_truth_label_taggedjet", &HGamEventInfoAuxDyn_yyb_truth_label_taggedjet, &b_HGamEventInfoAuxDyn_yyb_truth_label_taggedjet);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yyb_weight", &HGamEventInfoAuxDyn_yyb_weight, &b_HGamEventInfoAuxDyn_yyb_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_BCal_vbf_selected", &HGamEventInfoAuxDyn_yybb_btag77_BCal_vbf_selected, &b_HGamEventInfoAuxDyn_yybb_btag77_BCal_vbf_selected);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_BReg_vbf_selected", &HGamEventInfoAuxDyn_yybb_btag77_BReg_vbf_selected, &b_HGamEventInfoAuxDyn_yybb_btag77_BReg_vbf_selected);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_KF_vbf_selected", &HGamEventInfoAuxDyn_yybb_btag77_KF_vbf_selected, &b_HGamEventInfoAuxDyn_yybb_btag77_KF_vbf_selected);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_vbf_selected", &HGamEventInfoAuxDyn_yybb_btag77_vbf_selected, &b_HGamEventInfoAuxDyn_yybb_btag77_vbf_selected);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.BDTEleScore", &HGamTauJetsAuxDyn_BDTEleScore, &b_HGamTauJetsAuxDyn_BDTEleScore);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.BDTEleScoreSigTrans", &HGamTauJetsAuxDyn_BDTEleScoreSigTrans, &b_HGamTauJetsAuxDyn_BDTEleScoreSigTrans);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.BDTEleScoreSigTrans_retuned", &HGamTauJetsAuxDyn_BDTEleScoreSigTrans_retuned, &b_HGamTauJetsAuxDyn_BDTEleScoreSigTrans_retuned);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.RNNJetScore", &HGamTauJetsAuxDyn_RNNJetScore, &b_HGamTauJetsAuxDyn_RNNJetScore);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.RNNJetScoreSigTrans", &HGamTauJetsAuxDyn_RNNJetScoreSigTrans, &b_HGamTauJetsAuxDyn_RNNJetScoreSigTrans);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.charge", &HGamTauJetsAuxDyn_charge, &b_HGamTauJetsAuxDyn_charge);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.eta", &HGamTauJetsAuxDyn_eta, &b_HGamTauJetsAuxDyn_eta);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.nTracks", &HGamTauJetsAuxDyn_nTracks, &b_HGamTauJetsAuxDyn_nTracks);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.overlapJet", &HGamTauJetsAuxDyn_overlapJet, &b_HGamTauJetsAuxDyn_overlapJet);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.phi", &HGamTauJetsAuxDyn_phi, &b_HGamTauJetsAuxDyn_phi);
   fChain->SetBranchAddress("HGamTauJetsAuxDyn.pt", &HGamTauJetsAuxDyn_pt, &b_HGamTauJetsAuxDyn_pt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.fcnc_fJvtweight", &HGamEventInfoAuxDyn_fcnc_fJvtweight, &b_HGamEventInfoAuxDyn_fcnc_fJvtweight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_85_BCal_vbf_selected", &HGamEventInfoAuxDyn_yybb_btag77_85_BCal_vbf_selected, &b_HGamEventInfoAuxDyn_yybb_btag77_85_BCal_vbf_selected);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_85_BReg_vbf_selected", &HGamEventInfoAuxDyn_yybb_btag77_85_BReg_vbf_selected, &b_HGamEventInfoAuxDyn_yybb_btag77_85_BReg_vbf_selected);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_85_KF_vbf_selected", &HGamEventInfoAuxDyn_yybb_btag77_85_KF_vbf_selected, &b_HGamEventInfoAuxDyn_yybb_btag77_85_KF_vbf_selected);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.yybb_btag77_85_vbf_selected", &HGamEventInfoAuxDyn_yybb_btag77_85_vbf_selected, &b_HGamEventInfoAuxDyn_yybb_btag77_85_vbf_selected);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.charge", &HGamMuonsAuxDyn_charge, &b_HGamMuonsAuxDyn_charge);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.eta", &HGamMuonsAuxDyn_eta, &b_HGamMuonsAuxDyn_eta);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.isBad", &HGamMuonsAuxDyn_isBad, &b_HGamMuonsAuxDyn_isBad);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.isTight", &HGamMuonsAuxDyn_isTight, &b_HGamMuonsAuxDyn_isTight);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.muonType", &HGamMuonsAuxDyn_muonType, &b_HGamMuonsAuxDyn_muonType);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.neflowisol20", &HGamMuonsAuxDyn_neflowisol20, &b_HGamMuonsAuxDyn_neflowisol20);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.passIPCut", &HGamMuonsAuxDyn_passIPCut, &b_HGamMuonsAuxDyn_passIPCut);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.phi", &HGamMuonsAuxDyn_phi, &b_HGamMuonsAuxDyn_phi);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.pt", &HGamMuonsAuxDyn_pt, &b_HGamMuonsAuxDyn_pt);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.ptcone20_TightTTVA_pt500", &HGamMuonsAuxDyn_ptcone20_TightTTVA_pt500, &b_HGamMuonsAuxDyn_ptcone20_TightTTVA_pt500);
   fChain->SetBranchAddress("HGamMuonsAuxDyn.ptvarcone30_TightTTVA_pt500", &HGamMuonsAuxDyn_ptvarcone30_TightTTVA_pt500, &b_HGamMuonsAuxDyn_ptvarcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.author", &HGamElectronsAuxDyn_author, &b_HGamElectronsAuxDyn_author);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.charge", &HGamElectronsAuxDyn_charge, &b_HGamElectronsAuxDyn_charge);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.eta", &HGamElectronsAuxDyn_eta, &b_HGamElectronsAuxDyn_eta);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.eta_s2", &HGamElectronsAuxDyn_eta_s2, &b_HGamElectronsAuxDyn_eta_s2);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.isTight", &HGamElectronsAuxDyn_isTight, &b_HGamElectronsAuxDyn_isTight);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.m", &HGamElectronsAuxDyn_m, &b_HGamElectronsAuxDyn_m);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.phi", &HGamElectronsAuxDyn_phi, &b_HGamElectronsAuxDyn_phi);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.pt", &HGamElectronsAuxDyn_pt, &b_HGamElectronsAuxDyn_pt);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.ptvarcone20_TightTTVA_pt1000", &HGamElectronsAuxDyn_ptvarcone20_TightTTVA_pt1000, &b_HGamElectronsAuxDyn_ptvarcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("HGamElectronsAuxDyn.topoetcone20", &HGamElectronsAuxDyn_topoetcone20, &b_HGamElectronsAuxDyn_topoetcone20);
   Notify();
}

Bool_t data::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void data::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t data::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef data_cxx
