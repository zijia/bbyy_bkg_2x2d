#include <HGamBackgroundStudies/SB2x2DParameters.h>
  
#define WggTITI p[WGGTITI]
#define WgjTITI p[WGJTITI]
#define WjgTITI p[WJGTITI]
#define WjjTITI p[WJJTITI]
#define e1I     p[E1I]
#define e2I     p[E2I]
#define e1T     p[E1T]
#define e2T     p[E2T]
#define ep1I    p[EP1I]
#define ep2I    p[EP2I]
#define ep1T    p[EP1T]
#define ep2T    p[EP2T]
#define f1I     p[F1I]
#define f2I     p[F2I]
#define f1T     p[F1T]
#define f2T     p[F2T]
#define fp1I    p[FP1I]
#define fp2I    p[FP2I]
#define fp1T    p[FP1T]
#define fp2T    p[FP2T]
#define x1g     p[X1G]
#define x2g     p[X2G]
#define x1j     p[X1J]
#define x2j     p[X2J]
#define xIgg    p[XIGG]
#define xIgj    p[XIGJ]
#define xIjg    p[XIJG]
#define xIjj    p[XIJJ]
#define xTgg    p[XTGG]
#define xTgj    p[XTGJ]
#define xTjg    p[XTJG]
#define xTjj    p[XTJJ]

#define w2WggTITI w2p[WGGTITI]
#define w2WgjTITI w2p[WGJTITI]
#define w2WjgTITI w2p[WJGTITI]
#define w2WjjTITI w2p[WJJTITI]
#define w2e1I     w2p[E1I]
#define w2e2I     w2p[E2I]
#define w2e1T     w2p[E1T]
#define w2e2T     w2p[E2T]
#define w2ep1I    w2p[EP1I]
#define w2ep2I    w2p[EP2I]
#define w2ep1T    w2p[EP1T]
#define w2ep2T    w2p[EP2T]
#define w2f1I     w2p[F1I]
#define w2f2I     w2p[F2I]
#define w2f1T     w2p[F1T]
#define w2f2T     w2p[F2T]
#define w2fp1I    w2p[FP1I]
#define w2fp2I    w2p[FP2I]
#define w2fp1T    w2p[FP1T]
#define w2fp2T    w2p[FP2T]
#define w2x1g     w2p[X1G]
#define w2x2g     w2p[X2G]
#define w2x1j     w2p[X1J]
#define w2x2j     w2p[X2J]
#define w2xIgg    w2p[XIGG]
#define w2xIgj    w2p[XIGJ]
#define w2xIjg    w2p[XIJG]
#define w2xIjj    w2p[XIJJ]
#define w2xTgg    w2p[XTGG]
#define w2xTgj    w2p[XTGJ]
#define w2xTjg    w2p[XTJG]
#define w2xTjj    w2p[XTJJ]

namespace SB {

SB2x2DParameters::SB2x2DParameters()
{
  SetDefaults();
}

SB2x2DParameters::~SB2x2DParameters()
{
}

void SB2x2DParameters::SetDefaults()
{
  for(int i=0;i<X1G;i++) {
    p[i]=w2p[i]=errup[i]=errlo[i]=0;
  }
  for(int i=X1G;i<NPARAMS;i++) {
    p[i]=1; w2p[i]=errup[i]=errlo[i]=0;
  }
}

void SB2x2DParameters::SetValues(const SB2x2DParameters& par)
{
  for(int i=0;i<NPARAMS;i++) {
    p[i]   = par.GetValue(i);
    w2p[i] = par.GetError2(i);
  }
}

void SB2x2DParameters::SimplifyIsolationInputs() 
{
  // Simplify input values
  ep1I = ep1I*x1g*xIgg;
  ep2I = ep2I*x2g*xIgg;
  e1I  = e1I*x1g*xIgj;
  e2I  = e2I*x2g*xIjg;
  fp1I = fp1I*x1j;
  fp2I = fp2I*x2j;
  f1I  = f1I*x1j*xIjg;
  f2I  = f2I*x2j*xIgj;
  // Only xIjj can be different from 1
  for(int i=X1G;i<NPARAMS;i++) {
    if(i==XIJJ) continue;
    p[i] = 1; w2p[i] = 0;
  }
}

void SB2x2DParameters::SimplifyEfficiencyInputs() 
{
  ep1T = e1T = ep1T*x1g*xTgg;
  ep2T = e2T = ep2T*x2g*xTgg;
  ep1I = e1I = ep1I*x1g*xIgg;
  ep2I = e2I = ep2I*x2g*xIgg;
  w2ep1I = w2ep2I = w2ep1T = w2ep2T = 0;
  
  for(int i=X1G;i<NPARAMS;i++) {
    if(i==XIJJ) continue;
    p[i] = 1; w2p[i] = 0;
  }
}

void SB2x2DParameters::Simplest() 
{
  SimplifyEfficiencyInputs();
  xIjj = 1; w2xIjj = 0;
}

void SB2x2DParameters::SetValues(const double* par)
{
  for(int i=0;i<NPARAMS;i++) {
    p[i] = par[i];
  }
}

void SB2x2DParameters::SetErrors(const double* par)
{
  for(int i=0;i<NPARAMS;i++) {
    w2p[i] = par[i]*par[i];
  }
}

}
