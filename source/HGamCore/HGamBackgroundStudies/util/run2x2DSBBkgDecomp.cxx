#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include "HGamBackgroundStudies/DiPhotonBkg2x2DSB.h"

int main( int argc, char* argv[] ) {

  Str cfgName="";
  // Take the submit directory from the input if provided:
  Str submitDir = "submitDir";
  //if( argc > 1 ) submitDir = argv[ 1 ];
  //if( argc > 2 ) sb_name = argv[ 2 ];
  if( argc > 1 ) cfgName = argv[ 1 ];
  DiPhotonBkg2x2DSB* alg = new DiPhotonBkg2x2DSB("DiPhotonBkg2x2DSB");
  alg->initialize(cfgName);
  alg->execute();
  alg->finalize();    
  return 0;
}

