#include "HGamBackgroundStudies/HGam2x2DInputMaker.h"
#include "HGamBackgroundStudies/RunNAFJob.h"
#include "HGamAnalysisFramework/RunUtils.h"
#include "HGamAnalysisFramework/Config.h"
#include "EventLoop/DirectDriver.h"                                                                 
#include "EventLoop/Job.h"                                                                          
#include "SampleHandler/SampleHandler.h"                                                            
#include "SampleHandler/ToolsDiscovery.h"
#include <EventLoop/GEDriver.h> 
#include <SampleHandler/SampleLocal.h>                                                              
#include <SampleHandler/ToolsSplit.h> 


int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();  

  // Create our algorithm                                                                           
  HGam2x2DInputMaker *alg = new HGam2x2DInputMaker("HGam2x2DInputMaker"); 
  HG::runJob(alg, argc, argv);

  /* // THIS WAS OLD STYLE
  // Get cutflow hist name
  Str cf_hist_name = argv[1];

  // remove the leading one here since it seems to do funny things
  char** argv2 = new char*[argc-1];
  for(int i=1; i < argc; i++) {
    argv2[i-1] = argv[i];
  }

  std::cout<<" HISTOGRAM NAME :: "<<cf_hist_name<<std::endl;
  // Create our algorithm                                                                           
  HGam2x2DInputMaker *alg = new HGam2x2DInputMaker(cf_hist_name,"HGam2x2DInputMaker"); 
  HG::runJob(alg, argc-1, argv2);
  */

  return 0;
}
