#!/bin/bash

script=run_job.sh

outDir="/path/to/directory/where/you/want/the/output/from/Step1/"
baseInDir="/nfs/pt3nfs/xdata/jahreda/hgam_signaldecomp"
baseDir=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/

inDir=${baseDir}h015d/data15/
${baseDir}h015d/data16/
${baseDir}h015d/mc15c.Sherpa_2DP20_myy_100_165_3jets.MxAODDetailed.p2666.h015d.root/
${baseDir}h015d/mc15c.Sherpa_2DP20_myy_165_200_3jets.MxAODDetailed.p2812.h015d.root/
${baseDir}h015d/mc15c.Sherpa_2DP20_myy_90_100_3jets.MxAODDetailed.p2949.h015d.root/


for dir in $inDir ; do
    for f in $(ls ${dir}*.root*) ; do
	fn=$(basename $f)
	sumWConfig=SumOfWeights_h015.config
        fn=$(basename $f)
	log=sub_${fn/root/logQ}
        err=sub_${fn/root/errQ}
	echo "---------------------------------------"
	
        if [ ! -d ${outDir}${fn}OUTPUT ] ; then
      	    echo "submitting job with parameters:"
	    echo $dir
       	    echo $f
	    echo $fn
	    #echo $dir$fn
	    #echo $log
	    #echo $outDir
	    
	    searchstr="mc15c."
	    cutFlowHistTemp=CutFlow_${fn#*$searchstr}
	    echo $cutFlowHistTemp

	    cutFlowHist=$( echo $cutFlowHistTemp | cut -f1 -d'.' )
	    
	    echo ${cutFlowHist}_noDalitz_weighted
	    torun="runHGam2x2DInputMaker ${cutFlowHist}_noDalitz_weighted BaseConfig: ${baseInDir}HGamAnalysisFramework/data/HGamRel20p7.config ${baseInDir}HGamAnalysisFramework/data/${sumWConfig} ${baseInDir}HGamAnalysisFramework/data/MCSamples.config $f OutputDir: ${outDir}${fn}OUTPUT SampleName: ${fn} &> $log"
            echo $torun
	else
	    echo "Dir exists!!"
        fi
    done
done

infiles="${baseDir}h015d/mc15c.Pythia8_2DP20_Mass_55_100.MxAODDetailed.p2666.h015d.root
${baseDir}h015d/mc15c.Pythia8_2DP20_Mass_100_160.MxAODDetailed.p2666.h015d.root
${baseDir}h015d/mc15c.Pythia8_2DP20_Mass_160_250.MxAODDetailed.p2666.h015d.root
${baseDir}h015d/mc15c/mc15c.Sherpa_2DP20_myy_100_160.MxAODDetailed.p2666.h015.root
${baseDir}h015d/mc15c/mc15c.Sherpa_2DP20_myy_100_165.MxAODDetailed.p2666.h015.root
${baseDir}h015d/mc15c/mc15c.Sherpa_2DP20_myy_160_250.MxAODDetailed.p2666.h015.root
${baseDir}h015d/mc15c/mc15c.Sherpa_2DP20_myy_55_100.MxAODDetailed.p2666.h015.root
"


for f in $infiles ; do
    fn=$(basename $f)
    log=sub_${fn/root/logQ}
    err=sub_${fn/root/errQ}
    echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    if [ ! -d ${outDir}${fn}OUTPUT ] ; then
	sumWConfig=SumOfWeights_h015.config
     	echo "submitting job with parameters:"
	echo $f
	echo $fn
	echo $log
	    	    
	searchstr="mc15c."
	cutFlowHistTemp=CutFlow_${fn#*$searchstr}
	echo $cutFlowHistTemp
	
	cutFlowHist=$( echo $cutFlowHistTemp | cut -f1 -d'.' )
	
	echo ${cutFlowHist}_noDalitz_weighted
        torun="runHGam2x2DInputMaker ${cutFlowHist}_noDalitz_weighted BaseConfig: ${baseInDir}HGamAnalysisFramework/data/HGamRel20p7.config ${baseInDir}HGamAnalysisFramework/data/${sumWConfig} ${baseInDir}HGamAnalysisFramework/data/MCSamples.config $f OutputDir: ${outDir}${fn}OUTPUT SampleName: ${fn} &> $log & "
        echo $torun
    fi

done

#EOF
