#include "TH1.h"
#include "TMarker.h"
#include "TBox.h"
#include "TPaveLabel.h"

using namespace std;

typedef TString Str;
typedef vector<double> VecD;
typedef vector<Str> StrV;

TCanvas *_can;
TLatex *_tex;
double TEXTSIZE = 0.05;
bool is8TeV = false;
bool isPseudoData = false;
/* double _lumi = 36.1; */
/* //double _lumi = 43.8; */

Str GetXTitle(Str htype)
{
  if(htype.Contains("mu")) return "#mu";
  if(htype.Contains("cat_ntag")) return "HF category";
  if(htype.Contains("N_j_central30")) return "N_{jets}~[p^{jet}_{T}>30~GeV, ctr.]";
  if(htype.Contains("mgg")) return "m_{#gamma#gamma} [GeV]";
  if(htype.Contains("runNumber")) return "Run Number";
  if(htype.Contains("m_yy")) return "m_{#gamma#gamma} [GeV]";
  if(htype.Contains("N_lep_15")) return "XS: N_{lep} cat";
  if(htype.Contains("catXS_VBF")) return "XS: VBF enriched";
  if(htype.Contains("catXS_ttH")) return "XS: ttH";
  if(htype.Contains("catXS_Nlep")) return "XS: Nlep";
  if(htype.Contains("catXS_MET80pTyy80")) return "XS: high MET";
  if(htype.Contains("yAbs_yy")) return "|y_{#gamma#gamma}|";
  if(htype.Contains("cosTS_yy")) return "cos#theta^{*}_{#gamma#gamma}";
  if(htype.Contains("pTt_yy")) return "p_{Tt} [GeV]";
  if(htype.Contains("Dy_y_y")) return "#Delta y_{#gamma#gamma}";
  if(htype.Contains("HT_30")) return "H_{T} [GeV]";
  if(htype.Contains("N_j_30")) return "N_{jets}~[p^{jet}_{T}>30~GeV]";
  if(htype.Contains("N_j_50")) return "N_{jets}~[p^{jet}_{T}>50~GeV]";
  if(htype.Contains("N_j")) return "N_{jets}~[p^{jet}_{T}>25~GeV]";
  if(htype.Contains("pT_j1_30")) return "p_{T}^{j1} [GeV]";
  if(htype.Contains("pT_j2_30")) return "p_{T}^{j2} [GeV]";
  if(htype.Contains("pT_j3_30")) return "p_{T}^{j3} [GeV]";
  if(htype.Contains("yAbs_j1_30")) return "|#it{y}_{j1}|";
  if(htype.Contains("yAbs_j2_30")) return "|#it{y}_{j2}|";
  if(htype.Contains("Dphi_j_j_30_signed")) return "#Delta#phi_{jj} [rad]";
  if(htype.Contains("Dphi_j_j_30")) return "|#Delta#phi_{jj}| [rad]";
  if(htype.Contains("Dy_j_j_30")) return "#Delta y_{jj}";
  if(htype.Contains("m_jj_30")) return "#it{m}_{jj}  [GeV]";
  if(htype.Contains("pT_yyjj_30")) return "p_{T}^{#gamma#gamma,jj} [GeV]";
  if(htype.Contains("Dphi_yy_jj_30")) return "|#Delta#phi_{#gamma#gamma,jj}| [rad]";
  if(htype.Contains("pT_yy_0j_30")) return "p_{T}^{#gamma#gamma} [GeV]";
  if(htype.Contains("pT_yy_1j_30")) return "p_{T}^{#gamma#gamma} [GeV]";
  if(htype.Contains("pT_yy_2j_30")) return "p_{T}^{#gamma#gamma} [GeV]";
  if(htype.Contains("pT_yy_3j_30")) return "p_{T}^{#gamma#gamma} [GeV]";

  if(htype.Contains("pT_yy_cosTS_1")) return "p_{T}^{#gamma#gamma} [GeV]";
  if(htype.Contains("pT_yy_cosTS_2")) return "p_{T}^{#gamma#gamma} [GeV]";

  if(htype.Contains("sumTau_yyj_30")) return "#Sigma#tau^{#gamma#gamma,jj} [GeV]";
    if(htype.Contains("maxTau_yyj_30")) return "max #tau^{#gamma#gamma,jj} [GeV]";
  
  if(htype.Contains("catCoup")) return "Coupling Cat";
  if(htype.Contains("pT_yy")) return "p_{T}^{#gamma#gamma} [GeV]";
  if(htype.Contains("yybb")) return "bb#gamma#gamma Cat.";
  if(htype.Contains("XGB")) return "XGB Cat.";
  if(htype.Contains("catHiggsHF")) return "Higgs HF category";
  if(htype.Contains("inclusive")) return "Inclusive";
  return "Can't understand htype: "+htype;
}

Str GetCouplingBinLabel(int bin)
{
  if(bin==1) return "Baseline";
  if(bin==2) return "GGH 0J CEN";
  if(bin==3) return "GGH 0J FWD";
  if(bin==4) return "GGH 1J LOW";
  if(bin==5) return "GGH 1J MED";
  if(bin==6) return "GGH 1J HIGH";
  if(bin==7) return "GGH 1J BSM";
  if(bin==8) return "GGH 2J LOW";
  if(bin==9) return "GGH 2J MED";
  if(bin==10) return "GGH 2J HIGH";
  if(bin==11) return "GGH 2J BSM";
  if(bin==12) return "VBF HjjLO loose";
  if(bin==13) return "VBF HjjLO tight";
  if(bin==14) return "VBF HjjHI loose";
  if(bin==15) return "VBF HjjHI tight";
  if(bin==16) return "VHhad loose";
  if(bin==17) return "VHhad tight";
  if(bin==18) return "QQH BSM";
  if(bin==19) return "VHMET LOW";
  if(bin==20) return "VHMET MED";
  if(bin==21) return "VHMET BSM";
  if(bin==22) return "VHlep LOW";
  if(bin==23) return "VHlep HIGH";
  if(bin==24) return "VHdilep LOW";
  if(bin==25) return "VHdilep HIGH";
  if(bin==26) return "ttHhad 6j2b";
  if(bin==27) return "ttHhad 6j1b";
  if(bin==28) return "ttHhad 5j2b";
  if(bin==29) return "ttHhad 5j1b";
  if(bin==30) return "tHhad 4j2b";
  if(bin==31) return "tHhad 4j1b";
  if(bin==32) return "ttHlep";
  if(bin==33) return "tHlep 1fwd";
  if(bin==34) return "tHlep 0fwd";
  return "Can't understand coupling label!";
}



/* Str GetCoupBDTlingBinLabel(int bin) */
/* { */
/*   if(bin==1) return "Baseline"; */
/*   if(bin==2) return "GGH 0J CEN"; */
/*   if(bin==3) return "GGH 0J FWD"; */
/*   if(bin==4) return "GGH 1J LOW"; */
/*   if(bin==5) return "GGH 1J MED"; */
/*   if(bin==6) return "GGH 1J HIGH"; */
/*   if(bin==7) return "GGH 1J BSM"; */
/*   if(bin==8) return "GGH 2J LOW"; */
/*   if(bin==9) return "GGH 2J MED"; */
/*   if(bin==10) return "GGH 2J HIGH"; */
/*   if(bin==11) return "GGH 2J BSM"; */
/*   if(bin==12) return "VBF HjjLO loose"; */
/*   if(bin==13) return "VBF HjjLO tight"; */
/*   if(bin==14) return "VBF HjjHI loose"; */
/*   if(bin==15) return "VBF HjjHI tight"; */
/*   if(bin==16) return "VHhad loose"; */
/*   if(bin==17) return "VHhad tight"; */
/*   if(bin==18) return "QQH BSM"; */
/*   if(bin==19) return "VHMET LOW"; */
/*   if(bin==20) return "VHMET HIGH"; */
/*   if(bin==21) return "VHlep LOW"; */
/*   if(bin==22) return "VHlep HIGH"; */
/*   if(bin==23) return "VHdilep"; */
/*   if(bin==24) return "tHhad 4j2b"; */
/*   if(bin==25) return "tHhad 4j1b"; */
/*   if(bin==26) return "ttHhad BDT4"; */
/*   if(bin==27) return "ttHhad BDT3"; */
/*   if(bin==28) return "ttHhad BDT2"; */
/*   if(bin==29) return "ttHhad BDT1"; */
/*   if(bin==30) return "ttHlep"; */
/*   if(bin==31) return "tHlep 1fwd"; */
/*   if(bin==32) return "tHlep 0fwd"; */
/*   return "Can't understand coupling BDT label!"; */
/* } */


Str GetCouplingBDTBinLabel(int bin)
{
  if(bin==0) return "Baseline";
  if(bin==1) return "ggH 0J Cen";
  if(bin==2) return "ggH 0J Fwd";
  if(bin==3) return "ggH 1J LOW";
  if(bin==4) return "ggH 1J MED";
  if(bin==5) return "ggH 1J HIGH";
  if(bin==6) return "ggH 1J BSM";
  if(bin==7) return "ggH 2J LOW";
  if(bin==8) return "ggH 2J MED";
  if(bin==9) return "ggH 2J HIGH";
  if(bin==10) return "ggH 2J BSM";
  if(bin==11) return "VBF HjjLOW loose";
  if(bin==12) return "VBF HjjLOW tight";
  if(bin==13) return "VBF HjjHIGH loose";
  if(bin==14) return "VBF HjjHIGH tight";
  if(bin==15) return "VHhad loose";
  if(bin==16) return "VHhad tight";
  if(bin==17) return "qqH BSM";
  if(bin==18) return "VHMET LOW";
  if(bin==19) return "VHMET HIGH BSM";
  if(bin==20) return "VHMET BSM";
  if(bin==21) return "VHlep LOW";
  if(bin==22) return "VHlep HIGH";
  if(bin==23) return "VHdilep LOW HIGH";
  if(bin==24) return "VHdilep HIGH";
  if(bin==25) return "ttH Had 6j2b";
  if(bin==26) return "ttH Had 6j1b";
  if(bin==27) return "ttH Had 5j2b";
  if(bin==28) return "ttH Had 5j1b";
  if(bin==29) return "ttH Had 4j2b";
  if(bin==30) return "ttH Had 4j1b";
  if(bin==31) return "ttH Lep";
  if(bin==32) return "ttH Lep 0fwd";
  if(bin==33) return "ttH Lep 1fwd";
  return "Can't understand coupling BDT label!";
}



void SetXTitle(TH1D* h)
{
  if(h==0) return;
  Str htype = h->GetName();
  Str xtitle = GetXTitle(htype);
  
  if(xtitle.Contains("N_{jets}")){
    h->GetXaxis()->SetBinLabel(1,"N_{jets} = 0");
    h->GetXaxis()->SetBinLabel(2,"N_{jets} = 1");
    if(xtitle.Contains("50")){
      h->GetXaxis()->SetBinLabel(3,"N_{jets} #geq 2");
    }else{
      h->GetXaxis()->SetBinLabel(3,"N_{jets} = 2");
      h->GetXaxis()->SetBinLabel(4,"N_{jets} #geq 3");
    }
  }else if(xtitle.Contains("CoupBDT")){  
    for(int i=1; i<=h->GetNbinsX(); i++)
      h->GetXaxis()->SetBinLabel(i,GetCouplingBDTBinLabel(i));
  }else if(xtitle.Contains("Coup")){  
    for(int i=1; i<=h->GetNbinsX(); i++)
      h->GetXaxis()->SetBinLabel(i,GetCouplingBinLabel(i));
  }else if(xtitle.Contains("XS: VBF")){  
    h->GetXaxis()->SetBinLabel(1,"Rest");
    h->GetXaxis()->SetBinLabel(2,"VBF");
  }else if(xtitle.Contains("XS: N_{lep}")){
    h->GetXaxis()->SetBinLabel(1,"Rest");
    h->GetXaxis()->SetBinLabel(2,"N_{lep}");
  }else if(xtitle.Contains("XS: ttH cat")){
    h->GetXaxis()->SetBinLabel(1,"Rest");
    h->GetXaxis()->SetBinLabel(2,"ttH");
  }else if(xtitle.Contains("bb#gamma#gamma")){
    h->GetXaxis()->SetBinLabel(1,"0-btag");
    h->GetXaxis()->SetBinLabel(2,"1-btag");
    h->GetXaxis()->SetBinLabel(3,"2-btag");
    h->GetXaxis()->SetTitle(xtitle);
  }else if(xtitle.Contains("XGB")){
    h->GetXaxis()->SetBinLabel(1,"highMass BDTtight");
    h->GetXaxis()->SetBinLabel(2,"highMass BDTloose");
    h->GetXaxis()->SetBinLabel(3,"lowMass BDTtight");
    h->GetXaxis()->SetBinLabel(4,"lowMass BDTloose");
    h->GetXaxis()->SetTitle(xtitle);
  }else if(xtitle.Contains("cos")){
    h->GetXaxis()->SetTitle("|cos(#it{#theta*})|");
  }else
    h->GetXaxis()->SetTitle(xtitle);
}

void SetYTitle(TH1* h)
{
  if(h==0) return;
  Str htype = h->GetName();
  Str ytitle = "Entries/GeV";
  if(htype.Contains("mgg"))       ytitle = "dN/dm_{#gamma#gamma} [GeV^{-1}]";
  if(htype.Contains("ptgg"))      ytitle = "dN/dp_{T,#gamma#gamma} [GeV^{-1}]";
  if(htype.Contains("dphiggjj"))  ytitle = "dN/d#Delta#phi{y}_{#gamma#gamma,jj}";
  if(htype.Contains("dPhigg"))    ytitle = "dN/d#Delta#phi_{#gamma#gamma} [rad^{-1}]";
  if(htype.Contains("costsgg"))   ytitle = "dN/dcos#theta^{*}_{#gamma#gamma}";
  if(htype.Contains("dijety"))    ytitle = "dN/d#Delta#it{y}_{jj}";
  if(htype.Contains("eta1"))      ytitle = "dN/d|#eta_{#gamma1}|";
  if(htype.Contains("eta2"))      ytitle = "dN/d|#eta_{#gamma2}|";
  if(htype.Contains("dyggjj"))    ytitle = "dN/d#Delta#it{y}_{#gamma#gamma,jj}";
  if(htype.Contains("dphijj"))    ytitle = "dN/d#Delta#phi_{jj}";
  if(htype.Contains("absj1y"))    ytitle = "dN/d|#it{y}_{j1}|";
  if(htype.Contains("absj2y"))    ytitle = "dN/d|#it{y}_{j2}|";
  if(htype.Contains("zepp"))      ytitle = "dN/dZeppenfelder";
  if(htype.Contains("njet"))      ytitle = "dN/d(N_{jets})";
  h->GetYaxis()->SetTitle(ytitle);
}



Str GetPlotLabel(Str variable)
{  
  if(variable.Contains("pT_yy_0j"))    return "N_{jets} = 0";
  if(variable.Contains("pT_yy_1j"))    return "N_{jets} = 1";
  if(variable.Contains("pT_yy_2j"))    return "N_{jets} = 2";
  if(variable.Contains("pT_yy_3j"))    return "N_{jets} #geq 3";
  if(variable.Contains("njet0_mgg"))   return "N_{jets} = 0";
  if(variable.Contains("njet1_mgg"))   return "N_{jets} = 1";
  if(variable.Contains("njet2_mgg"))   return "N_{jets} = 2";
  if(variable.Contains("njetge3_mgg")) return "N_{jets} #geq 3";
  if(variable.Contains("njet0"))       return "N_{jets} = 0";
  if(variable.Contains("njet1"))       return "N_{jets} = 1";
  if(variable.Contains("njet2"))       return "N_{jets} = 2";
  if(variable.Contains("njetge3"))     return "N_{jets} #geq 3";
  if(variable.Contains("pT_yy_cosTS_1")) return " 0.0 #leq Cos#theta^{*} < 0.5";
  if(variable.Contains("pT_yy_cosTS_2")) return " 0.5 #leq Cos#theta^{*} < 1.0";
  
  //if(variable.Contains("catCoup") && !variable.Contains("mgg"))
  //return "Coupling Cat.";

   
  if(variable.Contains("mgg"))
    return variable.ReplaceAll("_mgg_5GeV","").ReplaceAll("_"," ");  
  
  return "";
}

Str GetCouplingCatLabel(Str cat)
{  
  return cat.ReplaceAll("_"," ");
}

Str GetSystematicLabel(Str sys)
{
  if(sys=="LoosePrime2")    return "LP2";
  if(sys=="LoosePrime3")    return "LP3";
  if(sys=="LoosePrime4")    return "LP4";
  if(sys=="LoosePrime5")    return "LP5";
  if(sys=="CRdefinition")   return "CR def. (env. of LP)";
  if(sys=="NOFUDGE")        return "Fudge Factor";
  if(sys=="FF")             return "Fudge Factor";
  if(sys=="NoFudgeFactor") return "Fudge Factor";
  if(sys=="PYTHIA")         return "Pythia vs. Sherpa";
  if(sys=="Generator")      return "Pythia vs. Sherpa";
  if(sys=="IsoEff")         return "isolation $\\epsilon$";
  return "Can't understand systematic label";
}

Str GetRegionLabel(Str variable)
{
  if(variable.Contains("njet0"))   return "N_{jets}=0";
  if(variable.Contains("njet1"))   return "N_{jets}=1";
  if(variable.Contains("njet2"))   return "N_{jets}=2";
  if(variable.Contains("njetge3")) return "N_{jets} \\geq 3";
  if(variable.Contains("0j"))      return "[N_{jets}=0]";
  if(variable.Contains("1j"))      return "[N_{jets}=1]";
  if(variable.Contains("2j"))      return "[N_{jets}=2]";
  if(variable.Contains("3j"))      return "[N_{jets} \\geq 3]";

  if(variable.Contains("pT_yy_cosTS_1")) return "Cos\\theta^{*} [0.0,0.5]";
  if(variable.Contains("pT_yy_cosTS_2")) return "Cos\\theta^{*} [0.0,1.0]";
  
  return "Can't understand region label";
}

Str GetNjetBinLabel(int bin)
{
  if(bin==1) return "$N_{jets} = 0$";
  if(bin==2) return "$N_{jets} = 1$";
  if(bin==3) return "$N_{jets} = 2$";
  if(bin==4) return "$N_{jets} \\geq 3$";
  return "Can't understand region label";
}

void PrintJetInfo(Str region, Str variable, double x = 0.66, double y = 0.76, int align=12 )
{
  Str jetPtStr = " (p_{T}^{jet}>30)";
  if(variable.Contains("25") ) jetPtStr = " (p_{T}^{jet}>25)";
  if(variable.Contains("50") ) jetPtStr = " (p_{T}^{jet}>50)";
  if(variable.Contains("central30") ) jetPtStr = " (p_{T}^{jet}>30,ctr.)";
  Str jetStr;
  if(region.Contains("Inclusive"))
    jetStr = "Inclusive";
  if(region.Contains("jet0"))
    jetStr = Form("N_{jet}%s = 0",jetPtStr.Data());
  if(region.Contains("jet1"))
    jetStr = Form("N_{jet}%s = 1",jetPtStr.Data());
  if(region.Contains("jet2"))
    jetStr = Form("N_{jet}%s = 2",jetPtStr.Data());
  if(region.Contains("jet3"))
    jetStr = Form("N_{jet}%s #geq 3",jetPtStr.Data());
  _tex->SetTextAlign(align);
  if( !(jetStr=="Inclusive") )
    _tex->DrawLatex(x,y,jetStr);
  if( jetStr=="Inclusive" && variable.Contains("njet") ){
    jetPtStr.ReplaceAll("(","").ReplaceAll(")"," [GeV]");
    _tex->DrawLatex(x,y,jetPtStr);
  }  
  _tex->SetTextAlign(12);
}

void DrawLabels( Str region, Str variable, double x, double y){
  PrintJetInfo(region, variable );
  float dx=0.02, dy=0.02, dx1 = 0.15;
  TMarker *m = new TMarker(x,y,20);
  m->SetNDC(); m->SetMarkerSize(1.0); m->Draw("same");
  TLine *l = new TLine(); 
  l->SetLineWidth(2);
  l->DrawLineNDC(x,y-dy,x,y+dy);
  _tex->DrawLatex(x+dx+0.012,y,"Stat. Unc.");
  y = y-0.05;
  float bleft = x-dx, bright = x+dx,
    btop = y+dy, bbot = y-dy;
  TPaveLabel *b = new TPaveLabel();
  b->SetBorderSize(1);
  b->SetY2NDC(btop);
  b->SetY1NDC(bbot);
  b->SetX2NDC(bright);
  b->SetX1NDC(bleft);
  b->SetFillColor(kGray+1);
  b->Draw("");
  _tex->DrawLatex(bright+0.012,y,"Tot. Unc.");
  _tex->SetTextSize(TEXTSIZE);
  
}

void DrawAtlasLabelSim(Str generator, Str variable="")
{
  TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextColor(1);
  double x = 0.2, y = 0.87, dy=0.06;
  l.DrawLatex(x,y,"ATLAS");
  TLatex lp; lp.SetNDC(); lp.SetTextColor(1);
  //lp.DrawLatex(x+0.12,y,"Simulation Preliminary");
  lp.DrawLatex(x+0.12,y,"Simulation Internal");
  TLatex ll; ll.SetNDC(); ll.SetTextColor(1);
  Str global_title = is8TeV ?
    "#sqrt{s} = 8 TeV     "+GetPlotLabel(variable) :
    " #sqrt{s} = 13 TeV     "+GetPlotLabel(variable);
  ll.DrawLatex(x,y-=dy,global_title);
  ll.DrawLatex(0.75,y+=dy,generator);
}



void DrawAtlasLabel(bool isPurityPlot = false, Str variable="",double _lumi=0.0)
{
  TLatex l;l.SetNDC();
  l.SetTextColor(1);
  l.SetTextAlign(12);  
  TLatex lp; lp.SetNDC(); lp.SetTextColor(1);  
  Str global_title;
  if(!is8TeV)      global_title = Form("#sqrt{s} = 13 TeV, %.0f fb^{-1}",_lumi);
  else             global_title = "#sqrt{s} = 8 TeV, 20.3 fb^{-1}"; 
  if(isPseudoData) global_title = "#sqrt{s} = 8 TeV, pseudo-data";
  double x = 0.2, y = 0.87, dy = 0.06;
  //Str label = "Preliminary";
  Str label = "Internal";

  if(!isPurityPlot){
    lp.SetTextAlign(12);
    l.DrawLatex(0.62,y,Form("#font[72]{ATLAS} %s",label.Data()));
    lp.DrawLatex(0.18,y,GetPlotLabel(variable));
    if(!isPseudoData) lp.DrawLatex(0.62,y-=dy,global_title);
    else              lp.DrawLatex(0.52,y-=dy,global_title);
  }else{
    lp.SetTextAlign(12);
    l.DrawLatex(0.18,y+0.02,Form("#font[72]{ATLAS} %s",label.Data()));
    lp.DrawLatex(0.18,y-0.06,GetPlotLabel(variable));
    if(!isPseudoData) lp.DrawLatex(0.62,y+0.02,global_title);
    else              lp.DrawLatex(0.52,y+0.02,global_title);
  }  
}



template<class distribution>
void FormatEffDistribution( distribution d, Str type)
{
  d->SetMarkerSize(1.2);
  d->SetFillStyle(1001);
  int col = 0;
  if ( type == "SherpaCand1Tight")
    { col = kBlue-1; d->SetMarkerStyle(20); }
  if ( type == "SherpaCand2Tight")
    { col = kYellow+3;  d->SetMarkerStyle(21); }
  if ( type == "SherpaCand1Iso")
    { col = kTeal-6;    d->SetMarkerStyle(22); }
  if ( type == "SherpaCand2Iso")
    { col = kMagenta-5; d->SetMarkerStyle(23); }
  if ( type == "PythiaCand1Tight")
    { col = kOrange+2;     d->SetMarkerStyle(24); }
  if ( type == "PythiaCand2Tight")
    { col = kRed+1;     d->SetMarkerStyle(25); }
  if ( type == "PythiaCand1Iso")
    { col = kOrange+2;  d->SetMarkerStyle(26); }
  if ( type == "PythiaCand2Iso")
    {col = kBlue-4;    d->SetMarkerStyle(32); }
  d->SetLineColor(col);
  d->SetMarkerColor(col);
  d->SetFillColorAlpha(col,0.6);
}

template<class distribution>
void FormatDistribution( distribution d, Str type)
{
  d->SetMarkerSize(0.8);
  d->SetFillStyle(1001);
  if ( type.Contains("2")) d->SetFillStyle(1005);
  if ( type == "gg" ) { d->SetFillColorAlpha(kOrange+2,0.6); d->SetMarkerStyle(20); }
  if ( type == "gj" ) { d->SetFillColorAlpha(kCyan+3,0.6); d->SetMarkerStyle(21); }
  if ( type == "jg" ) { d->SetFillColorAlpha(kCyan+4,0.6); d->SetMarkerStyle(23); }
  if ( type == "jj" ) { d->SetFillColorAlpha(kMagenta+2,0.6); d->SetMarkerStyle(22); }
  if ( type == "gg2" ) { d->SetFillColorAlpha(29,0.6); d->SetMarkerStyle(24); }
  if ( type == "gj2" ) { d->SetFillColorAlpha(46,0.6); d->SetMarkerStyle(25); }
  if ( type == "jg2" ) { d->SetFillColorAlpha(49,0.6); d->SetMarkerStyle(28); }
  if ( type == "jj2" ) { d->SetFillColorAlpha(41,0.6); d->SetMarkerStyle(26); }  
}


double FIGURE2_RATIO = 0.35;
void DrawSubPad(bool log_x)
{
  _can->SetBottomMargin(FIGURE2_RATIO);
  TPad *p = new TPad( "p_test", "", 0, 0, 1, 1, 0, 0, 0);
  p->SetFillStyle(0);
  p->SetMargin(_can->GetLeftMargin(),
	       _can->GetRightMargin(),
	       0.15, 1.0 - FIGURE2_RATIO + 0.02 );
  if(log_x) p->SetLogx(true);
  p->Draw();
  p->cd(); 
}

void DrawAxis(vector<double> xbins, double minY, double maxY,
	      TH1D* hin,
	      Str xtit = "", Str ytit = "",
	      bool logx=false, bool logy =false,
	      bool sameAxis=false, bool ratio=false)
{
  if ( (xbins[0]==-100 && xbins[1]==0.0) ||
       (xbins[0]==-100 && xbins[1]<0.0 && xbins[1]>-5 ) ) 
    xbins.erase(xbins.begin());
  
  Str hname(Form("mall_%s_%d_%d_%d_%d",hin->GetName(),
		     int(xbins[0]*100),
		     int(xbins[xbins.size()-1]*100),
		     int(minY*100),int(maxY*100)));
  TH1D *h = (TH1D*)gROOT->FindObject(hname);
  if(h==nullptr) {
    h = new TH1D(hname,"",xbins.size()-1,&xbins[0]);
  }
  h->SetMinimum(minY); h->SetMaximum(maxY);
  h->GetXaxis()->SetTitleOffset(1.16);
  h->GetYaxis()->SetTitleOffset(1.35);

  if(hname.Contains("N_j")||hname.Contains("Njet")||hname.Contains("njet"))
    h->GetXaxis()->SetLabelSize(0.07);
    
  h->GetYaxis()->SetNdivisions(508);
  if(ratio){
    h->GetXaxis()->SetTitleOffset(20);
    h->GetXaxis()->SetLabelOffset(20);
  }
  if (logx) {
    h->GetXaxis()->SetTitleOffset(1.25);
    h->GetXaxis()->SetMoreLogLabels();
  }
  if( ytit.Contains("ratio") )
    h->GetYaxis()->SetNdivisions(504,6);
  if( ytit.Contains("purity") )
    h->GetYaxis()->SetNdivisions(506,6);

  if(xtit=="NONE") h->GetXaxis()->SetLabelOffset(20);
  //else if(xtit=="hist") SetXTitle(h);
  else if(xtit=="XGB") SetXTitle(h);
  else h->GetXaxis()->SetTitle(xtit);

  if(ytit=="NONE") h->GetYaxis()->SetLabelOffset(20);
  else if(ytit=="hist") SetYTitle(h);
  else h->GetYaxis()->SetTitle(ytit);

  if(sameAxis){ h->Draw("sameaxis"); h->Draw("samehist"); }
  else h->Draw();
}

