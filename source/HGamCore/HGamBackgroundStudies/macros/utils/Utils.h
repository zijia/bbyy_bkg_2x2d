
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cmath>

#include <TLorentzVector.h>
#include <TROOT.h>
#include <TGaxis.h>
#include <TStyle.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLine.h>
#include <TSystem.h>
#include <TMath.h>
#include <TTree.h>
#include <TFile.h>
#include <TLatex.h>
#include <TCanvas.h>
#include <TGraphAsymmErrors.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TObjArray.h>
#include <TObjString.h>

using namespace std;
typedef TString Str;
typedef vector<double> VecD;
typedef vector<Str> StrV;

bool isAF2 = false;

struct Sideband2x2DResults{
  Str infile_name;
  StrV var_names, syst_names;
  vector<VecD> bins;
  vector<TH1D*> h_nomMC_ph1_Ieff, h_nomMC_ph2_Ieff;
  vector<TH1D*> h_nomMC_ph1_Teff, h_nomMC_ph2_Teff;
  vector<TH1D*> h_systMC_ph1_Ieff, h_systMC_ph2_Ieff;
  vector<TH1D*> h_systMC_ph1_Teff, h_systMC_ph2_Teff;
  map<Str,vector<TGraphAsymmErrors*> > gg_yield_systematicgraphs;
  map<Str,vector<TGraphAsymmErrors*> > gjjg_yield_systematicgraphs;
  map<Str,vector<TGraphAsymmErrors*> > jj_yield_systematicgraphs;
  map<Str,vector<TGraphAsymmErrors*> > gg_purity_systematicgraphs;
  map<Str,vector<TGraphAsymmErrors*> > gjjg_purity_systematicgraphs;
  map<Str,vector<TGraphAsymmErrors*> > jj_purity_systematicgraphs;

  vector<TGraphAsymmErrors*>
  
  gg_yield_graphs,
    gj_yield_graphs,
    jg_yield_graphs,
    gjjg_yield_graphs,
    jj_yield_graphs;

  vector<TGraphAsymmErrors*>
  gg_yield_systErr_graphs,
    gj_yield_systErr_graphs,
    jg_yield_systErr_graphs,
    gjjg_yield_systErr_graphs,
    jj_yield_systErr_graphs;

  vector<TGraphAsymmErrors*>
  gg_yield_statErr_graphs,
    gj_yield_statErr_graphs,
    jg_yield_statErr_graphs,
    gjjg_yield_statErr_graphs,
    jj_yield_statErr_graphs;

  vector<TGraphAsymmErrors*>
  gg_purity_graphs,
    gjjg_purity_graphs,
    jg_purity_graphs,
    gj_purity_graphs,
    jj_purity_graphs;

  vector<TGraphAsymmErrors*>
  gg_purity_statErr_graphs,
    gjjg_purity_statErr_graphs,
    jg_purity_statErr_graphs,
    gj_purity_statErr_graphs,
    jj_purity_statErr_graphs;

  vector<TGraphAsymmErrors*>
  gg_purity_systErr_graphs,
    gjjg_purity_systErr_graphs,
    gj_purity_systErr_graphs,
    jg_purity_systErr_graphs,
    jj_purity_systErr_graphs;

  
};


int GetIndex(Sideband2x2DResults results, Str variable){
  int index=-1;
  for(int i=0; i<results.var_names.size(); i++)
    if(results.var_names[i]==variable) index = i;
  if(index==-1){ cout<<"Cannot find valid index for variable: "<<variable<<endl; abort(); }
  return index;
}

void SaveGraph(TGraphAsymmErrors *g, Str name){
  g->SetName(name); g->SetTitle(name); g->Write();
}

void SaveGraphsToFile(Sideband2x2DResults results, Str outFileName)
{
  TFile *fout = new TFile(outFileName.ReplaceAll(".ps",".root"),"recreate");
  for(auto var:results.var_names){
    int index=GetIndex(results,var);
    Str variable_str = var;
    if(var=="mgg_bin2") variable_str = "mgg_5GeVbins";


    Str gg_nameBase = Form("gr_%s_gg_yield_",variable_str.Data());    
    SaveGraph(results.gg_yield_graphs[index],gg_nameBase+"TotErr");
    SaveGraph(results.gg_yield_statErr_graphs[index],gg_nameBase+"StatErr");
    SaveGraph(results.gg_yield_systErr_graphs[index],gg_nameBase+"SystErr");

    Str gjjg_nameBase = Form("gr_%s_gjjg_yield_",variable_str.Data());
    SaveGraph(results.gjjg_yield_graphs[index],gjjg_nameBase+"TotErr");
    SaveGraph(results.gjjg_yield_statErr_graphs[index],gjjg_nameBase+"StatErr");
    SaveGraph(results.gjjg_yield_systErr_graphs[index],gjjg_nameBase+"SystErr");
    Str jg_nameBase = Form("gr_%s_jg_yield_",variable_str.Data());
    SaveGraph(results.jg_yield_graphs[index],jg_nameBase+"TotErr");
    SaveGraph(results.jg_yield_statErr_graphs[index],jg_nameBase+"StatErr");
    SaveGraph(results.jg_yield_systErr_graphs[index],jg_nameBase+"SystErr");
    Str gj_nameBase = Form("gr_%s_gj_yield_",variable_str.Data());
    SaveGraph(results.gj_yield_graphs[index],gj_nameBase+"TotErr");
    SaveGraph(results.gj_yield_statErr_graphs[index],gj_nameBase+"StatErr");
    SaveGraph(results.gj_yield_systErr_graphs[index],gj_nameBase+"SystErr");    
    Str jj_nameBase = Form("gr_%s_jj_yield_",variable_str.Data());
    SaveGraph(results.jj_yield_graphs[index],jj_nameBase+"TotErr");
    SaveGraph(results.jj_yield_statErr_graphs[index],jj_nameBase+"StatErr");
    SaveGraph(results.jj_yield_systErr_graphs[index],jj_nameBase+"SystErr");

    gg_nameBase.ReplaceAll("yield","purity");
    gjjg_nameBase.ReplaceAll("yield","purity");
    jj_nameBase.ReplaceAll("yield","purity");    
    SaveGraph(results.gg_purity_graphs[index],gg_nameBase+"TotErr");
    SaveGraph(results.gg_purity_statErr_graphs[index],gg_nameBase+"StatErr");
    SaveGraph(results.gg_purity_systErr_graphs[index],gg_nameBase+"SystErr");
    SaveGraph(results.gjjg_purity_graphs[index],gjjg_nameBase+"TotErr");
    SaveGraph(results.gjjg_purity_statErr_graphs[index],gjjg_nameBase+"StatErr");
    SaveGraph(results.gjjg_purity_systErr_graphs[index],gjjg_nameBase+"SystErr");
    
    /* SaveGraph(results.gj_purity_graphs[index],gj_nameBase+"TotErr"); */
    /* SaveGraph(results.gj_purity_statErr_graphs[index],gj_nameBase+"StatErr"); */
    /* SaveGraph(results.gj_purity_systErr_graphs[index],gj_nameBase+"SystErr"); */
    
    /* SaveGraph(results.jg_purity_graphs[index],jg_nameBase+"TotErr"); */
    /* SaveGraph(results.jg_purity_statErr_graphs[index],jg_nameBase+"StatErr"); */
    /* SaveGraph(results.jg_purity_systErr_graphs[index],jg_nameBase+"SystErr"); */

    
    SaveGraph(results.jj_purity_graphs[index],jj_nameBase+"TotErr");
    SaveGraph(results.jj_purity_statErr_graphs[index],jj_nameBase+"StatErr");
    SaveGraph(results.jj_purity_systErr_graphs[index],jj_nameBase+"SystErr");
  }
  fout->Close();
}


StrV Vectorize(Str str, Str sep=" ") {
  StrV result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    if (os->GetString()[0]!='#') result.push_back(os->GetString());
    else break;
  return result;
}

VecD VectorizeD(Str str) {
  VecD result; StrV vecS = Vectorize(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}

void GetFileInfo(ifstream &f, Str name, map<Str,float> &nom, map<Str,float> &stat, map<Str,float> &sysUp, map<Str,float> &sysDn ){

  StrV columns = Vectorize("WGG WJG WGJ WGJJG WJJ PURITYJG PURITYJJ PURITY");
  
  while( !f.eof() ) {
    string STRING="";
    getline(f,STRING);
    size_t found = STRING.find(name);
    if(found==string::npos) continue;
    int strL = STRING.size();
    if(strL<1) continue;
    for( int ci=0; ci<columns.size(); ci++ ){
      Str var_str =  columns[ci]+"TITI= ";
      size_t foundit = STRING.find(var_str);
      if(foundit==string::npos) continue;
      unsigned pos = STRING.find("=");
      string sub_STRING = STRING.substr ( pos+2 );
      Str sub_tSTRING = (Str)sub_STRING;
      sub_tSTRING.ReplaceAll(" +/- "," +/-");
      StrV vals_str = Vectorize(sub_tSTRING);
      float val = (float)atof(vals_str[0]);
      nom[ columns[ci] ] = val;
      vals_str[1].ReplaceAll("+/-","");
      float valstat = (float)atof(vals_str[1]);
      stat[ columns[ci] ] = valstat;
      float valsysup=-99, valsysdn=-99;
      if(vals_str.size()>2){
	vals_str[3].ReplaceAll("e+","X");
	vals_str[2].ReplaceAll("+","");
	vals_str[3].ReplaceAll("X","e+");
	valsysup = (float)atof(vals_str[2]);
	sysUp[ columns[ci] ] = valsysup;
      }      
      if(vals_str.size()>3){
	vals_str[3].ReplaceAll("e-","X");
	vals_str[3].ReplaceAll("-","");
	vals_str[3].ReplaceAll("X","e-");
	valsysdn = (float)atof(vals_str[3]);
	sysDn[ columns[ci] ] = valsysdn;
      }
    }
  }
}


TH1D* GetHisto(TFile* f, TString hname)
{
  TH1D* h = (TH1D*)f->Get(hname);
  if(h==NULL) { cout<<"Can't find histogram: "<<hname<<" in file :: "<<f->GetName()<<endl; abort(); }
  return h;
}

TGraphAsymmErrors* GetGraph(TFile &f, TString gname)
{
  TGraphAsymmErrors* g = (TGraphAsymmErrors*)f.Get(gname);
  if(g==NULL) { cout<<"Can't find graph: "<<gname<<" in file: "<< f.GetName()<<endl; abort(); }
  return g;
}

TH1D *GetRatio(TH1D *H1, TH1D *H2)
{
  static int index = 0;
  int Nbins = H1->GetNbinsX();
  vector<double> BINS;
  for( int ibin=1; ibin<=Nbins+1; ibin++ ) BINS.push_back( H1->GetBinLowEdge(ibin) );
  TH1D *hRatio = hRatio = new TH1D(Form("hRatio_%d",index++),"",BINS.size()-1,&BINS[0]);
  for(int ibin=0; ibin<Nbins; ibin++){
    double ratio = -99.0, ratioErr = 0.0;  
    if(H2->GetBinContent(ibin+1)!=0){
      ratio    = H1->GetBinContent(ibin+1)/H2->GetBinContent(ibin+1);
      ratioErr = sqrt( pow(H1->GetBinError(ibin+1)/H2->GetBinContent(ibin+1),2.0)  +  
		       pow( H1->GetBinContent(ibin+1)*H2->GetBinError(ibin+1)/pow(H2->GetBinContent(ibin+1),2),2) );
 
    }
    hRatio->SetBinContent(ibin+1,ratio);
    hRatio->SetBinError(ibin+1,ratioErr);
  }
  return hRatio;
}


TGraphAsymmErrors *GetRatioGraph (TGraphAsymmErrors *g1,  TGraphAsymmErrors *g2)
{
  static int index = 0;
  int Npoints = g1->GetN();
  TGraphAsymmErrors *gRatio = new TGraphAsymmErrors();
  for( int i=0; i<Npoints; i++ ){ 
    if(g2->GetY()[i]){
      gRatio->SetPoint(i, g1->GetX()[i], g1->GetY()[i]/g2->GetY()[i] );  
      double  ratioErrUp = sqrt( pow(g1->GetErrorYhigh(i)/g2->GetY()[i],2.0) +
				 pow( g1->GetY()[i]*g2->GetErrorYhigh(i)/pow(g2->GetY()[i],2),2) );
      gRatio->SetPointEYhigh( i, ratioErrUp );
      double  ratioErrDn = sqrt( pow(g1->GetErrorYlow(i)/g2->GetY()[i],2.0) +
				 pow( g1->GetY()[i]*g2->GetErrorYlow(i)/pow(g2->GetY()[i],2),2) );
      gRatio->SetPointEYlow( i, ratioErrDn );
    }

  }
  return gRatio;
}


void AddGraph(TGraphAsymmErrors *g, TGraphAsymmErrors *g2, bool hstack = false)
{
  int np = g->GetN();
  for(int i=0;i<np;i++) {
    double Yh1 = g->GetErrorYhigh(i);
    double Yh2 = g2->GetErrorYhigh(i);
    double Yl1 = g->GetErrorYlow(i);
    double Yl2 = g2->GetErrorYlow(i);
    double Yh, Yl;
    if(hstack){
      Yh = Yh1;
      Yl = Yl1;
    }else{
      Yh  = TMath::Sqrt(Yh1*Yh1+Yh2*Yh2);
      Yl  = TMath::Sqrt(Yl1*Yl1+Yl2*Yl2);
    }
    g->SetPoint(i,g->GetX()[i],g->GetY()[i]+g2->GetY()[i]);
    g->SetPointEYhigh(i,Yh);
    g->SetPointEYlow(i,Yl);
  }
}

void ScaleGraph(TGraphAsymmErrors *g, double sf)
{
  for( int i=0; i<g->GetN(); i++ ){    
    g->SetPoint(i,g->GetX()[i], g->GetY()[i]*sf);
    g->SetPointEYhigh(i, g->GetEYhigh()[i]*sf);
    g->SetPointEYlow(i, g->GetEYlow()[i]*sf);
  }
}

void RemoveXerrors(TGraphAsymmErrors *g)
{
  for( int i=0; i<g->GetN(); i++ ){    
    g->SetPointEXhigh(i, 0.0);
    g->SetPointEXlow(i, 0.0);
  }
}


void AverageOverBins(TGraphAsymmErrors *g, int nAverage = 1){

  int np = g->GetN();
  for(int i=0;i<np;i++) { // 0 to nbins-1
    double Y       = g->GetY()[i];
    double Yaverage  = 0;
    double Ylaverage = 0;
    double Yhaverage = 0;
    int    Naverage  = 0;
    for(int iw=i-nAverage;iw<=i+nAverage;iw++) {
      if(iw<0)  continue;
      if(iw>=np) continue;
      Naverage++;
      Yaverage  += g->GetY()[i];
      Yhaverage += g->GetEYhigh()[iw];
      Ylaverage += g->GetEYlow()[iw];
    }
    if(Naverage>0) {
      Yaverage  /= Naverage;
      Yhaverage /= Naverage;
      Ylaverage /= Naverage;
    }
    
    double Yh,Yl;
    if(Yaverage>0) {
      double w  = Y/Yaverage;
      Yh = w*Yhaverage;
	Yl = w*Ylaverage;
    } else {
	Yh = g->GetEYhigh()[i];
	Yl = g->GetEYlow()[i];
    }
    
    g->SetPointEYhigh(i,Yh);
    g->SetPointEYlow(i,Yl);
  }
  
}

void ShiftGraphX(TGraphAsymmErrors *g, float dx){
  for(int i=0; i<g->GetN(); i++)
    g->SetPoint(i,g->GetX()[i]+dx,g->GetY()[i]);
}

////////////////////////////////////////////////////
/////////////////////////////// error correlations
////////////////////////////////////////////////////

double getTotUncQuad(std::vector<TH1D*> sysVar, int bin) {
  double nom=sysVar[0]->GetBinContent(bin), V=0;
  for (int i=1;i<sysVar.size();++i)
    V+=pow((sysVar[i]->GetBinContent(bin)-nom)/nom,2);    
  return sqrt(V);
}

// correaltions... code snipit from Dag 
TH2D *getCorrelationMatrix(vector<TH1D*> regions_syst, StrV region_labels){
  int Nsys = regions_syst.size();
  int Nreg = regions_syst[0]->GetNbinsX();
  TH2D *corr = new TH2D("","",Nreg,0,Nreg,Nreg,0,Nreg);
  Nreg++;
  for (int i=1;i<Nreg;++i) {    
    corr->GetXaxis()->SetBinLabel(i,region_labels[i-1].ReplaceAll("\\","#"));
    corr->GetYaxis()->SetBinLabel(i,region_labels[i-1].ReplaceAll("\\","#"));
    double Ui = getTotUncQuad(regions_syst,i), nomi=regions_syst[0]->GetBinContent(i);
    for (int j=1;j<Nreg;++j) {
      double Uj = getTotUncQuad(regions_syst,j), nomj=regions_syst[0]->GetBinContent(j);
      double COV=0;
      for (int s=1;s<Nsys;++s)
	COV += (regions_syst[s]->GetBinContent(i)-nomi)/nomi *
	  (regions_syst[s]->GetBinContent(j)-nomj)/nomj;
      corr->SetBinContent(i,j,COV/Ui/Uj*100);
      //corr->SetBinContent(i,j,COV*100);
      //printf("nom(i)=%.6f, nom(j)=%.6f, Ui=%.6f, Uj=%.6f, COV=%.6f, CORR=%.6f, Ui*Uj=%.6f \n\n",nomi,nomj,Ui,Uj,COV,COV/Ui/Uj*100,Ui*Uj);
    }
  }
  return corr;
}


TH1D *ConvertGraphToUncertHist(TGraphAsymmErrors* g, VecD xbins, bool addErr = false){
  static int index = 0;
  int nbins = xbins.size();
  TH1D * h = new TH1D(Form("hist_corr_%d",index++),"",nbins-1,&xbins[0]);  
  for(int i=0; i<g->GetN(); i++){
    double val = g->GetY()[i];
    if(addErr) val+=g->GetEYhigh()[i]==0?-1.0*g->GetEYlow()[i]:g->GetEYhigh()[i];
    h->SetBinContent(i+1,val);
  }
  return h;
}
