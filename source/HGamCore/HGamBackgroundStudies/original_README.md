## HGamBackgroundStudies

### This package is used to obtain the background decomposition of the diphoton invariant mass spectrum from the 2x2D sideband method

For a fairly detailed description of the method and the equations used the fit see: https://cds.cern.ch/record/1450063/files/ATL-COM-PHYS-2012-592.pdf
Note: There is also a package that is used for the high mass analysis, here: PhysicsAnalysis/HiggsPhys/Run2/HGamma/xAOD/Purity2x2DSB
This hasn't been looked at at all by Jim (previous author) or Jahred (new one)

The code is being ported by Jahred to Rel 21 and GIT, but is a work in progress.
The code needs a *LOT* of cleanup

#### Step1: Compiling code to make mini-Ntuples used as input to the 2x2D method :: HGam2x2DInputMaker
HGam2x2DInputMaker runs within the HGamAnalysisFramework, taking MxAODs as input and producing a reduced ntuple.
Therefore before running HGamBackgroundStudies one must first setup AnalysisBase and the HGamAnalysisFramework

For all the details on HGamAnalysisFramework see: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HggDerivationAnalysisFramework
Note that this package is now updated to run on rel21, and is a work in progress. You want to follow the CMake + git version of the instructions.
You can stop just before "Run the analysis" though of course feel free to test your compilation by running the example

If you follow the TWiki pages, you can add this package just after running *git clone --recursive...*:

*cd HGamCore*

*git clone https://:@gitlab.cern.ch:8443/atlas-hgam-sw/HGamBackgroundStudies.git*

*cd -*

Alternatively, if you prefer ssh:

*cd HGamCore*

*git clone ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamBackgroundStudies.git*

*cd -*

#### Step 2a: After compilation from the TWiki, make 2x2d input ntuples by running the following for data:
*runHGam2x2DInputMaker BaseConfig: $TestArea/HGamCore/HGamAnalysisFramework/data/HGamRel21.config OutputDir: ./output InputFile: inputfileDataFile.root*

#### Step 2b: Or for MC (and change MC16d if you run on, for example, MC16a):
*runHGam2x2DInputMaker BaseConfig: $TestArea/HGamCore/HGamAnalysisFramework/data/HGamRel21.config $TestArea/HGamCore/HGamAnalysisFramework/data/MCSamples.config OutputDir: ./output InputFile: inputfileMCFile.root MonteCarloType: MC16d*

Your output file will be in (for example) _./output/data-MxAOD/sideband2x2d_input.root_

#### Step 3: Calculate event yields:
First modify _data/input.cfg_ to point to all your files, and then calculate the event yields, purities and systematic uncertainties by running:

*run2x2DSBBkgDecomp*

Note that other cfg files in that area change binning, etc. You might reduce the number of variables to run over, as this will save quite a bit of time!

#### Step 4: Finally, plot the results!
In HGamBackgroundStudies/macros, see the script: _PlotPurity.C_

For now, annoyingly, variables and filenames are hardcoded in the main function at the bottom of the script (this is not ideal, and to be fixed, too). 

We compile the standalone code and run it:

*source compile.sh*

### running it alone picks up the default input file. Otherwise you can pass it as the first argument
*./plot_2x2D_results* ### default input file

*./plot_2x2D_results MyInputFile.root*

