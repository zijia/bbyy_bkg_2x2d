#ifndef HGamBackgroundStudies_Includes_H
#define HGamBackgroundStudies_Includes_H

// STL includes
#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>
#include <vector>
#include <string>
#include <map>
#include <time.h>

#include <TString.h>
#include <TEnv.h>
#include <THashList.h>

#include <TROOT.h> 
#include <TSystem.h> 
#include <TClass.h> 
#include <TMath.h>

#include <TFile.h>
#include <TTree.h>
#include <TChain.h>

#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>

#include <TH1D.h>
#include <TH1.h>

#include <TLorentzVector.h>
#include <TLatex.h>

#include "PathResolver/PathResolver.h"

typedef TString Str;
typedef unsigned int uint;
typedef std::vector<Str> StrV;
typedef std::vector<std::string> StdStrV;
typedef std::vector<int> VecI;
typedef std::vector<float> VecF;
typedef std::vector<double> VecD; 
typedef std::vector<double>  NumV;
 
#endif
