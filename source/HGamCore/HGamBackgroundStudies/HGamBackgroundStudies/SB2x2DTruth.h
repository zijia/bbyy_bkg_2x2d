#ifndef HGamBackgroundStudies_SB2x2DTruth_h
#define HGamBackgroundStudies_SB2x2DTruth_h

#include <vector>
#include <iostream>
#include <TROOT.h>
#include <TClass.h>
#include <TH1D.h>
#include <TMath.h>

#include "HGamBackgroundStudies/SB2x2D.h"

using namespace std;

namespace SB {
  
  class SB2x2D;
  
  class SB2x2DTruth : public TNamed {
  public:
    SB2x2DTruth  ();
    SB2x2DTruth  (TString name, TString title, std::vector<int*>& flags, Int_t nbinsx, double* xbins);
    ~SB2x2DTruth ();
    
    // GetHist from: 
    //   - gg, gj, jg, jj -> LL or TITI 
    //   - signal fakerate -> 1 or 2 
    // The string search is case insensitive
    TH1D *GetHistogram(TString hname);
    virtual double GetEntries() const;
    virtual void  Print(Option_t* option = "Signal,BF,Total") const;
    void  Fill(double x=0, double weight=1);
    
    // To get the values for the 2x2DSideBandMethod
    // Get parameter i on full stat
    double  GetTruthValue(int i) const;       
    // Get parameter i in one bin
    double  GetTruthValueByBin(int i, int ibin) const; 
    // Get parameter i sigma^2 from data statistic
    double  GetTruthError2(int i) const; 
    // Get parameter i histo with value and sigma per bin
    TH1D*   GetTruthHist(int i) const; 
    
    inline SB2x2D *SideBand() { return fSignal; }
    
    enum { Quality1Signal1=0,  Quality2Signal1,
	   Quality1Signal2,    Quality2Signal2,
	   IsTrueSignal,
	   NbPointers
    };

      const char* ParamName[NPARAMS]={ "WGG", "WGJ", "WJG", "WJJ", "WGJJG", "PURITY", "ALPHA", "WGGII", "WGJII", "WJGII", "WJJII", "WGJJGII", "PURITYII", "ALPHAII", "WGGTITI", "WGJTITI", "WJGTITI", "WJJTITI", "WGJJGTITI", "PURITYJGTITI", "PURITYJJTITI", "PURITYTITI", "ALPHATITI", "E1I", "E2I", "E1T", "E2T", "EP1I", "EP2I", "EP1T", "EP2T", "F1I", "F2I", "F1T", "F2T", "FP1I", "FP2I", "FP1T", "FP2T", "X1G", "X2G" ,"X1J", "X2J", "XIGG", "XIGJ", "XIJG", "XIJJ", "XTGG", "XTGJ", "XTJG", "XTJJ"};

      
  protected:
    TString MakeName(TString gname, TString hname);
    TString MakeTitle(TString gtitle, TString htitle);
    
    SB2x2D *fSignal;  //
    Int_t *fIsTruth; //! Pointer to is signal or background branch
    
  };

}
#endif
  
