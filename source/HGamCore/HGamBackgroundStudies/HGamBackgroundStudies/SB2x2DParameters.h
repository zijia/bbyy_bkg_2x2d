#ifndef HGamBackgroundStudies_SB2x2DParams_h
#define HGamBackgroundStudies_SB2x2DParams_h

#include <iostream>
#include <TROOT.h>
#include <TClass.h>
#include <TMath.h>


namespace SB {

  class SB2x2DParameters : public TObject {
  
  public:
    
    SB2x2DParameters();
    ~SB2x2DParameters();
    
    void SetDefaults();
    void SetValues(const SB2x2DParameters& par);
    void SimplifyIsolationInputs();
    void SimplifyEfficiencyInputs();
    void Simplest();
    void SetValues(const double* par);
    void SetErrors(const double* par);

    inline double GetValue(int i) const     { return p[i]; }
    inline double GetError2(int i) const    { return w2p[i]; }
    inline double GetError(int i) const     { return TMath::Sqrt(w2p[i]); }
    inline double GetErrorHigh(int i) const { return errup[i]; }
    inline double GetErrorLow(int i) const  { return errlo[i]; }
    inline bool HasAsymmetricErrors(int i) const { if(errup[i]>0) return true; return false; }
    
    inline void SetValue(int i, double x)     { p[i] = x; }
    inline void SetError2(int i, double x)    { w2p[i] = x; }
    inline void SetError(int i, double x)     { w2p[i] = x*x; }
    inline void SetErrorHigh(int i, double x) { errup[i] = x; }
    inline void SetErrorLow(int i, double x)  { errlo[i] = x; }
    inline double*GetAddress(int i)           { return &(p[i]); }
    

  private:
      
    enum { WGG, WGJ, WJG, WJJ, WGJJG, PURITY, ALPHA, 
	   WGGII, WGJII, WJGII, WJJII, WGJJGII, PURITYII, ALPHAII, 
	   WGGTITI, WGJTITI, WJGTITI, WJJTITI, WGJJGTITI, PURITYJGTITI, PURITYJJTITI, PURITYTITI, ALPHATITI, 
	   E1I, E2I, E1T, E2T, 
	   EP1I, EP2I, EP1T, EP2T, 
	   F1I, F2I, F1T, F2T, 
	   FP1I, FP2I, FP1T, FP2T, 
	   X1G, X2G ,X1J, X2J, 
	   XIGG, XIGJ, XIJG, XIJJ, 
	   XTGG, XTGJ, XTJG, XTJJ, 
	   NPARAMS};
    
    double p[NPARAMS]; // parameters
    double w2p[NPARAMS]; // parameters error squared
    
    double errup[NPARAMS]; // output parameters error up
    double errlo[NPARAMS]; // output parameters error low
    
  };
}

#endif
