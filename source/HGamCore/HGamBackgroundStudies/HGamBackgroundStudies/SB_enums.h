#ifndef HGamBackgroundStudies_SB_enums_h
#define HGamBackgroundStudies_SB_enums_h

namespace SB {

  
  enum { A=0, B, C, D, NBOXES};
  static const char* BoxName[NBOXES]={"A", "B", "C", "D" };
  
  enum { WGG, WGJ, WJG, WJJ, WGJJG, PURITY, ALPHA, 
	 WGGII, WGJII, WJGII, WJJII, WGJJGII, PURITYII, ALPHAII, 
	 WGGTITI, WGJTITI, WJGTITI, WJJTITI, WGJJGTITI, PURITYGJTITI,PURITYJJTITI, PURITYTITI, ALPHATITI, 
	 E1I, E2I, E1T, E2T, 
	 EP1I, EP2I, EP1T, EP2T, 
	 F1I, F2I, F1T, F2T, 
	 FP1I, FP2I, FP1T, FP2T, 
	 X1G, X2G ,X1J, X2J, 
	 XIGG, XIGJ, XIJG, XIJJ, 
	 XTGG, XTGJ, XTJG, XTJJ, 
	 NPARAMS};

}

#endif
