#ifndef HGamBackgroundStudies_SB2x2DSolver_h
#define HGamBackgroundStudies_SB2x2DSolver_h

#include <iostream>
#include <TROOT.h>
#include <TClass.h>

using namespace std;

#include "HGamBackgroundStudies/SB2x2D.h"

namespace SB {

  class SB2x2D;

class SB2x2DSolver;
typedef double (SB2x2DSolver::*FMETHOD) ();

class SB2x2DSolver : public TObject {
 public:
  SB2x2DSolver  ();
  ~SB2x2DSolver ();

  // To fill inputs by hand or using a T2x2DSideBand class
  void Fill(SB2x2D *t, int ibin, int ifixed); 
  void Fill(SB2x2D *t, int ibin1, int ibin2, int ibinIgnore); 
  void SetValues(SB2x2D*t, int ifixed=-1);
  //inline void SetParametersPointer(double *ptr) { p.SetParametersPointer(ptr); }

  double GetNTotal();
  double GetNTotalError2();
  double GetNTotalChi2();
  double GetNPrediction(int i, int j);
  double GetNChi2(int i, int j);
  double GetChi2();

  double GetValue(int i);
  double GetError2(int i);
  double GetTruthValue(int i);
  double GetTruthError2(int i);

  inline void SetParameters(const double *par) { p.SetValues(par); }

  const char* ParamName[NPARAMS]={ "WGG", "WGJ", "WJG", "WJJ", "WGJJG", "PURITY", "ALPHA", "WGGII", "WGJII", "WJGII", "WJJII", "WGJJGII", "PURITYII", "ALPHAII", "WGGTITI", "WGJTITI", "WJGTITI", "WJJTITI", "WGJJGTITI", "PURITYJGTITI", "PURITYJJTITI", "PURITYTITI", "ALPHATITI", "E1I", "E2I", "E1T", "E2T", "EP1I", "EP2I", "EP1T", "EP2T", "F1I", "F2I", "F1T", "F2T", "FP1I", "FP2I", "FP1T", "FP2T", "X1G", "X2G" ,"X1J", "X2J", "XIGG", "XIGJ", "XIJG", "XIJJ", "XTGG", "XTGJ", "XTJG", "XTJJ"};

  
  enum { ISOLATION=0, SIMPLEST=1, SIMPLIFIED=2, FIT=3, NOTHING=4}; // Input type either eI, fI data driven or MC efficiencies
  
  
#define nAA n[A][A]
#define nAB n[A][B]
#define nAC n[A][C]
#define nAD n[A][D]
#define nBA n[B][A]
#define nBB n[B][B]
#define nBC n[B][C]
#define nBD n[B][D]
#define nCA n[C][A]
#define nCB n[C][B]
#define nCC n[C][C]
#define nCD n[C][D]
#define nDA n[D][A]
#define nDB n[D][B]
#define nDC n[D][C]
#define nDD n[D][D]

#define w2nAA w2n[A][A]
#define w2nAB w2n[A][B]
#define w2nAC w2n[A][C]
#define w2nAD w2n[A][D]
#define w2nBA w2n[B][A]
#define w2nBB w2n[B][B]
#define w2nBC w2n[B][C]
#define w2nBD w2n[B][D]
#define w2nCA w2n[C][A]
#define w2nCB w2n[C][B]
#define w2nCC w2n[C][C]
#define w2nCD w2n[C][D]
#define w2nDA w2n[D][A]
#define w2nDB w2n[D][B]
#define w2nDC w2n[D][C]
#define w2nDD w2n[D][D]


#define WggTITI p.GetValue(WGGTITI)
#define WgjTITI p.GetValue(WGJTITI)
#define WjgTITI p.GetValue(WJGTITI)
#define WjjTITI p.GetValue(WJJTITI)
#define e1I     p.GetValue(E1I)
#define e2I     p.GetValue(E2I)
#define e1T     p.GetValue(E1T)
#define e2T     p.GetValue(E2T)
#define ep1I    p.GetValue(EP1I)
#define ep2I    p.GetValue(EP2I)
#define ep1T    p.GetValue(EP1T)
#define ep2T    p.GetValue(EP2T)
#define f1I     p.GetValue(F1I)
#define f2I     p.GetValue(F2I)
#define f1T     p.GetValue(F1T)
#define f2T     p.GetValue(F2T)
#define fp1I    p.GetValue(FP1I)
#define fp2I    p.GetValue(FP2I)
#define fp1T    p.GetValue(FP1T)
#define fp2T    p.GetValue(FP2T)
#define x1g     p.GetValue(X1G)
#define x2g     p.GetValue(X2G)
#define x1j     p.GetValue(X1J)
#define x2j     p.GetValue(X2J)
#define xIgg    p.GetValue(XIGG)
#define xIgj    p.GetValue(XIGJ)
#define xIjg    p.GetValue(XIJG)
#define xIjj    p.GetValue(XIJJ)
#define xTgg    p.GetValue(XTGG)
#define xTgj    p.GetValue(XTGJ)
#define xTjg    p.GetValue(XTJG)
#define xTjj    p.GetValue(XTJJ)


 // isolation input
 double GetEfficiency1T_IsolationInput();
 double GetFakeRate1T_IsolationInput();
 double GetEfficiency2T_IsolationInput();
 double GetFakeRate2T_IsolationInput();
 double GetEfficiency1TError2_IsolationInput();
 double GetEfficiency2TError2_IsolationInput();
 double GetFakeRate1TError2_IsolationInput();
 double GetFakeRate2TError2_IsolationInput();


 
#define n1T (nAA + nAB + nAC + nAD + nBA + nBB + nBC + nBD) // Candidate 1 is Tight
#define n1L (nCA + nCB + nCC + nCD + nDA + nDB + nDC + nDD) // Candidate 1 is non-Tight but Loose
#define n2T (nAA + nAB + nBA + nBB + nCA + nCB + nDA + nDB) // Candidate 2 is Tight
#define n2L (nAC + nAD + nBC + nBD + nCC + nCD + nDC + nDD) // Candidate 2 is non-Tight but Loose
#define n1I (nAA + nAB + nAC + nAD + nCA + nCB + nCC + nCD) // Candidate 1 is Isolated
#define n1N (nBA + nBB + nBC + nBD + nDA + nDB + nDC + nDD) // Candidate 1 is non-Isolated
#define n2I (nAA + nAC + nBA + nBC + nCA + nCC + nDA + nDC) // Candidate 2 is Isolated
#define n2N (nAB + nAD + nBB + nBD + nCB + nCD + nDB + nDD) // Candidate 2 is non-Isolated
#define nXIXI (nAA + nAC + nCA + nCC) // both candidates isolated
#define nXIXN (nAB + nAD + nCB + nCD) // leading candidate isolated subleading not isolated
#define nXNXI (nBA + nBC + nDA + nDC) // leading candidate not isolated subleading isolated
#define nXNXN (nBB + nBD + nDB + nDD) // both candidates not isolated
#define nTXTX (nAA + nAB + nBA + nBB)
#define nTXNX (nAC + nAD + nBC + nBD)
#define nNXTX (nCA + nCB + nDA + nDB)
#define nNXNX (nCC + nCD + nDC + nDD)
#define nXITI (nAA + nCA)
#define nXNTI (nBA + nDA)
#define nTIXI (nAA + nAC)
#define nTIXN (nAB + nAD)
#define nNXTI (nCA + nDA)
#define nTXTI (nAA + nBA)
#define nTINX (nAC + nAD)
#define nTITX (nAA + nAB)
#define n1TI (nAA + nAB + nAC + nAD)
#define n1TN (nBA + nBB + nBC + nBD)
#define n1NI (nCA + nCB + nCC + nCD)
#define n1NN (nDA + nDB + nDC + nDD)
#define n2TI (nAA + nBA + nCA + nDA)
#define n2TN (nAB + nBB + nCB + nDB)
#define n2NI (nAC + nBC + nCC + nDC)
#define n2NN (nAD + nBD + nCD + nDD)



 
 // truth
 double GetEfficiency1IX1XI_Truth();
 double GetEfficiency2IX2XI_Truth();
 double GetEfficiency1TX1XT_Truth();
 double GetEfficiency2TX2XT_Truth();
 double GetEfficiency1IXI_Truth();
 double GetEfficiency2IXI_Truth();
 double GetEfficiency1TXT_Truth();
 double GetEfficiency2TXT_Truth();
 double GetEfficiency1I_Truth();
 double GetEfficiency2I_Truth();
 double GetCorrelationI_Truth();
 double GetCorrelation1_Truth();
 double GetCorrelation2_Truth();
 double GetCorrelationT_Truth();
 double GetEfficiency1T_Truth();
 double GetEfficiency2T_Truth();
 double GetEfficiency1IXIError2_Truth();
 double GetEfficiency2IXIError2_Truth();
 double GetEfficiency1TXTError2_Truth();
 double GetEfficiency2TXTError2_Truth();
 double GetEfficiency1IX1XIError2_Truth();
 double GetEfficiency2IX2XIError2_Truth();
 double GetEfficiency1TX2XTError2_Truth();
 double GetEfficiency2TX2XTError2_Truth();
 double GetEfficiency1IError2_Truth();
 double GetEfficiency2IError2_Truth();
 double GetEfficiency1TError2_Truth();
 double GetEfficiency2TError2_Truth();
 double GetCorrelationIError2_Truth();
 double GetCorrelation1Error2_Truth();
 double GetCorrelation2Error2_Truth();
 double GetCorrelationTError2_Truth();
 // simplified 
 double GetWgg();
 double GetWgj();
 double GetWjg();
 double GetWjj();
 double GetWgjjg();
 double GetAlpha();
 double GetPurity();
 double GetWggII();
 double GetWgjII();
 double GetWjgII();
 double GetWjjII();
 double GetWgjjgII();
 double GetAlphaII();
 double GetPurityII();
 double GetWggTITI();
 double GetWgjTITI();
 double GetWjgTITI();
 double GetWjjTITI();
 double GetWgjjgTITI();
 double GetAlphaTITI();
 double GetPurityTITI();
 double GetFakeRate1I_Simplified();
 double GetFakeRate2I_Simplified();
 double GetFakeRate1I_Simplest();
 double GetFakeRate2I_Simplest();
 double GetFakeRate1T_Simplified();
 double GetFakeRate2T_Simplified();
 double GetWggError2();
 double GetWgjError2();
 double GetWjgError2();
 double GetWjjError2();
 double GetWgjjgError2();
 double GetAlphaError2();
 double GetPurityError2();
 double GetWggIIError2();
 double GetWgjIIError2();
 double GetWjgIIError2();
 double GetWjjIIError2();
 double GetWgjjgIIError2();
 double GetAlphaIIError2();
 double GetPurityIIError2();
 double GetWggTITIError2();
 double GetWgjTITIError2();
 double GetWjgTITIError2();
 double GetWjjTITIError2();
 double GetWgjjgTITIError2();
 double GetAlphaTITIError2();
 double GetPurityTITIError2();
 double GetFakeRate1TError2_Simplified();
 double GetFakeRate2TError2_Simplified();
 double GetFakeRate1IError2_Simplified();
 double GetFakeRate2IError2_Simplified();
 double GetFakeRate1IError2_Simplest();
 double GetFakeRate2IError2_Simplest();
 // predictions
 double GetNTotalPrediction();
 double GetNAAPrediction();
 double GetNABPrediction();
 double GetNACPrediction();
 double GetNADPrediction();
 double GetNBAPrediction();
 double GetNBBPrediction();
 double GetNBCPrediction();
 double GetNBDPrediction();
 double GetNCAPrediction();
 double GetNCBPrediction();
 double GetNCCPrediction();
 double GetNCDPrediction();
 double GetNDAPrediction();
 double GetNDBPrediction();
 double GetNDCPrediction();
 double GetNDDPrediction();
 




 
 
 protected:
  static const FMETHOD vtable[NPARAMS];
  static const FMETHOD etable[NPARAMS];
  static const FMETHOD vtruthtable[NPARAMS];
  static const FMETHOD etruthtable[NPARAMS];

  // To clear the inputs
  void Initialize();    // Clears all input

  // Inputs
  Int_t mInput;
  Double_t n[NBOXES][NBOXES];
  Double_t w2n[NBOXES][NBOXES];

  // Efficiencies, fake rates and correlations
  SB2x2DParameters p;

};

}
#endif
