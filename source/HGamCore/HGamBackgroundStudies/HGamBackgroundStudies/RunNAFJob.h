#ifndef HGamBackgroundStudies_RunNAFJob_H
#define HGamBackgroundStudies_RunNAFJob_H

#include "HGamBackgroundStudies/Includes.h"
#include "HGamAnalysisFramework/HgammaIncludes.h" 
#include "HGamAnalysisFramework/HgammaAnalysis.h"

#include "EventLoop/DirectDriver.h"
#include "EventLoop/Job.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "xAODRootAccess/Init.h"
#include <TSystem.h>
#include <iostream>
#include <string>

namespace HG {
  void subimtJobsNAF(HgammaAnalysis *alg, int argc, char** argv);
}

#endif
