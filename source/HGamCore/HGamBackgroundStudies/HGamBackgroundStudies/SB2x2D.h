#ifndef HGamBackgroundStudies_SB2x2D_h
#define HGamBackgroundStudies_SB2x2D_h

#include "HGamBackgroundStudies/Includes.h"
#include "Math/Minimizer.h"
#include "HGamBackgroundStudies/SB2x2DParameters.h"
#include "HGamBackgroundStudies/SB_enums.h"
#include "HGamBackgroundStudies/SB2x2DSolver.h"


using namespace std;

namespace SB {

class SB2x2DSolver;
class SB2x2D;

class SB2x2D : public TNamed {
 public:

  SB2x2D  ();
  SB2x2D  (TString name, TString title, std::vector<int*>& flags, Int_t nbinsx, double* xbins);
  ~SB2x2D ();
  void Fill(double x=0, double weight=1);
  void Solve(int ibin=-1);
  //virtual
  void	 Print(Option_t* option = "") const;

  inline SB2x2DParameters& GetCurrent() { return pcur; }
  inline SB2x2DParameters& GetOutput() { return pout; }
  // GetHistogram from: 
  //   - gg, gj, jg, jj -> LL or TITI 
  //   - signal fakerate -> 1 or 2 
  // The string search is case insensitive
  TH1D *GetHistogram(TString hname);
  inline void SetHistogram(int ip, TH1D *h) { hInput[ip] = h; }
  inline void SetBinGatheringMode(int mode) { mDoGathering = mode; }
  inline void SetBinGathering(vector<double>& v) { vGatherBins = v; }

  // Change input from MC leakage coefficients to data driven isolation efficiency and fake rates
  void  SetInputMode(Int_t mode);
  inline Int_t GetInputMode() const { return mInput; }

  double  GetNTot()  const;
  double  GetNTotError2()  const;
  inline double  GetN(int i, int j)  const { return GetHistContent(hN[i][j]); }
  inline double  GetNError2(int i, int j)   const { return GetHistError2(hN[i][j]); }
  inline TH1D*   GetNHist(int i, int j)  const { return hN[i][j]; }

  //virtual double GetEntries() const;
  double GetEntries() const;
  inline Int_t   GetNbinsX() const { return hN[0][0]->GetNbinsX(); };
  const TArrayD* GetXbins() const  { return hN[0][0]->GetXaxis()->GetXbins(); };

  // For chi2 minimization
  inline double  GetNTotPrediction()  const { return NTotPred; }
  inline double  GetNPrediction(int i, int j)  const { return NPred[i][j]; }
  double  GetNTotChi2() const;
  double  GetNChi2(int i, int j) const;
  double  GetPChi2(int i) const;
  double  GetExtraChi2() const;
  double  GetChi2() const; 
  void    SetExtraConstraints();
  void    Fit(int ibin = -1);
  void    SetStartingValues(int ibin, int ibin1, int ibin2);
  void    SetMinimizerParameters(ROOT::Math::Minimizer *min, double naa1, double naa2, int nd);
  void    GetMinosErrors(ROOT::Math::Minimizer *min);

  // To get the values for the 2x2DSideBandMethod
  inline double  GetOutputValue(int i)    const { return pout.GetValue(i); }   // Get parameter i
  inline double  GetOutputError2(int i)   const { return pout.GetError2(i); } // Get parameter i sigma^2
  double  GetComputedValue(int ibin, int i);
  TH1D*   GetHist(int i);   // Get parameter i histo with value and sigma per bin
  TGraphAsymmErrors *GetError(int i);

  // Get or set input parameters values
  inline void SetOutputValues(const double *par) { pout.SetValues(par); }
  inline void SetOutputErrors(const double *par) { pout.SetErrors(par); }
  void SetParameter(int i, double value);
  void SetParameterAndError2(int i, double value, double error2);
  void SetParameterError2(int i, double error2);

  // Systematics
  void InitSystematics(); 
  void AddSystematics(TString name, double xlow, double xup, double step);
  void AddSystematics(std::vector<SB2x2D *> v, int symm=0, bool border=false);
  void AddSystematics(std::vector<void *> v, int symm=0);
  void AddSystematics(std::vector<double> v, int symm=0);
  void EndSystematics();
  void PrintSystematics(TString opt, ofstream *out=0, TString tag ="" );
  void PrintLastSystematics(TString opt,ofstream *out=0, TString tag ="" );
  inline double LastPosSystematics(int i) { return vRangeMax[i]-vNominal[i]; };
  inline double LastNegSystematics(int i) { return vNominal[i]-vRangeMin[i]; };
  inline double GetNomSystematics (int i) { return vNominal[i]; };
  inline double LastPosSystematics(int i, int ibin) { return vRangeMaxByBin[i][ibin]-vNominalByBin[i][ibin]; };
  inline double LastNegSystematics(int i, int ibin) { return vNominalByBin[i][ibin]-vRangeMinByBin[i][ibin]; };
  inline double GetNomSystematics (int i, int ibin) { return vNominalByBin[i][ibin]; };

  inline double GetPosSystematics(int i)  { return vSystPos[i]; };
  inline double GetNegSystematics(int i)  { return vSystNeg[i]; };
  TGraphAsymmErrors* GetSystematics(int iYield, int nAverage, bool addStat);
  TGraphAsymmErrors* GetLastSystematics(int iYield);

  // static methods
  static double GetHistContent(TH1D *h);
  static double GetHistError2(TH1D *h);

  enum { Quality1Signal1=0,  Quality2Signal1, 
	 Quality1Signal2,    Quality2Signal2, 
         NbPointers
  };

  const char* ParamName[NPARAMS]={ "WGG", "WGJ", "WJG", "WJJ", "WGJJG", "PURITY", "ALPHA", "WGGII", "WGJII", "WJGII", "WJJII", "WGJJGII", "PURITYII", "ALPHAII", "WGGTITI", "WGJTITI", "WJGTITI", "WJJTITI", "WGJJGTITI", "PURITYJGTITI", "PURITYJJTITI", "PURITYTITI", "ALPHATITI", "E1I", "E2I", "E1T", "E2T", "EP1I", "EP2I", "EP1T", "EP2T", "F1I", "F2I", "F1T", "F2T", "FP1I", "FP2I", "FP1T", "FP2T", "X1G", "X2G" ,"X1J", "X2J", "XIGG", "XIGJ", "XIJG", "XIJJ", "XTGG", "XTGJ", "XTJG", "XTJJ"};
    
 protected:
  void Initialize();
  void SetExtraTITI();
  void GetSystRange (TString name, double xlow, double xup, double step);
  void GetSystRangeByBin(int ibin, TString name, double xlow, double xup, double step);
  void GetSystRange (std::vector<SB2x2D *> v);
  void GetSystRangeByBin(int ibin, std::vector<SB2x2D *> v);
  void GetSystRange (std::vector<double> v);
  void GetSystRangeByBin(int ibin, std::vector<double> v);
  void FillRange(int ifirst, int ibin=-1);
  void FillRange(SB2x2D *t, int ifirst, int ibin=-1);
  //////  void FillRange(std::vector<double> v, int ifirst, int ibin=-1);
  void FillRange(std::vector<double> v, int ifirst);
  TString MakeName(TString gname, TString hname);
  TString MakeTitle(TString gtitle, TString htitle);

  // For chi2 minimization
  void PrintExtraConstraints() const;
  void GatherBins(TH1D *h, int ibin, int& ibin1, int& ibin2);

  Int_t mInput;
  int   mNFree;
  SB2x2DParameters pin;
  SB2x2DParameters pcur;
  SB2x2DParameters pout;

  double NTotPred;
  TH1D *hInput[NPARAMS];
  double NPred[NBOXES][NBOXES];
  TH1D *hN[NBOXES][NBOXES];

  std::vector<int> vConstraints;
  std::vector<int*> vFlags;
  std::vector<double> vRangeMax;
  std::vector<double> vRangeMin;
  std::vector<double> vNominal;
  std::vector< vector<double> > vNominalByBin;
  std::vector< vector<double> > vRangeMaxByBin;
  std::vector< vector<double> > vRangeMinByBin;
  std::vector< vector<double> > vStatErrByBin;
  std::vector<double> vSystNeg;
  std::vector< vector<double> > vSystNegByBin;
  std::vector<double> vSystPos;
  std::vector< vector<double> > vSystPosByBin;
  std::vector<double> vGatherBins;
  int isyst;
  int mDoGathering;

};

}

#endif

