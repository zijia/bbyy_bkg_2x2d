#ifndef HGamBackgroundStudies_HGam2x2DInputMaker_H
#define HGamBackgroundStudies_HGam2x2DInputMaker_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "PhotonVertexSelection/PhotonVertexSelectionTool.h"
#include "HGamBackgroundStudies/Includes.h"

class HGam2x2DInputMaker : public HgammaAnalysis
{

 private:
  
  TFile *m_fout; //!
  TTree* m_mini_tree; //!

  // initialize output TTree branches
  void MakeIBranch(Str name) { m_mini_tree->Branch(name,&_iMap[name],name+"/I"); }
  void MakeIBranches(StrV bnames) 
  {  for(auto br:bnames) MakeIBranch(br); }

  void MakeUIBranch(Str name) { m_mini_tree->Branch(name,&_uiMap[name],name+"/i"); }
  void MakeUIBranches(StrV bnames) 
  {  for(auto br:bnames) MakeUIBranch(br); }

  void MakeFBranch(Str name) { m_mini_tree->Branch(name,&_fMap[name],name+"/F"); }
  void MakeFBranches(StrV bnames) 
  {  for(auto br:bnames) MakeFBranch(br); }

  void MakeOBranch(Str name) { m_mini_tree->Branch(name,&_oMap[name],name+"/O"); }
  void MakeOBranches(StrV bnames) 
  {  for(auto br:bnames) MakeOBranch(br); }

  void Make4VecFBranches(Str name) 
  { MakeFBranch(name+"_pt"); MakeFBranch(name+"_eta"); 
    MakeFBranch(name+"_phi"); MakeFBranch(name+"_m"); }
  
  std::map<Str,int> _iMap; //!
  std::map<Str,bool> _oMap; //!
  std::map<Str,unsigned int> _uiMap; //!
  std::map<Str,float> _fMap; //!
  std::map<Str,double> _dMap; //!
  //  Str _cutflowHistN; 
  void resetBranches() {
    for (auto &e:_oMap) e.second = 1;
    for (auto &e:_iMap) e.second = -99;
    for (auto &e:_uiMap) e.second = 99999;
    for (auto &e:_fMap) e.second = -99.0;
    for (auto &e:_dMap) e.second = -99.0;
  }

 public:
  // this is a standard constructor
  HGam2x2DInputMaker() { }
  //  HGam2x2DInputMaker(Str cfname, const char *name);
  HGam2x2DInputMaker(const char *name);
  virtual ~HGam2x2DInputMaker() { }
  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode finalize();
  EL::StatusCode InitialiseMiniTree();
  void WriteOutput();
  void WriteMiniTree();
  // this is needed to distribute the algorithm to the workers
  ClassDef(HGam2x2DInputMaker, 1);
};

#endif // HGamTools_HGam2x2DInputMaker_H
