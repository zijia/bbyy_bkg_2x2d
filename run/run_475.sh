#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_475.cfg
echo "MassPoint: 475GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_475.cfg
echo "OutputFile: Res_475/Resonant_475.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_475.cfg
mkdir Res_475
run2x2DSBBkgDecomp input_475 > 475_log
