#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_1000.cfg
echo "MassPoint: 1000GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_1000.cfg
echo "OutputFile: Res_1000/Resonant_1000.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_1000.cfg
mkdir Res_1000
run2x2DSBBkgDecomp input_1000 > 1000_log
