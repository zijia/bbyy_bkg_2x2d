#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_900.cfg
echo "MassPoint: 900GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_900.cfg
echo "OutputFile: Res_900/Resonant_900.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_900.cfg
mkdir Res_900
run2x2DSBBkgDecomp input_900 > 900_log
