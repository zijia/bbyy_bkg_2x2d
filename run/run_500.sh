#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_500.cfg
echo "MassPoint: 500GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_500.cfg
echo "OutputFile: Res_500/Resonant_500.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_500.cfg
mkdir Res_500
run2x2DSBBkgDecomp input_500 > 500_log
