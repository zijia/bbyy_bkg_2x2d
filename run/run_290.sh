#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_290.cfg
echo "MassPoint: 290GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_290.cfg
echo "OutputFile: Res_290/Resonant_290.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_290.cfg
mkdir Res_290
run2x2DSBBkgDecomp input_290 > 290_log
