#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_600.cfg
echo "MassPoint: 600GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_600.cfg
echo "OutputFile: Res_600/Resonant_600.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_600.cfg
mkdir Res_600
run2x2DSBBkgDecomp input_600 > 600_log
