#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_260.cfg
echo "MassPoint: 260GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_260.cfg
echo "OutputFile: Res_260/Resonant_260.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_260.cfg
mkdir Res_260
run2x2DSBBkgDecomp input_260 > 260_log
