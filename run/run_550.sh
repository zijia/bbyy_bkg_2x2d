#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_550.cfg
echo "MassPoint: 550GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_550.cfg
echo "OutputFile: Res_550/Resonant_550.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_550.cfg
mkdir Res_550
run2x2DSBBkgDecomp input_550 > 550_log
