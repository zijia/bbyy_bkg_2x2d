#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_375.cfg
echo "MassPoint: 375GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_375.cfg
echo "OutputFile: Res_375/Resonant_375.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_375.cfg
mkdir Res_375
run2x2DSBBkgDecomp input_375 > 375_log
