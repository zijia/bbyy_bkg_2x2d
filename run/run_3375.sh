#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_3375.cfg
echo "MassPoint: 3375GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_3375.cfg
echo "OutputFile: Res_3375/Resonant_3375.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_3375.cfg
mkdir Res_3375
run2x2DSBBkgDecomp input_3375 > 3375_log
