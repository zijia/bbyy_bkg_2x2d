#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_800.cfg
echo "MassPoint: 800GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_800.cfg
echo "OutputFile: Res_800/Resonant_800.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_800.cfg
mkdir Res_800
run2x2DSBBkgDecomp input_800 > 800_log
