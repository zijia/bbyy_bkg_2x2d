#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_280.cfg
echo "MassPoint: 280GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_280.cfg
echo "OutputFile: Res_280/Resonant_280.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_280.cfg
mkdir Res_280
run2x2DSBBkgDecomp input_280 > 280_log
