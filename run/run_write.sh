#mass="260 280 300 325 350 400 450 500 550 600 700 800 900 1000"
#mass="251"
mass="251 270 290 3125 3375 375 425 475"

for m in $mass;do
#rm run_${m}.sh
echo "#!/bin/bash -f" >> run_${m}.sh
echo "cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/" >> run_${m}.sh
echo "source ../condor_setup.sh" >> run_${m}.sh
echo "cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/" >> run_${m}.sh
echo "cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_${m}.cfg" >> run_${m}.sh
echo "echo \"MassPoint: ${m}GeV\" >> ../source/HGamCore/HGamBackgroundStudies/data/input_${m}.cfg" >> run_${m}.sh
echo "echo \"OutputFile: Res_${m}/Resonant_${m}.root\" >> ../source/HGamCore/HGamBackgroundStudies/data/input_${m}.cfg" >> run_${m}.sh
echo "mkdir Res_${m}" >> run_${m}.sh
echo "run2x2DSBBkgDecomp input_${m} > ${m}_log" >> run_${m}.sh
chmod +x run_${m}.sh
#mv ${m}GeVtestOUT.tex Res_${m}/
#mv ${m}_log Res_${m}/
done
