#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_251.cfg
echo "MassPoint: 251GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_251.cfg
echo "OutputFile: Res_251/Resonant_251.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_251.cfg
mkdir Res_251
run2x2DSBBkgDecomp input_251 > 251_log
