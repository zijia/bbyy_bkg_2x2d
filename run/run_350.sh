#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_350.cfg
echo "MassPoint: 350GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_350.cfg
echo "OutputFile: Res_350/Resonant_350.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_350.cfg
mkdir Res_350
run2x2DSBBkgDecomp input_350 > 350_log
