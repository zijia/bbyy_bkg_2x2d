#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_3125.cfg
echo "MassPoint: 3125GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_3125.cfg
echo "OutputFile: Res_3125/Resonant_3125.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_3125.cfg
mkdir Res_3125
run2x2DSBBkgDecomp input_3125 > 3125_log
