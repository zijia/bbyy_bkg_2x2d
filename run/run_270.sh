#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_270.cfg
echo "MassPoint: 270GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_270.cfg
echo "OutputFile: Res_270/Resonant_270.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_270.cfg
mkdir Res_270
run2x2DSBBkgDecomp input_270 > 270_log
