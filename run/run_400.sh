#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_400.cfg
echo "MassPoint: 400GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_400.cfg
echo "OutputFile: Res_400/Resonant_400.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_400.cfg
mkdir Res_400
run2x2DSBBkgDecomp input_400 > 400_log
