#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_425.cfg
echo "MassPoint: 425GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_425.cfg
echo "OutputFile: Res_425/Resonant_425.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_425.cfg
mkdir Res_425
run2x2DSBBkgDecomp input_425 > 425_log
