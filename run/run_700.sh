#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_700.cfg
echo "MassPoint: 700GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_700.cfg
echo "OutputFile: Res_700/Resonant_700.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_700.cfg
mkdir Res_700
run2x2DSBBkgDecomp input_700 > 700_log
