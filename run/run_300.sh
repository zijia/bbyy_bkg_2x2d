#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_300.cfg
echo "MassPoint: 300GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_300.cfg
echo "OutputFile: Res_300/Resonant_300.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_300.cfg
mkdir Res_300
run2x2DSBBkgDecomp input_300 > 300_log
