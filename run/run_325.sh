#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_325.cfg
echo "MassPoint: 325GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_325.cfg
echo "OutputFile: Res_325/Resonant_325.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_325.cfg
mkdir Res_325
run2x2DSBBkgDecomp input_325 > 325_log
