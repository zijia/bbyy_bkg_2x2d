#!/bin/bash -f
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
source ../condor_setup.sh
cd /afs/cern.ch/work/z/zijia/private/hgam-HHyybb-v1_10_11/run/
cp ../source/HGamCore/HGamBackgroundStudies/data/input_head.cfg ../source/HGamCore/HGamBackgroundStudies/data/input_450.cfg
echo "MassPoint: 450GeV" >> ../source/HGamCore/HGamBackgroundStudies/data/input_450.cfg
echo "OutputFile: Res_450/Resonant_450.root" >> ../source/HGamCore/HGamBackgroundStudies/data/input_450.cfg
mkdir Res_450
run2x2DSBBkgDecomp input_450 > 450_log
